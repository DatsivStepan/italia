<?php

namespace app\models;

use yii\db\ActiveRecord;

class States extends ActiveRecord
{
	public static function tableName()
    {
        return 'states';
    }
}