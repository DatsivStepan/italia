<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Lang;

class Category extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%category}}';
    }

    public function scenarios()
    {
        return [
            'add' => ['parent_id'],
            'update' => ['parent_id'],
        ];
    }
    
    public function rules()
    {
        return [
            
        ];
    }
    public function getName()
    {
        $lang = Lang::getCurrent();
        $CategoryContent = CategoryContent::findOne([
            'join_id' => $this->id,
            'lang' => $lang->url,
        ]);
        return $CategoryContent['name'];
    }
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }
    
    public function getProduct()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
        ];
    }

    public function getTranslation()
    {
        return $this->hasMany(CategoryContent::className(), ['join_id' => 'id']);
    }

    public function getCategoryName()
    {
        
        $leng = Lang::getCurrent();
        return $this->hasMany(CategoryContent::className(), ['join_id' => 'id'])->andWhere(['lang' => $leng->url]);
    }

}
