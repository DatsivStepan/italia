<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;
use app\models\Lang;

class PremiumPacket extends ActiveRecord
{
	public static function tableName()
    {
        return 'premium_paket';
    }

    public function scenarios()
    {
        return [
            'default' => ['name_ru', 'name_it', 'count_prem_prod', 'price', 'description_ru', 'description_it', 'description_en'],
        ];
    }

    public function getName()
    {
        $leng = Lang::getCurrent();
        if($leng->url == 'ru'){
            $name = $this->name_ru;
        }else{
            $name = $this->name_it;
        }
        return $name;
    }
    public function getDescription()
    {
        $leng = Lang::getCurrent();
        if($leng->url == 'ru'){
            $name = $this->description_ru;
        }elseif ( $leng->url == 'it' ) {
            $name = $this->description_it;
        }else{
            $name = $this->description_en;
        }
        return $name;
    }
    public function attributeLabels()
    {
        return [
            'description_ru' => Yii::t('app', 'Описание (ru)'),
            'description_it' => Yii::t('app', 'Описание (it)'),
            'description_en' => Yii::t('app', 'Описание (en)')
        ];
    }

}