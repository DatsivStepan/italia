<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class ProductContent extends ActiveRecord
{
	public static function tableName()
    {
        return 'product_content';
    }
}