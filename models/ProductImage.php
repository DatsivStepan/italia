<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
        
class ProductImage extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%product_image}}';
    }

    public function scenarios()
    {
        return [
            'new_product_image' => [
                    'img_src', 'product_id'
                ],
        ];
    }
    
    public function rules()
    {
        return [
            [['img_src', 'product_id'], 'required']
        ];
    }
        
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
        ];
    }

}
