<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Lang;
use app\models\UserEmailPackets;
use app\models\PremiumPacket;
use app\modules\administration\models\PricingContent;
use app\modules\administration\models\Pricing;
use app\modules\administration\models\EmailPackets;
use app\models\UserOtherPays;
use \Datetime;

class UserPakets extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%user_pakects}}';
    }

    
    public function rules()
    {
        return [
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d");
        }

        return parent::beforeSave($insert);
    }

    public function getLangname()
    {
        
        $leng = Lang::getCurrent();
        return $this->hasOne(PricingContent::className(), ['join_id' => 'paket_id'])->andWhere(['lang' => $leng->url]);
    }

    public function getName()
    {
        $leng = Lang::getCurrent();

        if($this->type == 'premium')
        {
            $premiumPacket = PremiumPacket::find()->one();
            if($leng->url == 'ru'){
                $name = $premiumPacket->name_ru;
            }else{
                $name = $premiumPacket->name_it;
            }
            return $name;
        }
        else
        {
            $PricingContent = PricingContent::findOne([
                'join_id' => $this->paket_id,
                'lang' => $leng->url,
            ]);            
            return $PricingContent->name;
        }
    }

    public function getCountMessage()
    {
        if($this->type == 'premium')
        {
            $message = 999999;
        }
        else
        {
            $tarifModel = Pricing::find()->where(['id' => $this->paket_id])->one();
            $tarifMessage = $tarifModel->message;

            $UserEmailPackets = UserEmailPackets::find()->where(['user_id' => $this->user_id, 'status'=>10])->all();
            $message = 0;

            foreach ($UserEmailPackets as $key => $UserEmailPacket) 
            {
                $EmailPackets = EmailPackets::find()->where(['id' => $UserEmailPacket->paket_id])->one();
                $message += $EmailPackets->emails_count;
            }
            $message += $tarifMessage;
        }
        

        return $message - Yii::$app->user->identity->sended_emails;
    }

    public function getCountProdLimit()
    {
        if($this->type == 'premium')
        {
            $prodLimit = 999999;
        }
        else
        {
            $user_id = Yii::$app->user->identity->id;

            $tarifMLimit = $this->paket->product_count;

            $UserEmailPackets = UserEmailPackets::find()->where(['user_id' => $this->user_id, 'status'=>10])->all();
            $prodLimit = 0;

            foreach ($UserEmailPackets as $key => $UserEmailPacket) 
            {
                $EmailPackets = EmailPackets::find()->where(['id' => $UserEmailPacket->paket_id])->one();
                $prodLimit += $EmailPackets->product;
            }
            $prodLimit += $tarifMLimit;
        }
        

        return $prodLimit;
    }

    public function getPaket()
    {
        return $this->hasOne(Pricing::className(), ['id' => 'paket_id']);
    }

    public function getDescription()
    {
        if($this->status == 10)
        {
            $tarifModel = Pricing::find()->where(['id' => $this->paket_id])->one();
            $tarifType = $this->type;
            $month;

            switch ($tarifType) 
            {
                case "tarif_regist":
                    $month = $tarifModel->months_count;
                    break;
                case "tarif_one":
                    $month = 1;
                    break;
                case "tarif_standart":
                    $month = $tarifModel->mouth_to_long;
                    break;
                case "premium":
                    $month = 1;
                    break;
                default:                   
            }
            $availDate = DateTime::createFromFormat('Y-m-d', date('Y-m-d'))->modify('+'.$month.' month')->format('Y-m-d');
            $message = 'Тариф активен и доступен до '.$availDate;
        }
        else{
            $message = 'Тариф будет доступен после окончания срока действия активного тарифа';
        }
        
        return $message;
    }

    public function getPacketPrice()
    {
        if($this->type == 'premium')
        {
            $tarifModel = PremiumPacket::find()->one();
            $price = $tarifModel->price;
        }
        else
        {
            $tarifModel = Pricing::find()->where(['id' => $this->paket_id])->one();
            $tarifType = $this->type;
            $price;

            switch ($tarifType) 
            {
                case "tarif_regist":
                    $price = $tarifModel->months_count * $tarifModel->price;
                    break;
                case "tarif_one":
                    $price = $tarifModel->price_to_one;
                    break;
                case "tarif_standart":
                    $price = $tarifModel->mouth_to_long * $tarifModel->price_to_long;
                    break;
                default:                   
            }
        }
        
        return $price;
    }

    public function getMouths()
    {
        $tarifModel = Pricing::find()->where(['id' => $this->paket_id])->one();
        $tarifType = $this->type;
        $mouths;

        switch ($tarifType) 
        {
            case "tarif_regist":
                $mouths = 'на '.$tarifModel->months_count.' месяцев';
                break;
            case "tarif_one":
                $mouths = 'на один месяц';
                break;
            case "tarif_standart":
                $mouths = 'на '.$tarifModel->mouth_to_long.' месяцев';
                break;
            case "premium":
                $mouths = 'на один месяц';
                break;
            default:                   
        }
        
        return $mouths;
    }

    public function getPremiumVipCount()
    {
        $existVip = Product::find()->andwhere(['user_id' => $this->user_id, 'vip'=> 1])->count();
        $payVip = UserOtherPays::find()->andwhere(['user_id' => $this->user_id, 'type'=> 'top', 'status'=> 10])->count();
        $count = ( $payVip - $existVip) + 3;
        
        return $count;
    }

}