<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Lang;

class Pages extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%pages}}';
    }

    public function scenarios()
    {
        return [
            'update' => ['name', 'content', 'sort'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
            'sort' => Yii::t('app', 'Номер'),
        ];
    }

    public function getTranslation()
    {
        return $this->hasMany(PagesContent::className(), ['join_id' => 'id']);
    }

    public function getCurentLangPages()
    {
        
        $lang = Lang::getCurrent();
        return $this->hasMany(PagesContent::className(), ['join_id' => 'id'])->andWhere(['lang' => $lang->url]);
    }

}
