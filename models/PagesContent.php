<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class PagesContent extends ActiveRecord
{
	public static function tableName()
    {
        return 'pages_content';
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Название Страницы'),
            'content' => Yii::t('app', 'Контент Страницы'),
        ];
    }
}