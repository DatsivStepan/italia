<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class EmailArchive extends ActiveRecord
{
	public static function tableName()
    {
        return 'email_archive';
    }
}