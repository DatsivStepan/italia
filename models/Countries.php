<?php

namespace app\models;

use yii\db\ActiveRecord;

class Countries extends ActiveRecord
{
	public static function tableName()
    {
        return 'countries';
    }
}