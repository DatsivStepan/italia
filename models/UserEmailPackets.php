<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Lang;
use app\modules\administration\models\EmailPacketsContent;
use app\modules\administration\models\EmailPackets;

class UserEmailPackets extends ActiveRecord
{
	public static function tableName()
    {
        return 'user_email_packets';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d");
        }

        return parent::beforeSave($insert);
    }
    public function getPaket()
    {
        return $this->hasOne(EmailPackets::className(), ['id' => 'paket_id']);
    }
    public function getName()
    {
        $leng = Lang::getCurrent();
        $EmailsContent = EmailPacketsContent::findOne([
            'join_id' => $this->paket_id,
            'lang' => $leng->url,
        ]);
        return $EmailsContent->name;
    }
}