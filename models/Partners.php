<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

class Partners extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return 'partners';
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

}
