<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class ProductAttr extends ActiveRecord
{
	public static function tableName()
    {
        return 'product_attr';
    }
}