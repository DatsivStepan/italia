<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Lang;
use app\modules\administration\models\EmailPacketsContent;

class UserOtherPays extends ActiveRecord
{
	public static function tableName()
    {
        return 'user_other_pays';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d");
        }

        return parent::beforeSave($insert);
    }
}