<?php

namespace app\models;

use yii\db\ActiveRecord;

class Reviews extends ActiveRecord
{
	public static function tableName()
    {
        return 'reviews';
    }
    public function rules()
    {
        return [
            ['content', 'required'],
            ['content', 'string'],
        ];
    }
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
            $this->user_id = \Yii::$app->user->identity->id;
            $this->name = \Yii::$app->user->identity->username;
            $this->status = 0;
        }

        return parent::beforeSave($insert);
    }
}