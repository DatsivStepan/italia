<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class CategoryContent extends ActiveRecord
{
	public static function tableName()
    {
        return 'category_content';
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Название Категории'),
        ];
    }
}