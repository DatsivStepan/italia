<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Lang;
use app\models\Currency;
use app\models\ProductContent;
use app\models\ProductAttr;

//address_status:  1-Show address, 0-Not show address,
//status:  1-Show, 0-Not show,
        
class Product extends \yii\db\ActiveRecord
{
    public $image_array;
    public static function tableName()
    {
        return '{{%product}}';
    }

    public function scenarios()
    {
        return [
            'new_product' => [
                    'product_type_id', 'category_id', 'object_id',
                    'country_id', 'region_id', 'city_id',
                    'object_area', 'address',
                    'address_status', 'year_of_construction', 
                    'user_id', 'price','price_usd', 'img_src',
                    'currency', 'status', 'image_array',
                ],
            'update_image' => ['img_src']
        ];
    }
    
    public function rules()
    {
        return [
            
        ];
    }

    public static function find()
    {
        $controllerName = Yii::$app->controller->id;
        $controllerAction = Yii::$app->controller->action->id;
        
        if ($controllerName == 'product' && $controllerAction == 'product') 
        {
            return parent::find();
        }
        elseif($controllerName == 'product')
        {
            return parent::find()->where(['>', 'status', 0]);
        }
        
        else
        {
            return parent::find()->andWhere(['status'=> 10]);
        }
    }
        
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }
        $this->price_usd = $this->saveInUsd($this->price, $this->currency);
        return parent::beforeSave($insert);
    }

    public function getCategory()
    {
        if(isset($this->product_type_id)){
            return $this->hasOne(Category::className(), ['id' => 'product_type_id']);
        }else{
            return new Category();
        } 
    }
    public function getTranslationCategory()
    {
        return $this->hasMany(CategoryContent::className(), ['join_id' => 'product_type_id']);
    }
    public function getRegion()
    {
        if(isset($this->region_id)){
            return $this->hasOne(States::className(), ['id' => 'region_id']);
        }else{
            return new States();
        }
    }
    public function getProdAttr()
    {
        return $this->hasOne(ProductAttr::className(), ['join_id' => 'id']);
    }
    public function getImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_id' => 'id']);
    }

    public function getTranslation()
    {
        return $this->hasMany(ProductContent::className(), ['join_id' => 'id']);
    }

    public function getCurentLangProducts()
    {
        
        $leng = Lang::getCurrent();
        return $this->hasMany(ProductContent::className(), ['join_id' => 'id'])->andWhere(['lang' => $leng->url]);
    }
    public function getName()
    {
        $lang = Lang::getCurrent();
        $ProductContent = ProductContent::findOne([
            'join_id' => $this->id,
            'lang' => $lang->url,
        ]);
        return $ProductContent->name;
    }
    public function getCurrenc()
    {
        return $this->hasOne(Currency::className(), ['slug' => 'currency']);
    }

    public function getAbout()
    {
        $leng = Lang::getCurrent();
        $ProductContent = ProductContent::findOne([
            'join_id' => $this->id,
            'lang' => $leng->url,
        ]);
        return $ProductContent->content;
    }
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
            'product_type_id' => Yii::t('main', 'type_of_a_sentence'),
            'category_id' => Yii::t('main', 'Offer_Category'),
            'object_id' => Yii::t('main', 'Object_type'),
            'country_id' => Yii::t('main', 'country'),
            'region_id' => Yii::t('main', 'Region'),
            'city_id' => Yii::t('main', 'City'),
            'img_src' => Yii::t('main', 'Main_photo'),
            'object_area' => 'Жилая площадь',
            'address' => 'Адрес объекта на карте',
            'address_status' => Yii::t('main', 'Show_Address'),
            'year_of_construction' => Yii::t('main', 'Year_of_construction'),
            'price' => Yii::t('main', 'Enter_price'),
            'currency' => Yii::t('main', 'Enter_currency'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
    public function saveInUsd($price, $currency)
    {
        if($currency!==1){
            $modelCurren = Currency::find()->where(['slug'=>$currency])->one();
            return $price*$modelCurren->value;
        }
        else{
            return $price;
        }
    }
}
