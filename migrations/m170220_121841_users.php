<?php

use yii\db\Schema;
use yii\db\Migration;

class m170220_121841_users extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users}}', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING . '(255) NOT NULL',
            'email' => Schema::TYPE_STRING . '(255) NOT NULL',
            'status' => Schema::TYPE_STRING . '(255) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . '(255) NOT NULL',
            'auth_key' => Schema::TYPE_STRING . '(255) NOT NULL',
            'secret_key' => Schema::TYPE_STRING . '(255) NOT NULL',
            'date_create' => Schema::TYPE_DATETIME,
            'update_at' => Schema::TYPE_STRING . '(255) NOT NULL',
            'password_reset_token' => Schema::TYPE_STRING . '(255) NOT NULL',
            'access_token' => Schema::TYPE_STRING . '(255) NOT NULL',
            'social_id' => Schema::TYPE_STRING . '(255) NOT NULL',
            'signup_type' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);
        
        $this->batchInsert('users', ['username', 'email', 'status', 'password_hash', 'auth_key', 'secret_key', 'date_create', 'update_at'], 
            [['admin', 'datsivStepan@gmail.com', '10', '$2y$13$IXNGyMNxFdXBH.i.2eD9wOnOsEZNiWONememsdcgktY.HfysxdDci', 'key', '', date("Y-m-d H:i:s"), time()]]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }

}
