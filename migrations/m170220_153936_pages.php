<?php

use yii\db\Schema;
use yii\db\Migration;

class m170220_153936_pages extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%pages}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'contact' => Schema::TYPE_TEXT . '(255) NOT NULL',
            'sort' => Schema::TYPE_STRING . '(255) NOT NULL',
            'date_create' => Schema::TYPE_DATETIME,
            'slug' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%pages}}');
    }
    
}
