<?php

namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\UserPakets;
use app\models\Product;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Category;
use app\models\Setting;
use app\models\ProductAttr;
use app\models\EmailArchive;
use app\models\Lang;
use yii\data\ActiveDataProvider;


class ProductController extends Controller
{
    /**
     * @inheritdoc
     */

    public $offset = 12;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post','get'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionQuestion(){
        $result = [''];
        $name ='';
        $email ='';
        $text ='';
        foreach ($_POST['form_data'] as $value) {
            switch ($value['name']) {
                case 'name':
                    $name = $value['value'];
                    break;
                case 'email':
                    $email = $value['value'];
                    break;
                case 'text':
                    $text = $value['value'];
                    break;
                case 'email_user':
                    $email_user = $value['value'];
                    break;
            }
        }
       //var_dump($email_user); exit();
            if (($name != "")&&($email != "")&&($text != "")) {
                $to = $email_user;
                $subject = 'Вопрос по обявлению';
                $message = '
            <html>
                    <head>                                        
                        <title>' . $subject . '</title>
                    </head>
                <body>
                    <img src="http://italia.rocetdew/images/social_links/facebook.png">
                    <p><b>Имя:</b> ' . $name . '</p>
                    <p><b>эл. почта:</b> ' . $email . '</p>
                    <p><b>Сообщение:</b> ' . $text . '</p>
                </body>
            </html>';
                $headers = "Content-type: text/html; charset=utf-8 \r\n";
                mail($to, $subject, $message, $headers);

                $result['status'] = 'success';
            } else {
                $result['status'] = 'error';
            }

            echo json_encode($result);
    }

    public function actionSubscribe(){
        $result = [''];
        $email ='';
        foreach ($_POST['form_subscribe'] as $value) {
            switch ($value['name']) {
                case 'email':
                    $email = $value['value'];
                    break;
                case 'email_user':
                    $email_user = $value['value'];
                    break;
            }
        }
        //var_dump($email_user); exit();
        if (($email != "")&& filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $to = $email_user;
            $subject = 'Вопрос по обявлению';
            $message = '
            <html>
                    <head>                                        
                        <title>' . $subject . '</title>
                    </head>
                <body>
                    <img src="http://italia.rocetdew/images/social_links/facebook.png">
                    <p><b>эл. почта:</b> ' . $email . '</p>   
                </body>
            </html>';
            $headers = "Content-type: text/html; charset=utf-8 \r\n";
            mail($to, $subject, $message, $headers);

            $result['status'] = 'success';
        } else {
            $result['status'] = 'error';
        }

        echo json_encode($result);
    }
    public function actionTexpod(){
        //var_dump($_POST);exit();
        $result = [''];
        $email ='';
        foreach ($_POST['texpod_form'] as $value) {
            switch ($value['name']) {
                case 'text':
                    $text = $value['value'];
                    break;
                case 'email_user':
                    $email_user = $value['value'];
                    break;
            }
        }
        //var_dump($email_user); exit();
        if ($text != "") {
            $to = $email_user;
            $subject = 'Техподдержка';
            $message = '
            <html>
                    <head>                                        
                        <title>' . $subject . '</title>
                    </head>
                <body>
                    <img src="http://italia.rocetdew/images/social_links/facebook.png">
                    <p><b>Вопрос:</b> ' . $text . '</p>   
                </body>
            </html>';
            $headers = "Content-type: text/html; charset=utf-8 \r\n";
            mail($to, $subject, $message, $headers);

            $result['status'] = 'success';
        } else {
            $result['status'] = 'error';
        }
    }

    public function actionSendcall(){
        //var_dump($_POST);exit();
        $result = [''];
        foreach ($_POST['call_to_email'] as $value) {
            switch ($value['name']) {
                case 'name':
                    $name = $value['value'];
                    break;
                case 'number':
                    $number = $value['value'];
                    break;
                case 'email_user':
                    $email_user = $value['value'];
                    break;
            }
        }
        //var_dump($email_user); exit();
        if (($name != "")&&($number != "")) {
            $to = $email_user;
            $subject = 'Звонок';
            $message = '
            <html>
                    <head>                                        
                        <title>' . $subject . '</title>
                    </head>
                <body>
                    <img src="http://italia.rocetdew/images/social_links/facebook.png">
                    <p><b>Имя:</b> ' . $name . '</p>   
                    <p><b>Телефон:</b> ' . $number . '</p>   
                </body>
            </html>';
            $headers = "Content-type: text/html; charset=utf-8 \r\n";
            mail($to, $subject, $message, $headers);

            $result['status'] = 'success';
        } else {
            $result['status'] = 'error';
        }

        echo json_encode($result);
    }

    public function actionSendpopup(){
        //var_dump($_POST);exit();
        $session = Yii::$app->session;
        $session->set('popup', 'set');

        $result = [''];
        foreach ($_POST['call_to_email'] as $value) {
            switch ($value['name']) {
                case 'name':
                    $name = $value['value'];
                    break;
                case 'number':
                    $number = $value['value'];
                    break;
                case 'email':
                    $email = $value['value'];
                    break;
                case 'email_user':
                    $email_user = $value['value'];
                    break;
            }
        }
        //var_dump($email_user); exit();
        if (($name != "")&&($number != "")) {
            $to = $email_user;
            $subject = 'Вопрос менеджеру';
            $message = '
            <html>
                    <head>                                        
                        <title>' . $subject . '</title>
                    </head>
                <body>
                    <img src="http://italia.rocetdew/images/social_links/facebook.png">
                    <p><b>Имя:</b> ' . $name . '</p>   
                    <p><b>Телефон:</b> ' . $number . '</p>
                    <p><b>Email:</b> ' . $number . '</p>   
                </body>
            </html>';
            $headers = "Content-type: text/html; charset=utf-8 \r\n";
            mail($to, $subject, $message, $headers);

            $result['status'] = 'success';
        } else {
            $result['status'] = 'error';
        }

        echo json_encode($result);
    }

    public function actionUnsetpopup(){
        //var_dump($_POST);exit();
        $session = Yii::$app->session;
        $session->set('popup', 'set');
    }

    public function actionSendcallSelling(){
        $canMessageSend;
        $result = [''];
        foreach ($_POST['call_to_selling'] as $value) {
            switch ($value['name']) {
                case 'name':
                    $name = $value['value'];
                    break;
                case 'number':
                    $number = $value['value'];
                    break;
                 case 'email':
                    $email = $value['value'];
                    break;
                case 'content':
                    $content = $value['value'];
                    break;
                case 'email_user':
                    $email_user = $value['value'];
                    break;
                case 'prod_name':
                    $prod_name = $value['value'];
                    break;
            }
        }

        $owner = User::find()->where(['email'=>$email_user])->one();
        $ownerPaket = UserPakets::find()->where(['user_id'=>$owner->id, 'status'=>'10'])->one();

        if( $ownerPaket && $ownerPaket->countMessage > 0){
            $canMessageSend = true;
            if($ownerPaket->type !== 'premium'){
                $owner->sended_emails++;
                $owner->save();
            }
        }else{
            $canMessageSend = false;
        }

        if ( $name != ""  && ( $number != "" || $email != "") ) {
            $to = $email_user;
            $subject = '<b>Обратная связь по обявлению :</b> '. $prod_name .'';
            $message = '
            <html>
                    <head>                                        
                        <title>' . $subject . '</title>
                    </head>
                <body>
                    <p><b>Обратная связь по обявлению :</b> ' . $prod_name . '</p>
                    <p><b>Имя:</b> ' . $name . '</p>
                    <p><b>Email:</b> ' . $email . '</p>     
                    <p><b>Телефон:</b> ' . $number . '</p>
                    <p>'.$content.'</p>  
                </body>
            </html>';

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            $headers .= 'From: <'.$email_user.'>' . "\r\n";
            $headers .= 'Cc: '.$email_user. "\r\n";


            if($canMessageSend){
                $emailST = mail($to, $subject, $message, $headers);
                /*var_dump($to);
                var_dump($subject);
                var_dump($message);
                var_dump($headers);
                var_dump($emailST);exit;*/
            }else{
                $emailId = $this->addEmailToArchive($owner->id, $message);
                $message_hide = '
                <html>
                        <head>                                        
                            <title>' . $subject . '</title>
                        </head>
                    <body>
                        <p><b>Обратная связь по обявлению :</b> ' . $prod_name . '</p>
                        <p><b>Имя:</b> ' . $name . '</p>
                        <p><b>Email:</b>xxx@email.com</p>     
                        <p><b>Телефон:</b>xxxxxxxxxx</p>
                        <p>'.$content.'</p>  
                        <p> Чтобы получить данные пользователя нажмите <a href="/pay/pay-first-steep?model=email&id='.$emailId.'">здесь</a></p> 
                    </body>
                </html>';
                $emailST = mail($to, $subject, $message_hide, $headers);            
            }
            
            
            $result['status'] = 'success';
        } else {
            $result['status'] = 'error';
        }

        echo json_encode($result);
    }

    public function actionProduct($product_id=null){

        if($product_id != null){
            $attrModel = ProductAttr::find()->where(['join_id' => $product_id])->one();            
            if(!$attrModel){
                $attrModel = $this->createAttrModel();
            }
            $modelProduct = Product::find()->andWhere(['id' => $product_id])->one();
            $modelUserAutor = User::find()->where(['id' => $modelProduct->user_id])->one();
            $arraySetting = ArrayHelper::map(Setting::find()->all(),'key','value');
            if($modelProduct){
                return $this->render('product',[
                    'modelProduct' => $modelProduct,
                    'modelUserAutor' => $modelUserAutor,
                    'arraySetting' => $arraySetting,
                    'attrModel' => $attrModel,
                ]);                            
            }else{
                throw new \yii\web\NotFoundHttpException();                
            }
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    public function actionCategory($type_category=null, $category_id=null)
    {
        $count_prod = 0;
        
        // select only active users
        $activeUsers = UserPakets::find()->select(['user_id'])->where(['status'=>'10'])->asArray()->all();
        $user_ids = ArrayHelper::getColumn($activeUsers, 'user_id');

        if($type_category!=null)
        {
            if($type_category=='lease'){
                $parent_id =1;
                $link = 'lease';
                $this->view->params['menu_item']['lease'] = 'headMenu';
            }elseif ($type_category=='selling') {
                $parent_id =2;
                $link = 'selling';
                $this->view->params['menu_item']['selling'] = 'headMenu';
            }else{
                throw new \yii\web\NotFoundHttpException();
            }
            
            $paretnCategoty =  Category::find()->where( ['id'=>$parent_id] )->one();
            $title = $paretnCategoty->name;

            $categoriesFind = Category::find()->where( ['parent_id'=>$parent_id] );
            $modelCategories = new ActiveDataProvider(['query' => $categoriesFind, 'pagination' => ['pageSize' => 20]]);
            $categories = $modelCategories->getModels();

            if($category_id==null){
                $active_category = 'all';
                $count_prod = Product::find()->andWhere(['product_type_id'=>$parent_id, 'user_id'=>$user_ids])->count();
            }
            else{
                $active_category = $category_id;
            }
            
            return $this->render('category', [
                'categories'=> $categories,
                'pagination' => $modelCategories->pagination,
                'title' => $title,
                'active_category' => $active_category,
                'link' => $link,
                'count_prod'=> $count_prod,
                'parent_id'=> $parent_id
            ]);
        }
        else
        {
            throw new \yii\web\NotFoundHttpException();
        }
    }

    public function actionAgency($agency_id)
    {
        $agency = User::find()->where(['id'=>$agency_id])->one();

        if($agency){
            $title = $agency->userinfo->company_name;
            $count_prod = 0;

            $productsFind = Product::find()->andWhere(['user_id'=>$agency_id]);

            $modelProducts = new ActiveDataProvider(['query' => $productsFind, 'pagination' => ['pageSize' => 20]]);
            $products = $modelProducts->getModels();

            $count_prod = $productsFind->count();

            
            return $this->render('agency', [
                'products' => $products,
                'pagination' => $modelProducts->pagination,
                'title' => $title,
                'count_prod'=> $count_prod,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }      
    }



    public function actionSortFilters()
    {
        $user_ids = $this->activeUserIds();
        $postData = Yii::$app->request->post();
        $langsKey = $this->langsKey($postData['lang']);
        $products = Product::find()
        ->andWhere( ['product_type_id'=>(int)$postData['id'], 'user_id'=>$user_ids] )
        ->limit($this->offset);
        if($postData['filterCategory']!== '0'){
            $products->andWhere(['category_id'=>$postData['filterCategory']]);
        }

        $count = $products->count();

        if($postData['orderType']=='SORT_DESC'){
            $products->orderBy([$postData['orderName'] => SORT_DESC]);
        }else{
            $products->orderBy([$postData['orderName'] => SORT_ASC]);
        }
        //var_dump($products->all());exit;
        $products = $products->all();

        if($count!==0){
            $products = $this->productData($products, $langsKey);
            $productsJson = JSON::encode($products);
        }else{
            $productsJson = '';
        }


        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return [
            'products' => $productsJson,
            'count' => $count
        ];
    }

    public function addEmailToArchive($user_id, $message)
    {
        $EmailArchiveModel = new EmailArchive();
        $EmailArchiveModel->user_id = $user_id;
        $EmailArchiveModel->message = Html::encode($message);
        $EmailArchiveModel->status = 0;
        $EmailArchiveModel->save();

        return $EmailArchiveModel->id;
    }

    public function activeUserIds(){
        // select only active users
        $activeUsers = \app\models\UserPakets::find()->select(['user_id'])->where(['status'=>'10'])->asArray()->all();
        $user_ids = \yii\helpers\ArrayHelper::getColumn($activeUsers, 'user_id');

        $activeOwners = \app\models\User::find()->select(['id'])->where(['status'=>'10', 'type'=>'owner'])->asArray()->all();
        $owner_ids = \yii\helpers\ArrayHelper::getColumn($activeOwners, 'id');

        $ids = \yii\helpers\ArrayHelper::merge($user_ids, $owner_ids);
        return $ids;
    }

    function langsKey($lang_local)
    {
        $langs = Lang::find()->all();
        foreach ($langs as $key => $lang) {
            if($lang->local==$lang_local){

                $lang_key = $key;
            }
        }
        return $lang_key;
    }

    function productData($products, $lang_key)
    {
        
        $ArrProducts = [];
        
        foreach ($products as $key => $product) {
            $ArrProducts[$key]['name'] = $product->translation[$lang_key]->name;
            $ArrProducts[$key]['id'] = $product->id;
            $ArrProducts[$key]['category'] = $product->translationCategory[$lang_key]->name;
            $ArrProducts[$key]['img'] = $product->img_src;
            $ArrProducts[$key]['object_area'] = $product->object_area;
            $ArrProducts[$key]['region'] = $product->region->name;
            $ArrProducts[$key]['currenc'] = $product->currenc->name;
            $ArrProducts[$key]['price'] = $product->price;
        }
        return $ArrProducts;
    }
}
