<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Product;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Category;
use app\models\Lang;
use app\models\States;
use yii\data\ActiveDataProvider;

class CatalogController extends Controller
{
    /**
     * @inheritdoc
     */
    public $offset = 12;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post','get'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $user_ids = $this->activeUserIds();
        $products = Product::find()
        ->andWhere( ['product_type_id'=>1] )
        ->limit($this->offset)
        ->orderBy(['date_create' => SORT_DESC])
        ->all();
        $subCatsModel = Category::find()->where( ['parent_id'=>1] )->all();
        $categories = $this->subCategory($subCatsModel);
        $count = Product::find()->andWhere( ['product_type_id'=>1, 'user_id'=>$user_ids] )->count();

        $arrStates = States::find()->where(['country_id' => 107])->all();
               
        return $this->render('catalog', [
            'products'=> $products,
            'categories'=> $categories,
            'arrStates'=> $arrStates,
            'count'=> $count,
            'title'=> Yii::t('main','Catalog'),
        ]);
    }

    public function actionChangeCategory()
    {
        $user_ids = $this->activeUserIds();
        $postData = Yii::$app->request->post();
        $langsKey = $this->langsKey($postData['lang']);
        $products = Product::find()
        ->andWhere( ['product_type_id'=>$postData['id'], 'user_id'=>$user_ids] )
        ->limit($this->offset)
        ->orderBy(['date_create' => SORT_DESC])
        ->all();
        $subCatsModel = Category::find()->where( ['parent_id'=>$postData['id']] )->all();
        $categories = $this->subCategory($subCatsModel, $langsKey);
        $count = Product::find()->andWhere( ['product_type_id'=>$postData['id'], 'user_id'=>$user_ids] )->count();
        $products = $this->productData($products, $langsKey);
        $productsJson = JSON::encode($products);
        $categoriesJson = JSON::encode($categories);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return [
            'products' => $productsJson,
            'categories'=> $categoriesJson,
            'count' => $count,
        ];
    }

    public function actionResetCategory()
    {
        $user_ids = $this->activeUserIds();
        $postData = Yii::$app->request->post();
        $langsKey = $this->langsKey($postData['lang']);
        $products = Product::find()
        ->andWhere( ['product_type_id'=>$postData['id'], 'user_id'=>$user_ids] )
        ->limit($this->offset)
        ->orderBy(['date_create' => SORT_DESC])
        ->all();

        $count = Product::find()->andWhere( ['product_type_id'=>$postData['id'], 'user_id'=>$user_ids] )->count();
        $products = $this->productData($products, $langsKey);
        $productsJson = JSON::encode($products);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return [
            'products' => $productsJson,
            'count' => $count,
        ];
    }

    public function actionFiltersCategory()
    {
        $user_ids = $this->activeUserIds();
        $postData = Yii::$app->request->post();
        $langsKey = $this->langsKey($postData['lang']);
        $products = Product::find()
        ->andWhere( ['product_type_id'=>$postData['id'], 'user_id'=>$user_ids] )
        ->limit($this->offset);
        if($postData['filterCategory']!==''){
            $products->andWhere(['category_id'=>$postData['filterCategory']]);
        }
        if($postData['filterPrice']!==''){
            $prices = explode("-", $postData['filterPrice']);
            $products->andWhere(['>=', 'price_usd', (int)$prices[0]]);
            $products->andWhere(['<=', 'price_usd', (int)$prices[1]]);
        }
        if($postData['filterRegion']!==''){
            $products->andWhere(['region_id' => (int)$postData['filterRegion']]);
        }
        $count = $products->count();

        if($postData['orderType']=='SORT_DESC'){
            $products->orderBy([$postData['orderName'] => SORT_DESC]);
        }else{
            $products->orderBy([$postData['orderName'] => SORT_ASC]);
        }

        $products = $products->all();

        if($count!==0){
            $products = $this->productData($products, $langsKey);
            $productsJson = JSON::encode($products);
        }else{
            $productsJson = '';
        }


        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return [
            'products' => $productsJson,
            'count' => $count,
        ];
    }

    public function actionLoadMore()
    {
        $user_ids = $this->activeUserIds();
        $postData = Yii::$app->request->post();
        $langsKey = $this->langsKey($postData['lang']);

        $products = Product::find()
        ->andWhere( ['product_type_id'=>$postData['id'], 'user_id'=>$user_ids] )
        ->limit($this->offset);
        if($postData['filterCategory']!==''){
            $products->andWhere(['category_id'=>$postData['filterCategory']]);
        }
        if($postData['filterPrice']!==''){
            $products->andWhere(['<=', 'price_usd', (int)$postData['filterPrice']]);
        }
        $count = $products->count();

        if($postData['orderType']=='SORT_DESC'){
            $products->orderBy([$postData['orderName'] => SORT_DESC]);
        }else{
            $products->orderBy([$postData['orderName'] => SORT_ASC]);
        }

        $products = $products->all();

        if($count!==0){
            $products = $this->productData($products, $langsKey);
            $productsJson = JSON::encode($products);
        }else{
            $productsJson = '';
        }


        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return [
            'products' => $productsJson,
            'count' => $count,
        ];
    }


    function subCategory($arrSubCat, $langsKey=null){
        $subCats = [];

        foreach ($arrSubCat as $key=>$subCat) 
        {
            $subCats[$key]['name'] = $langsKey!==null ? $subCat->translation[$langsKey]->name : $subCat->name;
            $subCats[$key]['id'] = $subCat->id;       
        }
        return $subCats;
    }

    function productData($products, $lang_key)
    {
        
        $ArrProducts = [];
        
        foreach ($products as $key => $product) {
            $ArrProducts[$key]['name'] = $product->translation[$lang_key]->name;
            $ArrProducts[$key]['id'] = $product->id;
            $ArrProducts[$key]['category'] = isset($product->translationCategory[$lang_key]->name) ? $product->translationCategory[$lang_key]->name : '&#8212;';
            $ArrProducts[$key]['img'] = $product->img_src;
            $ArrProducts[$key]['object_area'] = isset($product->object_area) ? $product->object_area : '&#8212;';
            $ArrProducts[$key]['region'] = isset($product->region->name) ? $product->region->name : '&#8212;';
            $ArrProducts[$key]['currenc'] = $product->currenc->name;
            $ArrProducts[$key]['price'] = $product->price;
            $ArrProducts[$key]['room'] = $product->prodAttr->room;
            $ArrProducts[$key]['bathroom'] = $product->prodAttr->bathroom;
        }
        return $ArrProducts;
    }
    function langsKey($lang_local)
    {
        $langs = Lang::find()->all();
        foreach ($langs as $key => $lang) {
            if($lang->local==$lang_local){

                $lang_key = $key;
            }
        }
        return $lang_key;
    }
    public function activeUserIds(){
        // select only active users
        $activeUsers = \app\models\UserPakets::find()->select(['user_id'])->where(['status'=>'10'])->asArray()->all();
        $user_ids = \yii\helpers\ArrayHelper::getColumn($activeUsers, 'user_id');

        $activeOwners = \app\models\User::find()->select(['id'])->where(['status'=>'10', 'type'=>'owner'])->asArray()->all();
        $owner_ids = \yii\helpers\ArrayHelper::getColumn($activeOwners, 'id');

        $ids = \yii\helpers\ArrayHelper::merge($user_ids, $owner_ids);
        return $ids;
    }
}