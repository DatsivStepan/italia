<?php

namespace app\controllers;


use app\models\Setting;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Product;
use app\models\Category;
use app\models\Pages;
use app\models\Partners;
use app\models\Reviews;
use app\models\Lang;
use app\models\States;
use app\models\UserInfo;
use yii\data\ActiveDataProvider;
use app\modules\administration\models\Pricing;



class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post','get'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionInvesticii()
    {
        $page = Pages::find()->where( ['slug'=>'investicii'] )->one();
        $this->view->params['menu_item']['investicii'] = 'headMenu';
        return $this->render('investicii', [
            'page'=> $page,
            'title'=> $page->curentLangPages[0]->name,
        ]);
    }
    public function actionUslugi()
    {
        $page = Pages::find()->where( ['slug'=>'uslugi'] )->one();
        $this->view->params['menu_item']['uslugi'] = 'headMenu';
        return $this->render('uslugi', [
            'page'=> $page,
            'title'=> $page->curentLangPages[0]->name,
        ]);
    }

    public function actionForOwners()
    {
        $page = Pages::find()->where( ['slug'=>'owners'] )->one();
        $this->view->params['menu_item']['owners'] = 'headMenu';
        return $this->render('owners', [
            'page'=> $page,
            'title'=> $page->curentLangPages[0]->name,
        ]);
    }

    public function actionAbout()
    {
        $howBuy = Pages::find()->where( ['slug'=>'how_buy'] )->one();
        $pos=strpos($howBuy->curentLangPages[0]->content, ' ', 1600);
        $strHowBuy = substr($howBuy->curentLangPages[0]->content,0,$pos);

        $aboutProg = Pages::find()->where( ['slug'=>'about_prog'] )->one();
        $ourPhilosof = Pages::find()->where( ['slug'=>'our_philosof'] )->one();
        $partners = Partners::find()->limit(5)->all();
        $this->view->params['menu_item']['about'] = 'headMenu';
        return $this->render('about',[
            'howBuy'=> $howBuy,
            'howBuyContent' => $strHowBuy,
            'aboutProg'=> $aboutProg,
            'ourPhilosof'=> $ourPhilosof,
            'partners'=> $partners,
            'title'=> $aboutProg->curentLangPages[0]->name,
        ]);
    }
    public function actionHowBuy()
    {
        $howBuy = Pages::find()->where( ['slug'=>'how_buy'] )->one();
        return $this->render('investicii', [
            'page'=> $howBuy,
            'title'=> $howBuy->curentLangPages[0]->name,
        ]);
    }
    public function actionPartners()
    {
        $partners = Partners::find()->all();
        $aboutPartner = Pages::find()->where( ['slug'=>'about_partner'] )->one();
        return $this->render('partners', [
            'partners'=> $partners,
            'aboutPartner'=> $aboutPartner,
            'title'=> Yii::t('main','Our_partners'),
        ]);
    }

    public function actionReviews()
    {
        $langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
        $newReview = new Reviews();
        
        if(Yii::$app->request->post()&&!Yii::$app->user->isGuest)
        {
            $newReview->load(\Yii::$app->request->post());
            if($newReview->save()){
                Yii::$app->session->setFlash('added');
                return $this->redirect($langLink.'/site/reviews');          
            }
  
        }
        if(!Yii::$app->user->isGuest and Yii::$app->user->identity->type=='admin'){
            if(isset($_GET['delete']))
            {
                $review = Reviews::find()->where(['id'=>$_GET['delete']])->one();
                $review->delete();
                Yii::$app->session->setFlash('deleted');
                return $this->redirect($langLink.'/site/reviews');
            }
            elseif(isset($_GET['confirm']))
            {
                $review = Reviews::find()->where(['id'=>$_GET['confirm']])->one();
                $review->status = 1;
                $review->save();
                Yii::$app->session->setFlash('confirm');
                return $this->redirect($langLink.'/site/reviews');
            }
            $reviewsFind = Reviews::find()->orderBy('status');
            $modelReviews = new ActiveDataProvider(['query' => $reviewsFind, 'pagination' => ['pageSize' => 18]]);
            $reviews = $modelReviews->getModels();
            $isAdmin = true;
            $isLogined = true;
        }else{
            $reviewsFind = Reviews::find()->where(['status'=>1])->orderBy(['date_create' => SORT_DESC]);
            $modelReviews = new ActiveDataProvider(['query' => $reviewsFind, 'pagination' => ['pageSize' => 18]]);
            $reviews = $modelReviews->getModels();
            $isAdmin = false;
            if(!Yii::$app->user->isGuest){
                $isLogined = true;
            }else{
                $isLogined = false;
            }            
        }

        return $this->render('reviews', [
            'pagination' => $modelReviews->pagination,
            'reviews'=> $reviews,
            'isAdmin'=> $isAdmin,
            'isLogined'=> $isLogined,
            'newReview'=> $newReview,
            'title'=> Yii::t('main','Reviews'),
        ]);
    }

    public function actionIndex()
    {
        $user_ids = $this->activeUserIds();

        $newProducts = Product::find()
        ->limit(8)
        ->andWhere(['user_id'=>$user_ids])
        ->orderBy([
            'date_create' => SORT_DESC
              ])
        ->all();
        
        $products = Product::find()->andWhere(['user_id'=>$user_ids])->all();
        $product_low = Product::find()
        ->limit(8)
        ->andWhere(['<=', 'price_usd', 150000])
        ->andWhere(['user_id'=>$user_ids])
        ->orderBy(['date_create' => SORT_DESC])->all();
        $product_vip = Product::find()
        ->limit(8)
        ->andWhere(['vip'=>1, 'user_id'=>$user_ids])
        ->orderBy(['date_create' => SORT_DESC])->all();

        $arrStates = States::find()->where(['country_id' => 107])->all();
        $categories = Category::find()->all();
        $this->view->params['menu_item']['index'] = 'headMenu';
        return $this->render('index', [
            'newProducts' => $newProducts,
            'products'=> $products,
            'categories'=> $categories,
            'product_low'=> $product_low,
            'product_vip'=> $product_vip,
            'arrStates'=> $arrStates,
            'title'=> Yii::t('main','home'),
        ]);
    }

    public function actionSearch()
    {
        if(Yii::$app->request->post())
        {
            $post_data = Yii::$app->request->post();

            $products = Product::find()
            ->joinWith('translation')
            ->andWhere(['like', 'name', $post_data['name']]);

            if($post_data['price']!==''){
                $prices = explode("-", $post_data['price']);
                $products->andWhere(['>=', 'price_usd', (int)$prices[0]]);
                $products->andWhere(['<=', 'price_usd', (int)$prices[1]]);
            }

            if($post_data['area']!==''){
                $areas = explode("-", $post_data['area']);
                $products->andWhere(['>=', 'object_area', (int)$areas[0]]);
                $products->andWhere(['<=', 'object_area', (int)$areas[1]]);
            }

            if($post_data['region']!==''){
                $products->andWhere(['region_id' => (int)$post_data['region']]);
            }
            if($post_data['category']!==''){

                $categories = Category::find()
                ->joinWith('translation')
                ->where([ 'like', 'category_content.name', $post_data['category'] ])
                ->all();
                $arrCategories = [];
                foreach ($categories as $category) {
                    $arrCategories[] = $category->id;
                }
                
                $products->andWhere([ 'category_id'=>$arrCategories ]);
            }
            
            $modelProducts = new ActiveDataProvider(['query' => $products, 'pagination' => ['pageSize' => 30]]);

            return $this->render('search', [
                'modelProducts' => $modelProducts->getModels(),
                'pagination' => $modelProducts->pagination
            ]);
        }
        else
        {
            $modelProducts = [];
            $pagination = [];

            return $this->render('search', [
                'modelProducts' => $modelProducts,
                'pagination' => $pagination
            ]);
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
//    public function actionLogin()
//    {
//        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $model = new LoginForm();
//        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
//        }
//        return $this->render('login', [
//            'model' => $model,
//        ]);
//    }
    public function actionLogin() {
        $serviceName = \Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {
//                  var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes()); exit;

                    $identity = User::findByEAuth($eauth);
                    $profile = $eauth->getAttributes();
                    //var_dump($profile);exit;
                    if(!User::find()->where(['username' => $profile['name'], 'email' => (isset($profile['email']))?$profile['email']:$profile['id']])->exists()){
                        $model = new User();
                        $model->username = (isset($profile['name']))?$profile['name']:$profile['id'];
                        $model->password = \Yii::$app->security->generateRandomString(8);
                        $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);
                        $model->auth_key = \Yii::$app->security->generateRandomString(8);
                        $model->username = $profile['name'];
                        $model->email = (isset($profile['email']))?$profile['email']:$profile['id'];
                        $model->signup_type = $eauth->getServiceName();
                        $model->social_id = (isset($profile['id']))?$profile['id']:'';
                        if ($model->save()) {
                          if (Yii::$app->getUser()->login($model)) {
                            $eauth->redirect();
                          }
                        }
                    }else{
                        $model = User::find()->where(['social_id' => (isset($profile['id']))?$profile['id']:$profile['id'],'username' => $profile['name'], 'email' => (isset($profile['email']))?$profile['email']:$profile['id']])->one();
                        if (Yii::$app->getUser()->login($model)) {
                          $eauth->redirect();
                        }
                    }
                    //Yii::$app->getUser()->login($identity);

                    // special redirect with closing popup window
//                    $eauth->redirect();
                }else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }
        
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
        
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $howBuy = Pages::find()->where( ['slug'=>'how_buy'] )->one();
        $pos=strpos($howBuy->curentLangPages[0]->content, ' ', 1600);
        $strHowBuy = substr($howBuy->curentLangPages[0]->content,0,$pos);

        $this->view->params['menu_item']['contact'] = 'headMenu';
        return $this->render('contact', 
            [ 
                'howBuy'=> $howBuy,
                'howBuyContent' => $strHowBuy,
                'title'=> Yii::t('main','contacts'),
            ]
        );
    }
    
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionSignup(){
        $langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(Url::home());
        }
            $model = new User();

            $request = Yii::$app->request;
            $postedForm = $request->post('ContactForm');

            $model->scenario = 'signup';
            if ($model->load($request->post())){
                $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);

                $randStr = \Yii::$app->security->generateRandomString();
                $model->auth_key = \Yii::$app->security->generatePasswordHash($randStr);
                $model->status = 0;
                $model->type = '';
                $model->signup_type = '';
                $model->secret_key = '';
                $model->update_at = '';
                $model->password_reset_token = '';
                $model->access_token = '';
                $model->social_id = '';
                if ($model->save()) {
                        if (Yii::$app->getUser()->login($model)) {
                                $this->createUserInfoModel();
                                if (isset($_GET['paket-id'])) 
                                {
                                    if($_GET['paket-id']!=='0'){
                                        return $this->redirect($langLink.'/pay/pay-first-steep?model=tarif&type=tarif_regist&id='.$_GET['paket-id']);
                                    }else{
                                        return $this->redirect($langLink.'/pay/pay-first-steep?model=tarif&type=premium');
                                    } 
                                }
                                elseif(isset($_GET['owner'])){
                                    $model->type = 'owner';
                                    $model->save();
                                    return $this->redirect($langLink.'/prifile');
                                }
                        }
                                
                }
            }

            return $this->render('signup', [
                'newModel' => $model,
            ]);
    }
    public function createUserInfoModel(){
        $model = new UserInfo();
        $model->scenario = 'add';
        $model->user_id = Yii::$app->user->identity->id;
        $model->save();
    }
    public function activeUserIds(){
        // select only active users
        $activeUsers = \app\models\UserPakets::find()->select(['user_id'])->where(['status'=>'10'])->asArray()->all();
        $user_ids = \yii\helpers\ArrayHelper::getColumn($activeUsers, 'user_id');

        $activeOwners = \app\models\User::find()->select(['id'])->where(['status'=>'10', 'type'=>'owner'])->asArray()->all();
        $owner_ids = \yii\helpers\ArrayHelper::getColumn($activeOwners, 'id');

        $ids = \yii\helpers\ArrayHelper::merge($user_ids, $owner_ids);
        return $ids;
    }
}
