<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Product;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Category;
use app\models\Lang;
use app\models\States;
use app\models\UserPakets;
use app\modules\administration\models\EmailPackets;
use app\models\UserEmailPackets;
use app\models\EmailArchive;
use app\models\UserOtherPays;
use app\models\PremiumPacket;
use app\models\Partners;
use yii\data\ActiveDataProvider;
use app\modules\administration\models\Pricing;
use \Datetime;

class PayController extends Controller
{
    /**
     * @inheritdoc
     */
    public $offset = 12;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post','get'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function actionPayFirstSteep()
    {  
        $servId = isset($_GET['id']) ? $_GET['id'] : null;
        $modeltype = $_GET['model'];
        $packetType = isset($_GET['type']) ? $_GET['type'] : null;

        // if user can add to vip whithout pay
        if($modeltype == 'top' && $this->ifIsPremiumAndVip($modeltype, $packetType, $servId))
        {
            \Yii::$app->session->setFlash('paket_sucses_active');
            return $this->redirect('/profile');
        }
        else
        {
            if( !\Yii::$app->user->isGuest && Yii::$app->user->identity->auth_key)
            {
                $sercDetail = $this->generateServiceDetail($modeltype, $packetType, $servId);

                $payArr = Yii::$app->paypal->payServ((int)$sercDetail['price'], $sercDetail['name'], Yii::$app->user->identity->auth_key);          

                $hrefRedirect = $payArr->links[1]->href;
                $userId = Yii::$app->user->identity->id;
                $paymentId = $payArr->id;
                
                if($payArr->payer->payer_info->payer_id = Yii::$app->user->identity->auth_key)
                {
                    if($this->createServModel($modeltype, $packetType, $servId, $userId, $paymentId))
                    {
                        return $this->redirect($hrefRedirect);
                    }
                }      
            } 
        }

            
    }

    public function actionSuccess()
    {  
        if(!\Yii::$app->user->isGuest){
            $token = $_GET['token'];
            $PayerID = $_GET['PayerID'];
            $paymentId = $_GET['paymentId'];

            $userId = Yii::$app->user->identity->id;            
            $whereArr = [
                'user_id'=>$userId,
                'payment_id'=> $paymentId
            ];

            $servSucctModel = UserPakets::find()->where($whereArr)->one();
            $type = 'tarif';
            if($servSucctModel==NULL){
                $servSucctModel = UserEmailPackets::find()->where($whereArr)->one();
                $type = 'packet';
            }
            
            if ($servSucctModel==NULL) {
                $servSucctModel = UserOtherPays::find()->where($whereArr)->one();
                $type = $servSucctModel->type;
            }

            if($servSucctModel)
            {
                $payArr = Yii::$app->paypal->payAccept($paymentId, $PayerID);

                if($payArr->state=='approved')
                {
                    if ($type == 'tarif' || $type == 'packet') 
                    {

                        if($type == 'tarif'){
                            if($servSucctModel->type = 'premium')
                            {
                                $this->addUserToPartners($servSucctModel->user_id);
                            }
                            $servSucctModel->status = $this->generateStatus('status');
                            $servSucctModel->date_activate = $this->generateStatus('date');
                            $this->userActivator();                       
                        }elseif ($type == 'packet') {
                            $servSucctModel->status = 10;
                        }
                        $servSucctModel->save();

                        $this->sendSuccessEmail($servSucctModel->name);
                    }
                    else{
                        if($type == 'email'){
                            $servSucctModel->status = 10;
                            $this->sendEmailWithContactData($servSucctModel->user_id, $servSucctModel->paket_id);
                        }elseif ($type == 'top') {
                            $servSucctModel->status = 10;
                            $this->productToTop($servSucctModel->user_id, $servSucctModel->paket_id);
                        }
                        $servSucctModel->save();
                    }
                    
                    \Yii::$app->session->setFlash('paket_sucses_active');
                    return $this->redirect('/profile'); 
                }

            }
            
        }
        
    }


    public function generateServiceDetail($modeltype, $servType, $servId){
        if($modeltype=='tarif')
        {
            if($servType == 'premium'){
                $servModel = PremiumPacket::find()->one();
            }else{
                $servModel = Pricing::find()->where(['id'=>(int)$servId])->one();
            }
            
            switch ($servType) 
            {
                case "tarif_regist":
                    $servDetail['price'] = $servModel->price*$servModel->months_count;
                    $servDetail['name'] = $servModel->name;
                    $servDetail['months'] = $servModel->months_count;
                    break;
                case "tarif_one":
                    $servDetail['price'] = $servModel->price_to_one;
                    $servDetail['name'] = $servModel->name. ' ( на один месяц )';
                    $servDetail['months'] = 1;
                    break;
                case "tarif_standart":
                    $servDetail['price'] = $servModel->price_to_long*$servModel->mouth_to_long;
                    $servDetail['name'] = $servModel->name. ' ( на '.$servModel->mouth_to_long.' месяцев, )';
                    $servDetail['months'] = $servModel->mouth_to_long;
                    break;
                case "premium":
                    $servDetail['price'] = $servModel->price;
                    $servDetail['name'] = $servModel->name. ' ( на один месяц )';
                    $servDetail['months'] = 1;
                    break;
                default:
                    
            }

        }elseif ($modeltype=='packet') {

            $servModel = EmailPackets::find()->where(['id'=>(int)$servId])->one();
            $servDetail['price'] = $servModel->price;
            $servDetail['name'] = $servModel->name;

        }elseif ($modeltype=='email') {
            $servDetail['price'] = 2;
            $servDetail['name'] = 'Оплата 1 email с контактными данными';

        }elseif ($modeltype=='top') {

            $servDetail['price'] = 10;
            $servDetail['name'] = 'Оплата за поднятия в ТОП';
        }
        
        return $servDetail;
    }

    public function createServModel($modeltype, $type, $paketId, $userId, $paymentId){
        if($modeltype=='tarif')
        {
            $paketsModel = new UserPakets();
            $paketsModel->paket_id = $type=='premium' ? $paketId : 0;
            $paketsModel->user_id = $userId;
            $paketsModel->payment_id = $paymentId;
            $paketsModel->type = $type;
            $paketsModel->status = 0;

        }elseif ($modeltype=='packet') {

            $paketsModel = new UserEmailPackets();
            $paketsModel->paket_id = $paketId;
            $paketsModel->user_id = $userId;
            $paketsModel->payment_id = $paymentId;

        }elseif ($modeltype=='email') {

            $paketsModel = new UserOtherPays();
            $paketsModel->paket_id = $paketId;
            $paketsModel->user_id = $userId;
            $paketsModel->payment_id = $paymentId;
            $paketsModel->type = 'email';

        }elseif ($modeltype=='top') {

            $paketsModel = new UserOtherPays();
            $paketsModel->paket_id = $paketId;
            $paketsModel->user_id = $userId;
            $paketsModel->payment_id = $paymentId;
            $paketsModel->type = 'top';
        }

        if($paketsModel->save()){
            return true;
        }else{
            return false;
        }
    }

    public function generateStatus($type){
        $userHasPaket = UserPakets::find()->where(['user_id'=>Yii::$app->user->identity->id, 'status'=>'10'])->one();

        if($userHasPaket)
        {
            $packetStatus = 5;
            $packetDate = date("Y-m-d");
        }else
        {
            $packetStatus = 10;
            $packetDate = date("Y-m-d");
        }

        if($type == 'status'){
            return $packetStatus;
        }elseif ($type == 'date') {
            return $packetDate;
        }else{
            return '';
        }
    }
    public function sendSuccessEmail($name){

        $to = Yii::$app->user->identity->email;
        $subject = 'Ваш пакет оплачен';
        $message = '
        <html>
                <head>                                        
                    <title>' . $subject . '</title>
                </head>
            <body>
                <p>Ваш пакет "'.$name.'"успешно оплачен</p>   
            </body>
        </html>';
        $headers = "Content-type: text/html; charset=utf-8 \r\n";

        mail($to, $subject, $message, $headers);       
    }

    public function userActivator(){
        if(Yii::$app->user->identity->status!=='10')
        {
            $userModel = User::find()->where(['id'=>Yii::$app->user->identity->id])->one();
            $userModel->status = '10';
            $userModel->save();
        }
    }

    public function sendEmailWithContactData($userId, $paketId){
        $emailArchive = EmailArchive::find()->where(['id'=>$paketId])->one();
        
        if(!empty($emailArchive)){
            
            $message = $emailArchive->message;

            $to = Yii::$app->user->identity->email;
            $subject = 'Ваши emails с контактными данными';              
            $headers = "Content-type: text/html; charset=utf-8 \r\n";

            mail($to, $subject, $message, $headers);
        }
    }

    public function productToTop($userId, $productId){
        $productModel = Product::find()->andWhere(['id'=> $productId, 'user_id'=> $userId])->one();
        $productModel->vip = 1;
        $productModel->scenario = 'new_product';
        $productModel->save();
    }

    public function ifIsPremiumAndVip( $modeltype, $packetType, $servId ){

        $userPaket = UserPakets::find()->where(['user_id'=>Yii::$app->user->identity->id, 'status'=>'10'])->one();
        if($userPaket and 
        $userPaket->type == 'premium' and 
        $modeltype == 'top' and 
        $servId and
        $userPaket->premiumVipCount > 0){

            $this->productToTop(Yii::$app->user->identity->id, $servId);
            \Yii::$app->session->setFlash('paket_sucses_active');
            return true;
        }
        else
        {
            return false;
        }


    }

    public function addUserToPartners($userId)
    {
        $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();

        $ifUserIsInPartners = Partners::find()->where(['user_id'=>$userId])->one();

        if(!$ifUserIsInPartners)
        {
            $avatar = 'images/default_avatar.jpg';
            if ($modelUser->userinfo and $modelUser->userinfo->avatar != '') { 
                $avatar = $modelUser->userinfo->avatar;
            }

            $partnerModel = new Partners();
            $partnerModel->name = $modelUser->userinfo->company_name;
            $partnerModel->img = $avatar;
            $partnerModel->user_id = $userId;
            $partnerModel->save();
        }
    }

}