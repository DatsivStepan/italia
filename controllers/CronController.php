<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\Json;
use app\models\User;
use app\models\Lang;
use app\models\UserInfo;
use yii\data\ActiveDataProvider;
use app\modules\administration\models\Pricing;
use app\modules\administration\models\EmailPackets;
use app\models\UserPakets;
use app\models\UserOtherPays;
use \Datetime;


class CronController extends Controller
{

	public function actionIndex()
    {
    	$usersActivePackets = UserPakets::find()->where(['status'=>10])->all();

    	foreach ($usersActivePackets as $userActivePacket) 
    	{
			$packetData = Pricing::find()->where(['id'=>$userActivePacket->paket_id])->one();

			if($userActivePacket->type == 'tarif_regist'){
				$month = $packetData->months_count;
			}elseif ($userActivePacket->type == 'tarif_one' || $userActivePacket->type == 'premium') {
				$month = 1;
			}elseif ($userActivePacket->type == 'tarif_standart') {
				$month = $packetData->mouth_to_long;
			}

			$dateActivate = $userActivePacket->date_activate;

    		$dateAvailable = new DateTime($dateActivate);
			$dateAvailable->modify('+'.$month.' month');
			$dateAvailable = $dateAvailable->format('Y-m-d');

			if( date("Y-m-d") > $dateAvailable){
				$userActivePacket->status = 0;
				$userActivePacket->save();

				$usersNotActivePackets = UserPakets::find()->where(['status'=>5])->orderBy('date_create')->one();
				$usersNotActivePackets->status = 10;
				$usersNotActivePackets->save();
			}
    	}

    	$userTopProducts = UserOtherPays::find()->where(['status'=>10])->all();

		foreach ($userTopProducts as $userTopProduct) 
    	{
    		$dateActivate = $userTopProduct->date_create;

    		$dateAvailable = new DateTime($dateActivate);
    		$dateAvailable->modify('+1 month');
    		$dateAvailable = $dateAvailable->format('Y-m-d');

    		if( date("Y-m-d") > $dateAvailable){
				$userTopProduct->status = 0;
				$userTopProduct->save();
			}
    	}    	
    	   	
    }
}