var markers = [];
var map = '';
function initMap(address, LatLng) {
    var myLatLng = {lat: 55.7494718, lng: 37.3516334};
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: myLatLng
    });
    var input = document.getElementById('searchTextField');
    if (input.value != '') {
        var address = input.value;
        var geocoder = new google.maps.Geocoder();
        //var address = $(this).val();
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var Local = results[0].geometry.location;
                var latLng = {lat: parseFloat(Local.lat()), lng: parseFloat(Local.lng())};
                var marker = new google.maps.Marker({
                    position: latLng,
                    zoom: 6,
                    map: map,
                    //   title: 'Hello World!'
                });
                map.setCenter(latLng);
                map.setZoom(14);
            } else {
                alert('error');
            }
        });
    }


    var options = {
        //types: ['(cities)'],
        componentRestrictions: {country: "ru"}
    };
    // var autocomplete = new google.maps.places.Autocomplete(input, options);
    //
    // autocomplete.addListener('place_changed', function () {
    //     setMapOnAll(null);
    //     var input = document.getElementById('saveStatusField').value = 'true';
    //     var place = autocomplete.getPlace();
    //     map.setCenter(place.geometry.location);
    //     var marker = new google.maps.Marker({
    //         position: place.geometry.location,
    //         map: map,
    //         title: 'Hello World!'
    //     });
    //     markers.push(marker);
    //
    // });

}

function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

$(document).ready(function () {
    if ($('#map').length) {
        initMap();
    }
})
