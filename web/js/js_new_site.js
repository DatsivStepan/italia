$(document).ready(function (){
   $('.owl-carousel').owlCarousel({
       nav: false,
       responsive:{
           420:{
               items:4
           }
       }
    });
    
    $('.style-img-select-my').hover(function (){
        $(this).find('.img-class-watch').css('-webkit-filter','brightness(40%)');
        $(this).find('.watch-icon').addClass('watch-icon-hover');
    },
    function (){
        $(this).find('.img-class-watch').css('-webkit-filter','brightness(100%)');
        $(this).find('.watch-icon').removeClass('watch-icon-hover');
    });
    
    $('.img-class-watch').click(function(){
        var src = $(this).attr('src');
        $('.style-img-select').find('img').attr('src', src);
    });
    
    var $grid_demo_multiple_columns = $('#masonry_hybrid_demo1');
    var grid = new MasonryHybrid($grid_demo_multiple_columns, {col:3, space: 30}); 
    
    var $grid_demo_multiple_columns = $('#masonry_hybrid_demo2');
    var grid = new MasonryHybrid($grid_demo_multiple_columns, {col:5, space: 30}); 
});


