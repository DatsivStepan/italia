$(document).ready(function(){
    
    $(".click").click(function(){
        $('#show-' + $(this).data('togg')).toggle();
    });
    $('.owl-carousel:not(.owl-carousel-product)').owlCarousel({
        margin:10,
        nav:true,
        responsiveClass:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:false,
        navText:["<img src='/img/left_icon.png'>","<img src='/img/right-icon.png'>"],
        responsive:{
            420:{
                items:1
            },
            768:{
                items:2
            },
            992:{
                items:3
            },
            1200:{
                items:4
            }
        }
    });

    $('.owl-carousel.owl-carousel-product').owlCarousel({
        margin:10,
        nav:true,
        responsiveClass:true,
        navText:["<img src='/img/left_icon.png'>","<img src='/img/right-icon.png'>"],
        responsive:{
            420:{
                items:1
            },
            768:{
                items:2
            },
            992:{
                items:3
            },
            1200:{
                items:4
            }
        }
    });
    
    $('.katalog-ul-style li').click(function(){
        $('.tab-katalog').css('display', 'none');
        $('#katalog-panel' + $(this).data('katalog')).css('display', 'block');
        $('.katalog-ul-style li a').removeClass('katalog-active');
        $(this).find('a').addClass('katalog-active');
    });
    
    $('.menu-ul li').click(function (){
        $('.menu-ul li a').removeClass('headMenu');
        $(this).find('a').addClass('headMenu');
    });
    
    $(".content-class-style").hover(function (){
        $(this).css("background", "rgba(56,56,56,1)");
        $(this).find(".product-hr").css("background", "#626262");
        $(this).find('.edit_button').removeClass("button_display");
        $(this).find('.unistall_button').removeClass("button_display");
        $(this).find("span").addClass("oneColor");
        $(this).find("p").addClass("oneColor");
        $(this).find(".change-hover").css("background", "rgba(56,56,56,1)");
        $(this).find(".change_hover_all").css("background", "rgba(56,56,56,1)");
        $(this).find(".shower_class").attr("src", "/img/shower_hover.png");
        $(this).find(".bed_class").attr("src", "/img/bed_hover.png");
    },
    function (){
        $(this).css("background", "rgba(255,255,255,1)");
        $(this).find(".product-hr").css("background", "#e8e8e8");
        $(this).find('.edit_button').addClass("button_display");
        $(this).find('.unistall_button').addClass("button_display");
        $(this).find("span").removeClass("oneColor");
        $(this).find("p").removeClass("oneColor");
        $(this).find(".change-hover").css("background", "rgba(255,255,255,1)");
        $(this).find(".change_hover_all").css("background", "transparent");
        $(this).find(".shower_class").attr("src", "/img/shower_icon.png");
        $(this).find(".bed_class").attr("src", "/img/bed_icon.png");
    }
    );
    $(".tabs-style li").click(function(){
        $(".tab").css("display","none");
        $("#panel" + $(this).data("id")).css("display","block");
        $('.tabs-style li a').removeClass('in-focus');
        $(this).find('a').addClass('in-focus');
    });
    
    $('.tabs-inside1 li').click(function(){
        $('.tabs1').css("display","none");
        $('#ar-panel' + $(this).data("tab")).css("display","block");
        $('.tabs-inside1 li a').removeClass('setActive');
        $(this).find('a').addClass('setActive');
    });
    $('.tabs-inside2 li').click(function(){
        $('.tabs2').css("display","none");
        $('#pr-panel' + $(this).data('tt')).css("display","block");
        $('.tabs-inside2 li a').removeClass('setActive');
        $(this).find('a').addClass('setActive');
    
    });
    $('.arenda-ta-style li').click(function(){
       $('.sel-tab-panel').css('display', 'none');
       $('#arenda-panel' + $(this).data('arenda')).css('display', 'block');
       $('.arenda-ta-style li a').removeClass('tab-active-arend');
       $(this).find('a').addClass('tab-active-arend');
    });
    
    $(document).on('click','.click-registration', function (){
        $('.registration-panel').css('display','block');
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        
        return false;
    });
    
    $('.close-registration').click(function (){
        $('.registration-panel').css('display','none');        
    });
    
    $(document).on('click','.click-call-back', function (){
        $('.call-back-panel:not(.call-to-sell)').css('display','block');
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        
        return false;
    });

    $('.close-call-back').click(function (){
        $('.call-back-panel').css('display','none');     
    });

    $(document).on('click','.click-call-back-2', function (){
        $('.call-to-sell').css('display','block');
        
        return false;
    });

    
    

    $('.close-call-popup').click(function (){
        $('.call-back-popup').css('display','none');
        $.ajax({
            type: 'POST',
            url: '../../../product/unsetpopup',
            data: 'popup=no'
        });      
    });

    setTimeout(function(){
        var popup = $( ".call-back-popup" );
        if ( !(popup.attr('nopopup')) )
        {
            popup.css('display','block');
            $('body,html').animate({
                scrollTop: 0
            }, 400);
        }
    },30000);

    $('.open-call-back-popup').click(function (){
        var popup = $( ".call-back-popup" );
        popup.css('display','block');
        $('body,html').animate({
            scrollTop: 0
        }, 400);    
    });
    
    new MasonryHybrid($('#masonry_hybrid_demo1'), {col:3, space: 30}); 
    
    new MasonryHybrid($('#masonry_hybrid_demo2'), {col:5, space: 80}); 
    
    new MasonryHybrid($('#masonry_hybrid_demo3'), {col:5, space: 60});



        // The slider being synced must be initialized first
    function getGridSize() {
        return (window.innerWidth > 300) ? 4:3 ;
    }

        $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 137,
            itemMargin: 5,
            prevText: "",
            nextText: "",
            minItems: getGridSize(),
            maxItems: getGridSize(),
            asNavFor: '#slider'
        });



        $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel"
        });

    $('.style-img-select-my').hover(function (){
            $(this).find('.img-class-watch').css('-webkit-filter','brightness(60%)');
            $(this).find('.watch-icon').addClass('watch-icon-hover');
        },
        function (){
            $(this).find('.img-class-watch').css('-webkit-filter','brightness(100%)');
            $(this).find('.watch-icon').removeClass('watch-icon-hover');
        });
    /*$('#product-country_id').change(function(){
        var country_id = $(this).val();
        if(country_id!=='')
        {
            $.ajax({
                url: '/profile/product/get-regions',
                type: 'post',
                data: {
                         'country_id': country_id
                     },
                success: function (data) {
                  var stateData = JSON.parse(data.search);
                  $('#product-region_id').html(addOption(this, stateData, 'State'));
                  $('#product-region_id').prop('disabled', false);
                }
            });
        }
        else
        {
            $('#product-region_id').prop('disabled', true);
            $('#product-region_id').html('<option>Select State</option>');        
        }        
    });*/
    $('#product-region_id').change(function(){
        var region_id = $(this).val();
        if(region_id!=='')
        {
            $.ajax({
                url: '/profile/product/get-towns',
                type: 'post',
                data: {
                         'region_id': region_id
                     },
                success: function (data) {
                  var townData = JSON.parse(data.search);
                  $('#product-city_id').html(addOption(this, townData, 'City'));
                  $('#product-city_id').prop('disabled', false);
                }
            });
        }
        else
        {
            $('#product-city_id').prop('disabled', true);
            $('#product-city_id').html('<option>Select City</option>');        
        }        
    });
    $('#product-product_type_id').change(function(){
        var product_type_id = $(this).val();
        if(product_type_id!=='')
        {
            $.ajax({
                url: '/profile/product/get-sub-categoty',
                type: 'post',
                data: {
                         'product_type_id': product_type_id,
                         'lang': $('html').attr('lang')
                     },
                success: function (data) {
                  var subCatData = JSON.parse(data.search);
                  $('#product-category_id').html(addOption(this, subCatData, 'Categoty'));
                }
            });
        }        
    });

    $('#tabs-catalog > li > a').click(function(){
        if(!$(this).hasClass('katalog-active'))
        {

            $('#tabs-catalog > li > a').removeClass('katalog-active');
            $(this).addClass('katalog-active');
            var typeId = $(this).data('category');

            $.ajax({
                url: '/catalog/change-category',
                type: 'post',
                data:   {
                            'id': typeId,
                            'lang': $('html').attr('lang')
                        },
                success: function (data) {
                    var subCatData = JSON.parse(data.categories);
                    var productsData = JSON.parse(data.products);
                    $('#filters-section select[name=catalog-category]').html(addOption(this, subCatData, 'Category'));
                    $('#dunamic_container').html(createProductHtml(productsData));
                    $('#search_count span').html(data.count);
                    $('#catalog-reset').data('type', typeId);
                }
            });
            resetSetects();
        }

    });
    $('#catalog-reset').on('click', function(){
        var typeId = $(this).data('type');
        $.ajax({
            url: '/catalog/reset-category',
            type: 'post',
            data:   {
                        'id': typeId,
                        'lang': $('html').attr('lang')
                    },
            success: function (data) {
                var productsData = JSON.parse(data.products);
                $('#dunamic_container').html(createProductHtml(productsData));
                $('#search_count span').html(data.count);
                $('#load_more').data('offset', 4);
                delete typeId;
            }
        });
        resetSetects();
    });

    $('#filters-section select').on('change', function(){
        var typeId = $('#catalog-reset').data('type');
        var filterRegion = $('#filters-section select[name=catalog-region]').val();
        var filterCategory = $('#filters-section select[name=catalog-category]').val();
        var filterPrice = $('#filters-section select[name=catalog-price]').val();
        var orderType = $('#filters-section select[name=order-type]').val();
        var orderName = $('#filters-section select[name=order-name]').val();
        $.ajax({
            url: '/catalog/filters-category',
            type: 'post',
            data:   {
                        'id': typeId,
                        'lang': $('html').attr('lang'),
                        'filterRegion': filterRegion,
                        'filterCategory': filterCategory,
                        'filterPrice': filterPrice,
                        'orderType': orderType,
                        'orderName': orderName,

                    },
            success: function (data) {
                if(data.products!=='[]'){
                    var productsData = JSON.parse(data.products);
                    $('#dunamic_container').html(createProductHtml(productsData));
                }else{
                    $('#dunamic_container').html('<h1>Ничего не найденно</h1>');
                }                
                $('#search_count span').html(data.count);
                delete typeId;
            }
        });
    });


    $('#filters-category-section select').on('change', function(){
        var typeId = $('#category_data_cont').attr('typeid');
        var filterCategory = $('#category_data_cont').attr('catalogcategory');

        var orderType = $('#filters-category-section select[name=order-type]').val();
        var orderName = $('#filters-category-section select[name=order-name]').val();

        $.ajax({
            url: '/product/sort-filters',
            type: 'post',
            data:   {
                        'id': typeId,
                        'lang': $('html').attr('lang'),
                        'filterCategory': filterCategory,
                        'orderType': orderType,
                        'orderName': orderName,

                    },
            success: function (data) {
                console.log(data);
                if(data.products!=='[]'){
                    var productsData = JSON.parse(data.products);
                    $('#dunamic_container').html(createProductHtml(productsData));
                }else{
                    $('#dunamic_container').html('<h1>Ничего не найденно</h1>');
                }
                $('#dunamic_container').prepend('<p class=\"arenda-cont-title\">Результат поиска '+data.count+' </p>');              
                delete typeId;
            }
        });
    });


    $('#load_more').on('click', function(){
        var typeId = $('#catalog-reset').data('type');
        var filterRegion = $('#filters-section select[name=catalog-region]').val();
        var filterCategory = $('#filters-section select[name=catalog-category]').val();
        var filterPrice = $('#filters-section select[name=catalog-price]').val();
        var orderType = $('#filters-section select[name=order-type]').val();
        var orderName = $('#filters-section select[name=order-name]').val();
        $.ajax({
            url: '/catalog/load-more',
            type: 'post',
            data:   {
                        'id': typeId,
                        'lang': $('html').attr('lang'),
                        'filterRegion': filterRegion,
                        'filterCategory': filterCategory,
                        'filterPrice': filterPrice,
                        'orderType': orderType,
                        'orderName': orderName,
                    },
            success: function (data) {
                if(data.products!=='[]'){
                    var productsData = JSON.parse(data.products);
                    $('#dunamic_container').html(createProductHtml(productsData));
                }else{
                    $('#dunamic_container').html('<h1>Ничего не найденно</h1>');
                }
                delete typeId;
            }
        });
    });

});

function resetSetects()
{
    $("#filters-section select").each(
        function() {
            $(this).find('option:first').attr('selected','selected');
            $(this).val($(this).find('option:first').val());
        }
    );
}
function addOption(obj, arrOption, str)
{
    var strOptions = '<option value="">Select '+str+'</option>';
    arrOption.forEach(function(item, i, arrOption) {

      strOptions = strOptions+'<option value=\"'+item.id+'\">'+item.name+'</option>';
    });
    return strOptions;
}

function createProductHtml(products)
{
    var lang = $('html').attr('lang');
    lang = lang.replace(/-...?$/,'');

    var html = ``;
    var htmlContent = $('#dunamic_container > a').first().clone();

    products.forEach(function(product, i, products) {

        htmlContent.attr('href', lang+'/product/'+product.id);
        htmlContent.find('.hover-div-slider').attr('style', 'background-image: url(../images/product/'+product.img+');');
        htmlContent.find('.title_room').html(product.name);
        htmlContent.find('.title_category').html(product.category);
        htmlContent.find('.title_object_area').html(product.object_area);
        htmlContent.find('.title_region').html(product.region);
        htmlContent.find('.money_style').html(product.currenc+' '+product.price);
        htmlContent.find('.shower_bl > span').html(product.bathroom);
        htmlContent.find('.bed_bl > span').html(product.room);
        html = html + htmlContent.get(0).outerHTML;
                    
    });
    
    return html;
}



