$(document).ready(function(){
    $(document).on('click', '.send-question', function () {
        var form_data = $('#ask_guestion').serializeArray();
        $.ajax({
            type: 'POST',
            url: '../../../product/question',
            data: {form_data: form_data },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    swal("Повідомлення надіслано!", "", "success")
                    $('.question-seller').find('.form-style-kartochka').val("");
                }

            }
        });
    })
});

$(document).ready(function(){
    $(document).on('click', '.send-button-contact', function () {
        var form_data = $('#ask_guestion_site').serializeArray();
        $.ajax({
            type: 'POST',
            url: '../../../product/question',
            data: {form_data: form_data },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    swal("Повідомлення надіслано!", "", "success")
                    $('.question-contakt').find('.form-style-kartochka').val("");
                }

            }
        });
    })
});

$(document).ready(function(){
    $(document).on('click', '.subscribe', function () {
        var form_subscribe = $('#form_subscribe').serializeArray();
        $.ajax({
            type: 'POST',
            url: '../../../product/subscribe',
            data: {form_subscribe: form_subscribe },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    swal("Повідомлення надіслано!", "", "success")
                    $('#form_subscribe').find('.input-style-footer').val("");
                }

            }
        });
    })
});
$(document).ready(function(){
    $(document).on('click', '.send_texpod', function () {
        var texpod_form = $('#texpod_form').serializeArray();
        $.ajax({
            type: 'POST',
            url: '../../../product/texpod',
            data: {texpod_form: texpod_form },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    swal("Повідомлення надіслано!", "", "success")
                    $('#texpod_form').find('.clear_input').val("");
                }

            }
        });
    })
});

$(document).ready(function(){
    $(document).on('click', '.send_call_to_email', function () {
        var call_to_email = $('#call_to_email').serializeArray();
        $.ajax({
            type: 'POST',
            url: '../../../product/sendcall',
            data: {call_to_email: call_to_email },
            dataType: 'json',
            success: function (response) {
                $('.call-back-panel').css('display','none');  
                if (response.status == 'success') {
                    swal("Повідомлення надіслано!", "", "success")
                    $('#call_to_email').find('.clear_input').val("");
                }

            }
        });
    })
});

$(document).ready(function(){
    $(document).on('click', '.send_call_to_popup', function () {
        var call_to_email = $('#call_to_popup').serializeArray();
        $.ajax({
            type: 'POST',
            url: '../../../product/sendpopup',
            data: {call_to_email: call_to_email, dsfsf: "sdfsdf" },
            dataType: 'json',
            success: function (response) {
                $('.call-back-popup').css('display','none');  
                if (response.status == 'success') {
                    swal("Повідомлення надіслано!", "", "success")
                    $('#call_to_popup').find('.clear_input').val("");
                }

            }
        });
    })
});

$(document).ready(function(){
    $(document).on('click', '.send_call_to_selling', function () {
        var form = $('#call_to_selling');
        var call_to_selling = form.serializeArray();

        
        var formElems = document.getElementById("call_to_selling");

        var name = formElems.querySelector('input[name="name"]');
        var email = formElems.querySelector('input[name="email"]');
        var phone = formElems.querySelector('input[name="number"]');
        var text = formElems.querySelector('textarea[name="content"]');

        form.find('input[name="email"]').attr('required', '');
        form.find('input[name="number"]').attr('required', '');

        if (
                name.validity.valid &&
                ( 
                    ( (phone.validity.valid || phone.validity.valueMissing) && email.validity.valid )
                    ||
                    ( (email.validity.valid || email.validity.valueMissing) && phone.validity.valid )
                )
            ) 
        {

            form.find('input[name="email"]').removeAttr('required');
            form.find('input[name="number"]').removeAttr('required');
            if(!text.validity.valid){
                $results = false;
                return true;
            }else{
                $results = true;
            }            
        }else{
            $results = false;
        }
                

        if($results)
        {
            $.ajax({
                type: 'POST',
                url: '../../../product/sendcall-selling',
                data: {call_to_selling: call_to_selling },
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        $('.call-back-panel').fadeOut();
                        swal("Повідомлення надіслано!", "", "success")
                        $('#call_to_selling').find('.clear_input').val("");
                    }

                }
            });
            return false;
        }

    })
});
