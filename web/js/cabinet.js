$(document).ready(function () {
    
    if($('.addPhoto').length){
            var previewNode = document.querySelector("#template");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);
            var photosArray = [];
            if($('#product-image_array').val() != ''){
                photosArray = $.parseJSON($('#product-image_array').val());              
            }
            console.log(photosArray);
            var myDropzoneG = new Dropzone($('.addPhoto')[0], {
                url: "../../../profile/product/saveproductimg",
                maxFiles:10,
                previewTemplate: previewTemplate,
                previewsContainer: "#previews", 
                clickable: ".addPhoto" 
            });

            myDropzoneG.on("maxfilesexceeded", function(file) {
            });

            myDropzoneG.on("complete", function(response) {
                if (response.status == 'success') {
                    
                    $( ".addPhoto" ).each(function() {                        
                        var thisBackground = $(this).css('background-image');
                        if ( thisBackground.indexOf('default_avatar') !== -1 ){
                            photosArray.push(response.xhr.response);
                            $('#product-image_array').val(JSON.stringify(photosArray));
                            $(this).css('background-image', 'url(/images/product/'+response.xhr.response+')')
                            $(this).data('delete_status','yes')
                            $(this).parent().find('.deleteProductImage').data('name',response.xhr.response);
                            return false;
                        }
                    });
                    
                }else{
                    //$('.maxPhoto').show()
                }
            });

            myDropzoneG.on("removedfile", function(response) {
                if(response.xhr != null){
                    
                }
            });
            
            $(document).on('click','.deleteProductImage',function(){
                var shos = $(this).parent();
                var name_photo = $(this).data('name');
                console.log(name_photo);
                $.each( myDropzoneG.files, function( key, value ) {
                    if(value.xhr.response == name_photo){
                        myDropzoneG.removeFile(myDropzoneG.files[key]);
                    }
                });
                $( ".addPhoto" ).each(function() {
                    var thisBackground = $(this).css('background-image');
                    if ( thisBackground.indexOf(name_photo) !== -1 ){
                            delete(photosArray[name_photo]);
                            var removeItem = name_photo;
                            photosArray = jQuery.grep(photosArray, function(value) {
                                return value != removeItem;
                              });
                        $('#product-image_array').val(JSON.stringify(photosArray));

                        $(this).css('background-image', 'url(/images/default_avatar.jpg)')
                        $(this).parent().find('.deleteProductImage').hide();
                        $(this).data('delete_status','no')
                    }
                })
                
            });
    }
    
    
    $( ".NewProductImageBlock" ).hover(
        function() {
            if($(this).data('delete_status') == 'yes'){                
                $(this).parent().find('.deleteProductImage').show();
            }
            $(this).css('box-shadow', 'none');
        }, function() {
            var classH = $(this).parent().find('.deleteProductImage');
            if (classH.is(':hover')) {
            } else {
                classH.hide();
                classH.parent().find('.NewProductImageBlock').css('box-shadow', '0 0 5px rgba(0,0,0,0.5)');
            }
        }
    );
    
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ["<img src='/img/left2_icon.png'>", "<img src='/img/right2-icon.png'>"],
        responsive: {
            0: {
                items: 1,
                dots: false
            },
            768: {
                items: 5,
                dots: false
            },
            992: {
                items: 5,
                dots: false
            },
            1200: {
                items: 5,
                dots: false
            }
        }
    });

    $('.cat-item').hover(function () {
        $(this).find('img[data-hover]').attr('tmp', $(this).find('img[data-hover]').attr('src')).attr('src', $(this).find('img[data-hover]').attr('data-hover')).attr('data-hover', $(this).find('img[data-hover]').attr('tmp')).removeAttr('tmp');

    }).each(function () {
        $('<img />').attr('src', $(this).find('img[data-hover]').attr('data-hover'));
    });
    
    $(".home-active").mouseout(function () {
        $(this).find(".hov_dis_non").css('display', 'block');
    });
    $(".home-active").mouseover(function () {
        $(this).find(".hov_dis_non").css('display', 'none');
    });
});


