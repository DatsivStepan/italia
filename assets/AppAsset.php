<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/owl.carousel.css',
        'css/owl.theme.default.css',
        'css/sweetalert.css',
        'css/flexslider.css',
        'js/fresco/css/fresco/fresco.css',
        'css/index_css.css',
        'css/cabinet.css',
        'css/responsive.css',

    ];
    public $js = [
        'js/plugins/sweetalert.min.js',
        'js/isotope.pkgd.min.js',
        'js/masonry-hibryd.js',
        'js/owl.carousel.js',
        'js/dropzone.js',
        'js/jquery.flexslider.js',
        'js/fresco/js/fresco.js',
        'js/main.js',
        'js/js_site.js',
        'js/js_site.js',
        'js/cabinet.js',
        'js/ask_question.js',
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyCxbzAFE9mBuLStzXM2zprvUsv5qH9s3Rs&v=3.exp&sensor=false',
        'js/map.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
