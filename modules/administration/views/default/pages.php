<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;

?>
<section class="content-header">
                    <h1 style="color:black;">
                        Страницы
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Страницы</li>
                    </ol>
                </section>
                
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header" style="color:black;">

                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive no-padding" style="color:black;">
                                        <table class="table">
                                            <tr>
                                                <td><b>#</b></td>
                                                <td><b>Страница</b></td>
                                                <td></td>
                                            </tr>
                                            <?php foreach($modelPages as $page){ ?>
                                                <tr>
                                                    <td><?= $page['id']; ?></td>
                                                    <td><?= $page->translation[0]['name']; ?></td>

                                                    <td><a href="<?= Url::home(); ?>administration/default/page?id=<?= $page['id']; ?>" class="btn btn-primary">Редактировать</a></td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>