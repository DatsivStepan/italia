<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;

if(Yii::$app->session->hasFlash('updated')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Настройки изменены',
    ]);
endif;
if(Yii::$app->session->hasFlash('not_updated')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'настройки не изменены',
    ]);
endif;
?>
<div class="dashboard-container">
    <div class="container">
        
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa fa-arrow-down" data-action="show"> </i> Настройки
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">

                                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                                                             
                                    <?= $form->field($modelPartner, 'name')->textinput()->label('Имя Партнера'); ?>
                                    <div style="display:none;">
                                        <div class="row-table-style">
                                            <div class="table table-striped" class="files" id="previews">
                                                <div id="template" class="file-row">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="div_prof_2">
                                        <?php $avatar = '/images/default_avatar.jpg'; ?>
                                        <?php if($modelPartner->img != ''){ ?>
                                            <?php $avatar = '/'.$modelPartner->img; ?>
                                        <?php } ?>
                                        <img class="userProfileImage" src="<?= $avatar; ?>" style="width: 150px; display: block;">

                                        <div class="blockSavePhotoButton" style="display:none;">
                                            <input type="hidden" name="image_prodile_src"  value="<?= $avatar; ?>">
                                            
                                        </div>
                                        <div class="change_photo" style="padding:15px 0;">
                                            <a class="changeProfilePhoto sp_izm_logo btn btn-info" >Изменить логотип</a>
                                        </div>
                                    </div>

                                    <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-info', 'style' => 'box-shadow: 0 0 10px rgba(0,0,0,0.5);']) ?>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>