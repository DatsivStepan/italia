<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;

?>
<section class="content-header">
                    <h1 style="color:black;">
                        Партнеры
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Партнеры</li>
                    </ol>
                </section>
                
                <section class="content">
                    <a href="<?= Url::home(); ?>administration/default/partner/0" class="btn btn-success" style="margin: 10px 0;">Добавить Партнера</a>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header" style="color:black;">

                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive no-padding" style="color:black;">
                                        <table class="table">
                                            <tr>
                                                <td><b>#</b></td>
                                                <td><b>Партнер</b></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <?php foreach($partners as $page){ ?>
                                                <tr>
                                                    <td><?= $page->id; ?></td>
                                                    <td><?= $page->name; ?></td>

                                                    <td class="text-right"><a href="<?= Url::home(); ?>administration/default/partner/<?= $page->id; ?>" class="btn btn-primary">Редактировать</a></td>
                                                    <td><a href="<?= Url::home(); ?>administration/default/partnerdelete?id=<?= $page->id; ?>" class="btn btn-danger">Удалить</a></td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>