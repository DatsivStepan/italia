<?php
use yii\helpers\Html;
use yii\bootstrap\Alert;
if(Yii::$app->session->hasFlash('update')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Данные Обновлены',
    ]);
endif;
?>
    <section class="content-header">
        <h1 style="color:black;">
            Валюты
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Валюты</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header" style="color:black;">

                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding" style="color:black;">
                            <table class="table">
                                <tr>
                                    <td><b>#</b></td>
                                    <td>Обозначение</td>
                                    <td>Название Валюты</td>
                                    <td><b>Курс к Доллару</b></td>
                                </tr>
                                <form action="/administration/default/currency" method="post">
                                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                                <?php foreach($currency as $key => $currenc): ?>
                                    <tr>
                                        <td><?= $currenc['id']; ?></td>
                                        <td><?= $currenc['name']; ?></td>
                                        <td>
                                            <input type="text" value="<?= $currenc['full_name']; ?>" name="<?=$key.'[full_name]'?>" class="form-control">
                                        </td>
                                        <td>
                                        <?php if($currenc['slug']!=1): ?>
                                            <input type="text" value="<?= $currenc['value']; ?>" name="<?=$key.'[value]'?>" class="form-control">
                                        <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                                <button type="submit" class="btn btn-info" style="float: right; margin: 20px;">Обновить</button>
                                </form>
                            
                    </div>
                </div>
            </div>
        </div>
    </section>