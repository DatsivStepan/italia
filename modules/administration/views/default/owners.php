<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;

?>
<section class="content-header">
                    <h1 style="color:black;">
                        Владельцы
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Владельцы</li>
                    </ol>
                </section>
                
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header" style="color:black;">

                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive no-padding" style="color:black;">
                                        <table class="table">
                                            <tr>
                                                <td><b>#</b></td>
                                                <td><b>Владельц</b></td>
                                                <td><b>email</b></td>
                                                <td></td>
                                            </tr>
                                            <?php foreach($modelOwners as $owner){ ?>
                                                <tr class="<?=$owner->status == '10' ? 'success' : 'danger'?>">
                                                    <td><?= $owner['id']; ?></td>
                                                    <td><?= $owner->username; ?></td>
                                                    <td><?= $owner->email; ?></td>
                                                    <td>
                                                    <?php if($owner->status !== '10') : ?>
                                                    <a href="<?= Url::home(); ?>administration/default/owners?id=<?= $owner['id']; ?>&act=activate" class="btn btn-success">Активировать</a>
                                                    <?php else: ?>
                                                    <a href="<?= Url::home(); ?>administration/default/owners?id=<?= $owner['id']; ?>&act=block" class="btn btn-danger">Заблокировать</a>
                                                    <?php endif; ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>