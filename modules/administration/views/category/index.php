<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;

?>
    <?php
        if(Yii::$app->session->hasFlash('category_added')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Category added',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('category_not_added')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Category not added!',
                ]);
        endif; 
        
        
        if(Yii::$app->session->hasFlash('category_updated')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Category updated',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('category_not_updated')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Category not updated!',
                ]);
        endif; 
        
        
        if(Yii::$app->session->hasFlash('category_deleted')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Category deleted',
                ]);
        endif; 
        if(Yii::$app->session->hasFlash('category_not_deleted')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Category not deleted',
                ]);
        endif; 
    ?>
<section class="content-header">
    <h1 style="color:black;">
        Список категорий
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Категории</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6" style="margin:0 auto;float:none;">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Добавить Категорию</h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body" style="color:black;">
                    <?php foreach ($langs as $key =>$lang): ?>
                        <?= $form->field($langsContent[$lang->url], 'name')->textinput([
                                                'name' => $lang->url.'[name]',
                                            ])->label('Название Категории ('.$lang->url.')'); ?>
                    <?php endforeach; ?>
                        <?= $form->field($modelNewCategory, 'parent_id')->dropDownList($arrayParentCategory)->label('Родительская Категория'); ?>
                        
                        <?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>                                    
                <!-- form end -->

            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header" style="color:black;">
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding" style="color:black;">
                        <?= GridView::widget([
                            'dataProvider' => $modelCategories,
                            'tableOptions' => [
                                'class' => 'table table-hover'
                            ],
                            'columns' => [
                                [
                                    'attribute' => 'name',
                                    'format' => 'html',
                                    'label' => 'Имя Категории',
                                    'value' => function ($modelCategories) {
                                        if($modelCategories->name){
                                            return $modelCategories->name;
                                        }else{
                                            return '';
                                        }
                                    }
                                ],
                                [
                                    'attribute' => 'parent_category',
                                    'format' => 'html',
                                    'label' => 'Родительская Категория',
                                    'value' => function ($modelCategories) {
                                        if($modelCategories['parent_id']){
                                            return $modelCategories['parent_id'];
                                        }else{
                                            return '';
                                        }
                                    }
                                ],
                                [
                                    'attribute' => 'date_create',
                                    'format' => 'html',
                                    'label' => 'Дата',
                                ],
                                [
                                     'class' => 'yii\grid\ActionColumn',
                                     'template' => '{update} {delete}',
                                     'buttons' => [
                                         'delete' => function ($url,$modelPricings) {
                                                if($modelPricings['id']!==1 && $modelPricings['id']!==2){
                                                    return Html::a(
                                                    '<span class="glyphicon glyphicon-trash"></span>', 
                                                    'delete?id='.$modelPricings['id']);
                                                }else{
                                                    return '';
                                                }
                                                 
                                         },
                                         'update' => function ($url,$modelPricings) {
                                                 return Html::a(
                                                 '<span class="glyphicon glyphicon-pencil"></span>', 
                                                 'update?id='.$modelPricings['id']);
                                         },
                                     ],
                                 ],
                            ],
                        ]) ?>
                </div>
            </div>
        </div>
    </div>
</section>
 