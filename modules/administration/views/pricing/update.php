<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\ckeditor\CKEditor;
?>
    
<section class="content-header">
    <h1 style="color:black;">
        Список Пакетов
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Список Пакетов</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6" style="margin:0 auto;float:none;">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Обновить Пакет</h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body" style="color:black;">
                        <ul class="nav nav-tabs">
                            <?php foreach ($langs as $key =>$lang): ?>
                            <li class="<?=$key==0 ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#lang<?=$lang->id?>">
                                <img src="<?=Url::home().'flags/'.$lang->icon?>">
                                <span><?=$lang->name?></span>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content">    
                        <?php foreach ($langs as $key =>$lang): ?>
                        <div id="lang<?=$lang->id?>" class="tab-pane fade <?=$key==0 ? 'in active' : '' ?>">
                            <?= $form->field($langsContent[$lang->url], 'name')->textinput([
                                                'name' => $lang->url.'[name]',
                                                'required' => true,
                                            ])->label('Название Пакета'); ?>
                            <?= $form->field($langsContent[$lang->url], 'description')
                            ->widget(CKEditor::className(), 
                            [ 
                                'options' => 
                                [ 
                                    'rows' => 8, 
                                    'name' => $lang->url.'[description]', 
                                    'id' => $lang->url.'[description]',

                                ], 
                                'preset' => 'standard',
                            ]) ?>
                        </div>
                        <?php endforeach; ?>
                        <?= $form->field($modelPricing, 'months_count')->input('number')->label('Времья действия ( в месяцах )'); ?>
                        <?= $form->field($modelPricing, 'product_count')->input('number')->label('Количество Объявлений'); ?>
                        <?= $form->field($modelPricing, 'price')->textinput(['type'=>'number', 'step'=>'0.01'])->label('Начальная Цена Пакета'); ?>
                        <?= $form->field($modelPricing, 'price_to_one')->textinput(['type'=>'number', 'step'=>'0.01'])->label('Цена Пакета На Один Месяц'); ?>
                        <?= $form->field($modelPricing, 'price_to_long')->textinput(['type'=>'number', 'step'=>'0.01'])->label('Цена Пакета При Долгострочном Продлении'); ?>
                        <?= $form->field($modelPricing, 'mouth_to_long')->input('number')->label('Время действия ( в месяцах ) При Долгострочном Продлении'); ?>
                        <?= $form->field($modelPricing, 'message')->input('number')->label('Количество Сообщений В Пакете'); ?>
                        
                        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>                                    
                <!-- form end -->

            </div>
        </div>
    </div>
</section>

 