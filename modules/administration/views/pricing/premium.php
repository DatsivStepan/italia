<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\ckeditor\CKEditor;
?>
    
<section class="content-header">
    <h1 style="color:black;">
        Премиум Пакет 
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Премиум Пакет</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6" style="margin:0 auto;float:none;">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Обновить Пакет</h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body" style="color:black;">

                        <?= $form->field($premiumModel, 'name_ru')->textinput(['required'=>true])->label('Название Пакета (ru)'); ?>
                        <?= $form->field($premiumModel, 'name_it')->textinput(['required'=>true])->label('Название Пакета (it)'); ?>
                        <?= $form->field($premiumModel, 'count_prem_prod')->textinput(['required'=>true, 'type'=>'number', 'step'=>'1'])->label('Количество ТОП Обьявлений'); ?>
                        <?= $form->field($premiumModel, 'price')->textinput(['required'=>true, 'type'=>'number', 'step'=>'0.01'])->label('Цена'); ?>
                        <?= $form->field($premiumModel, 'description_ru')
                            ->widget(CKEditor::className(), 
                            [ 
                                'options' => 
                                [ 
                                    'rows' => 8,
                                    'id' => 'description_ru',

                                ], 
                                'preset' => 'standard',
                            ]) ?>
                        <?= $form->field($premiumModel, 'description_it')
                            ->widget(CKEditor::className(),
                            [ 
                                'options' => 
                                [ 
                                    'rows' => 8,
                                    'id' => 'description_it',

                                ], 
                                'preset' => 'standard',
                            ]) ?>
                        <?= $form->field($premiumModel, 'description_en')
                            ->widget(CKEditor::className(),
                            [ 
                                'options' => 
                                [ 
                                    'rows' => 8,
                                    'id' => 'description_en',

                                ], 
                                'preset' => 'standard',
                            ]) ?>
                        
                        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>                                    
                <!-- form end -->

            </div>
        </div>
    </div>
</section>