<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\ckeditor\CKEditor;
?>
    <?php
        if(Yii::$app->session->hasFlash('pricing_added')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Pricing added',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('pricing_not_added')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Pricing not added!',
                ]);
        endif; 
        
        
        if(Yii::$app->session->hasFlash('pricing_updated')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Pricing updated',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('pricing_not_updated')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Pricing not updated!',
                ]);
        endif; 
        
        
        if(Yii::$app->session->hasFlash('pricing_deleted')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Pricing deleted',
                ]);
        endif; 
        if(Yii::$app->session->hasFlash('pricing_not_deleted')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Pricing not deleted',
                ]);
        endif; 
    ?>
<section class="content-header">
    <h1 style="color:black;">
        Список Пакетов
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Список Пакетов</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6" style="margin:0 auto;float:none;">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Добавить Пакет</h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body" style="color:black;">
                        <ul class="nav nav-tabs">
                            <?php foreach ($langs as $key =>$lang): ?>
                            <li class="<?=$key==0 ? 'active' : '' ?>">
                                <a data-toggle="tab" href="#lang<?=$lang->id?>">
                                <img src="<?=Url::home().'flags/'.$lang->icon?>">
                                <span><?=$lang->name?></span>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content">    
                        <?php foreach ($langs as $key =>$lang): ?>
                        <div id="lang<?=$lang->id?>" class="tab-pane fade <?=$key==0 ? 'in active' : '' ?>">
                            <?= $form->field($langsContent[$lang->url], 'name')->textinput([
                                                'name' => $lang->url.'[name]',
                                                'required' => true,
                                            ])->label('Название Пакета'); ?>
                            <?= $form->field($langsContent[$lang->url], 'description')
                            ->widget(CKEditor::className(), 
                            [ 
                                'options' => 
                                [ 
                                    'rows' => 8, 
                                    'name' => $lang->url.'[description]', 
                                    'id' => $lang->url.'[description]',

                                ], 
                                'preset' => 'standard',
                            ]) ?>
                        </div>
                        <?php endforeach; ?>
                        <?= $form->field($modelNewPricing, 'months_count')->input('number')->label('Время действия ( в месяцах )'); ?>
                        <?= $form->field($modelNewPricing, 'product_count')->input('number')->label('Количество Объявлений'); ?>
                        <?= $form->field($modelNewPricing, 'price')->textinput(['type'=>'number', 'step'=>'0.01'])->label('Начальная Цена Пакета'); ?>
                        <?= $form->field($modelNewPricing, 'price_to_one')->textinput(['type'=>'number', 'step'=>'0.01'])->label('Цена Пакета На Один Месяц'); ?>
                        <?= $form->field($modelNewPricing, 'price_to_long')->textinput(['type'=>'number', 'step'=>'0.01'])->label('Цена Пакета При Долгострочном Продлении'); ?>
                        <?= $form->field($modelNewPricing, 'mouth_to_long')->input('number')->label('Время действия ( в месяцах ) При Долгострочном Продлении'); ?>
                        <?= $form->field($modelNewPricing, 'message')->input('number')->label('Количество Сообщений В Пакете'); ?>
                        
                        <?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>                                    
                <!-- form end -->

            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header" style="color:black;">

                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding" style="color:black;">
                        <?= GridView::widget([
                            'dataProvider' => $modelPricings,
                            'tableOptions' => [
                                'class' => 'table table-hover'
                            ],
                            'columns' => [
                                [
                                    'attribute' => 'name',
                                    'format' => 'html',
                                    'label' => 'Название Пакета'
                                ],
                                [
                                    'attribute' => 'months_count',
                                    'format' => 'html',
                                    'label' => 'Времья действия ( в месяцах )'
                                ],
                                [
                                    'attribute' => 'product_count',
                                    'format' => 'html',
                                    'label' => 'Количество Объявлений'
                                ],
                                [
                                    'attribute' => 'price',
                                    'format' => 'html',
                                    'label' => 'Цена Пакета'
                                ],
                                [
                                     'class' => 'yii\grid\ActionColumn',
                                     'template' => '{update}',
                                     'buttons' => [
                                         'delete' => function ($url,$modelPricings) {
                                                 return Html::a(
                                                 '<span class="glyphicon glyphicon-trash"></span>', 
                                                 'delete?id='.$modelPricings['id']);
                                         },
                                         'update' => function ($url,$modelPricings) {
                                                 return Html::a(
                                                 '<span class="glyphicon glyphicon-pencil"></span>', 
                                                 'update?id='.$modelPricings['id']);
                                         },
                                     ],
                                 ],
                            ],
                        ]) ?>
                </div>
            </div>
        </div>
    </div>
</section>
 