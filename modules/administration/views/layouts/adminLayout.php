<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\widgets\LangWidget;

use app\assets\AdminappAsset;
AdminappAsset::register($this);
$menu_act = $this->params['menu_item'];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="bg-black skin-black">
<?php $this->beginBody() ?>

        <?php 
        NavBar::begin(['options' => [
                    'style' => 'display:none;',
                ]]);
        NavBar::end(); ?>
        <?php 
        /*
    <div class="container" style="min-height: 50px">
            NavBar::begin([
                'brandLabel' => '<span style="float:left;">My Company</span>',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);?>
            <ul class="nav navbar-nav">
                <?= LangWidget::widget(); ?>
            </ul>    
            <?php 
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    Yii::$app->user->isGuest ? (
                        ['label' => 'Login', 'url' => ['/administration']]
                    ) : (
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Logout (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link']
                        )
                        . Html::endForm()
                        . '</li>'
                    )
                ],
            ]);
            NavBar::end(); 
    </div>
     */ ?>
     <?php if(!\Yii::$app->user->isGuest){ ?>
    <header class="header">
            <a href="index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Italia
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php 
                        echo  Html::beginForm(['/site/logout'], 'post'); ?>
                            <i class="glyphicon glyphicon-user"></i>
                        <?php echo  Html::submitButton(
                            '<span>Выход (' . Yii::$app->user->identity->username . ')</span>',
                            ['class' => 'btn-link']
                        );
                        echo Html::endForm(); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
    </header>
     <?php } ?>
    
    
    
    <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php if(!\Yii::$app->user->isGuest){ ?>
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="/images/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <?php if(!\Yii::$app->user->isGuest){ ?>
                                <p>Привет, <?= \Yii::$app->user->identity->username; ?></p>
                            <?php } ?>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">

                        <li class="<?=isset($menu_act['setting'])? $menu_act['setting'] : ''?>">
                            <a href="/administration/default/setting">
                                <i class="fa fa fa-gear"></i> <span>Настройки сайта</span>
                            </a>
                        </li>
                        <li class="<?=isset($menu_act['page'])? $menu_act['page'] : ''?>">
                            <a href="/administration/default/pages">
                                <i class="fa fa-folder"></i> <span>Страницы</span>
                            </a>
                        </li>
                        <li class="treeview <?=isset($menu_act['price']) || isset($menu_act['emails']) || isset($menu_act['premium'] )? 'active' : ''?>">
                            <a href="#">
                            <i class="fa fa-laptop"></i>
                            <span>Пакеты</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">
                                <li class="<?=isset($menu_act['price'])? $menu_act['price'] : ''?>">
                                    <a href="/administration/pricing/index">
                                        <i class="fa fa-circle-o"></i> <span>Основные Пакеты</span>
                                    </a>
                                </li>
                                <li class="<?=isset($menu_act['emails'])? $menu_act['emails'] : ''?>">
                                    <a href="/administration/emails/index">
                                        <i class="fa fa-circle-o"></i> <span>Дополнительные Пакеты</span>
                                    </a>
                                </li>
                                <li class="<?=isset($menu_act['premium'])? $menu_act['premium'] : ''?>">
                                    <a href="/administration/pricing/premium">
                                        <i class="fa fa-circle-o"></i> <span>Премиум Пакет</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="<?=isset($menu_act['category'])? $menu_act['category'] : ''?>">
                            <a href="/administration/category/index">
                                <i class="fa fa-folder"></i> <span>Категории</span>
                            </a>
                        </li>
                        <li class="<?=isset($menu_act['partners'])? $menu_act['partners'] : ''?>">
                            <a href="/administration/default/partners">
                                <i class="fa fa-folder"></i> <span>Партнеры</span>
                            </a>
                        </li>
                        <li class="<?=isset($menu_act['owners'])? $menu_act['owners'] : ''?>">
                            <a href="/administration/default/owners">
                                <i class="fa fa-folder"></i> <span>Владельцы</span>
                            </a>
                        </li>
                        <!--<li class="<?php //isset($menu_act['currency'])? $menu_act['currency'] : ''?>">
                            <a href="/administration/default/currency">
                                <i class="fa fa-folder"></i> <span>Валюты</span>
                            </a>
                        </li>-->
                        <!--<li class="treeview">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Examples</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/examples/invoice.html"><i class="fa fa-angle-double-right"></i> Invoice</a></li>
                                <li><a href="pages/examples/login.html"><i class="fa fa-angle-double-right"></i> Login</a></li>
                                <li><a href="pages/examples/register.html"><i class="fa fa-angle-double-right"></i> Register</a></li>
                                <li><a href="pages/examples/lockscreen.html"><i class="fa fa-angle-double-right"></i> Lockscreen</a></li>
                                <li><a href="pages/examples/404.html"><i class="fa fa-angle-double-right"></i> 404 Error</a></li>
                                <li><a href="pages/examples/500.html"><i class="fa fa-angle-double-right"></i> 500 Error</a></li>
                                <li><a href="pages/examples/blank.html"><i class="fa fa-angle-double-right"></i> Blank Page</a></li>
                            </ul>
                        </li> -->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <aside class="right-side">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </aside>
            <?php }else{ ?>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>                
            <?php } ?>
            
            <!-- Right side column. Contains the navbar and content of the page -->
        </div>
    


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>