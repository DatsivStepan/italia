<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;

?>
    
<section class="content-header">
    <h1 style="color:black;">
        Список Дополнительных Пакетов
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Список Дополнительных Пакетов</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6" style="margin:0 auto;float:none;">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Обновить Пакет</h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body" style="color:black;">
                        <?php foreach ($langs as $key =>$lang): ?>
                        <?= $form->field($langsContent[$lang->url], 'name')->textinput([
                                                'name' => $lang->url.'[name]',
                                                'required' => true,
                                            ])->label('Название Пакета ('.$lang->url.')'); ?>
                        <?php endforeach; ?>
                        <?= $form->field($modelEmails, 'emails_count')->input('number')->label('Количество сообщений'); ?>
                        <?= $form->field($modelEmails, 'product')->input('number')->label('Количество продуктов'); ?>
                        <?= $form->field($modelEmails, 'price')->textinput()->label('Цена Пакета'); ?>
                        
                        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>                                    
                <!-- form end -->

            </div>
        </div>
    </div>
</section>

 