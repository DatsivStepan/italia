<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;

?>
    <?php
        if(Yii::$app->session->hasFlash('pricing_added')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Packet added',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('pricing_not_added')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Packet not added!',
                ]);
        endif; 
        
        
        if(Yii::$app->session->hasFlash('pricing_updated')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Packet updated',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('pricing_not_updated')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Packet not updated!',
                ]);
        endif; 
        
        
        if(Yii::$app->session->hasFlash('pricing_deleted')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Packet deleted',
                ]);
        endif; 
        if(Yii::$app->session->hasFlash('pricing_not_deleted')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Packet not deleted',
                ]);
        endif; 
    ?>
<section class="content-header">
    <h1 style="color:black;">
        Список Дополнительных Пакетов
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Список Дополнительных Пакетов</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6" style="margin:0 auto;float:none;">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Добавить Пакет</h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body" style="color:black;">
                        <?php foreach ($langs as $key =>$lang): ?>
                        <?= $form->field($langsContent[$lang->url], 'name')->textinput([
                                                'name' => $lang->url.'[name]',
                                                'required' => true,
                                            ])->label('Название Пакета ('.$lang->url.')'); ?>
                        <?php endforeach; ?>
                        <?php //$form->field($formEmailModel, 'type')->dropDownList(['both'=>'Сообщения + Email', 'mess'=>'Сообщения', 'email'=>'Email'])->label('Тип Пакета'); ?>
                        <?= $form->field($formEmailModel, 'emails_count')->textinput(['type'=>'number', 'value'=>'0'])->label('Количество сообщений'); ?>
                        <?= $form->field($formEmailModel, 'product')->textinput(['type'=>'number', 'value'=>'0'])->label('Количество продуктов'); ?>
                        <?= $form->field($formEmailModel, 'price')->textinput()->label('Цена Пакета'); ?>
                        
                        <?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>                                    
                <!-- form end -->

            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header" style="color:black;">

                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding" style="color:black;">
                        <?= GridView::widget([
                            'dataProvider' => $modelEmails,
                            'tableOptions' => [
                                'class' => 'table table-hover'
                            ],
                            'columns' => [
                                [
                                    'attribute' => 'name',
                                    'format' => 'html',
                                    'label' => 'Название Пакета'
                                ],
                                [
                                    'attribute' => 'emails_count',
                                    'format' => 'html',
                                    'label' => 'Количество сообщений'
                                ],
                                [
                                    'attribute' => 'product',
                                    'format' => 'html',
                                    'label' => 'Количество объявлений'
                                ],
                                [
                                    'attribute' => 'price',
                                    'format' => 'html',
                                    'label' => 'Цена Пакета'
                                ],
                                [
                                     'class' => 'yii\grid\ActionColumn',
                                     'template' => '{update}',
                                     'buttons' => [
                                         'delete' => function ($url,$modelEmails) {
                                                 return Html::a(
                                                 '<span class="glyphicon glyphicon-trash"></span>', 
                                                 'delete?id='.$modelEmails['id']);
                                         },
                                         'update' => function ($url,$modelEmails) {
                                                 return Html::a(
                                                 '<span class="glyphicon glyphicon-pencil"></span>', 
                                                 'update?id='.$modelEmails['id']);
                                         },
                                     ],
                                 ],
                            ],
                        ]) ?>
                </div>
            </div>
        </div>
    </div>
</section>
 