<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Pages;
use app\models\Setting;
use app\modules\administration\models\Pricing;
use app\models\LoginForm;
use app\models\PremiumPacket;
use yii\data\ActiveDataProvider;
use app\modules\administration\models\PricingContent;
use app\models\Lang;

class PricingController extends Controller
{
    public function beforeAction($action) {
        $this->layout = 'adminLayout';
        if((!\Yii::$app->user->isGuest) && (Yii::$app->user->identity->type == 'admin')){
            return parent::beforeAction($action);            
        }else{
            if(\Yii::$app->user->isGuest){
                return parent::beforeAction($action);            
               //return $this->redirect('administration'); 
            }else{
                throw new \yii\web\NotFoundHttpException();                
            }
        }
    }
    
    public function actionIndex()
    {            
        $modelNewPricing = new Pricing;
        $modelNewPricing->scenario = 'add';
        $langsContent = $this->createLangModels();
        if(Yii::$app->request->post()){
            if($modelNewPricing->load(\Yii::$app->request->post())){
                if($modelNewPricing->save()){
                    $this->writeLangModels($langsContent, $modelNewPricing->id);
                    $modelNewPricing = new Pricing;
                    $modelNewPricing->scenario = 'add';
                    \Yii::$app->session->setFlash('pricing_added');
                    $this->refresh();
                }else{
                    \Yii::$app->session->setFlash('pricing_not_added');                    
                }
            }
        }
        $queryPricings = Pricing::find();
        $modelPricings = new ActiveDataProvider(['query' => $queryPricings, 'pagination' => ['pageSize' => 30]]);
        $this->view->params['menu_item']['price'] = 'active';
        return $this->render('index', [
            'modelNewPricing' => $modelNewPricing,
            'modelPricings' => $modelPricings,
            'langsContent' => $langsContent,
            'langs' => Lang::find()->all(),
        ]);
    }

    public function actionPremium()
    {
        $premiumModel = PremiumPacket::find()->where(['id'=>1])->one();
        $premiumModel->scenario = 'default';
        if(Yii::$app->request->post()){
            if($premiumModel->load(\Yii::$app->request->post())){
                if($premiumModel->save())
                {
                    $this->refresh();
                }

            }
        }

        $this->view->params['menu_item']['premium'] = 'active';
        return $this->render('premium', [
            'premiumModel'=> $premiumModel
        ]);
    }
    
    public function actionDelete($id=null)
    {
        if($id != null){
            $modelPricing = Pricing::find()->where(['id' => $id])->one();
            if($modelPricing != null){
                if($modelPricing->delete()){
                    \Yii::$app->session->setFlash('pricing_deleted');
                }else{
                    \Yii::$app->session->setFlash('pricing_not_deleted');
                }
                return $this->redirect('/administration/pricing/index');
                
            }else{
                throw new \yii\web\NotFoundHttpException(); 
            }
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }
    }
    
    
    public function actionUpdate($id=null)
    {            
        if($id != null){
            $modelPricing = Pricing::find()->where(['id' => $id])->one();
            $langsContent = $this->rewriteArrModels($modelPricing->translation);
            if($modelPricing != null){
                $modelPricing->scenario = 'update';

                if(Yii::$app->request->post()){
                    if($modelPricing->load(\Yii::$app->request->post())){
                        if($modelPricing->save()){
                            $this->writeLangModels($langsContent, $modelPricing->id);
                            \Yii::$app->session->setFlash('pricing_updated');
                        }else{
                            \Yii::$app->session->setFlash('pricing_not_updated');
                        }
                        return $this->redirect('/administration/pricing/index');
                    }
                }
                $this->view->params['menu_item']['price'] = 'active';
                return $this->render('update', [
                    'modelPricing' => $modelPricing,
                    'langsContent' => $langsContent,
                    'langs' => Lang::find()->all(),
                ]);
                
            }else{
                throw new \yii\web\NotFoundHttpException(); 
            }            
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }
    }

    public function rewriteArrModels($models)
    {
        $arrModels = [];
        $langs = Lang::find()->all();
        foreach ($langs as $key =>$lang) {
            $arrModels[$lang->url] = $models[$key];
        }
        return $arrModels;
    }


    public function createLangModels()
    {
        $arrModels = [];
        $langs = Lang::find()->all();
        foreach ($langs as $lang) {
            $arrModels[$lang->url] = new PricingContent();
        }
        return $arrModels;
    }

    public function writeLangModels($langsContent, $join_id)
    {
        $langs = Lang::find()->all();
        foreach ($langs as $lang) {
            $langsContent[$lang->url]->join_id = $join_id;
            $langsContent[$lang->url]->lang = $lang->url;
            foreach ($_POST[$lang->url] as $post_key => $post_value) {
                $langsContent[$lang->url]->$post_key = $post_value;
            }
            $langsContent[$lang->url]->save();
        }
    }
}
