<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Pages;
use app\models\Setting;
use app\modules\administration\models\EmailPackets;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;
use app\modules\administration\models\EmailPacketsContent;
use app\models\Lang;

class EmailsController extends Controller
{
    public function beforeAction($action) {
        $this->layout = 'adminLayout';
        if((!\Yii::$app->user->isGuest) && (Yii::$app->user->identity->type == 'admin')){
            return parent::beforeAction($action);            
        }else{
            if(\Yii::$app->user->isGuest){
                return parent::beforeAction($action);            
               //return $this->redirect('administration'); 
            }else{
                throw new \yii\web\NotFoundHttpException();                
            }
        }
    }
    
    public function actionIndex()
    {            
        $model = new EmailPackets;
        $langsContent = $this->createLangModels();
        if(Yii::$app->request->post()){
            if($model->load(\Yii::$app->request->post())){
                if($model->save()){
                    $this->writeLangModels($langsContent, $model->id);
                    $this->refresh();
                    $model = new EmailPackets;
                    \Yii::$app->session->setFlash('pricing_added');
                }else{
                    \Yii::$app->session->setFlash('pricing_not_added');                    
                }
            }
        }
        $queryPricings = EmailPackets::find();
        $modelEmails = new ActiveDataProvider(['query' => $queryPricings, 'pagination' => ['pageSize' => 30]]);
        $this->view->params['menu_item']['emails'] = 'active';
        return $this->render('index', [
            'formEmailModel' => $model,
            'modelEmails' => $modelEmails,
            'langsContent' => $langsContent,
            'langs' => Lang::find()->all(),
        ]);
    }

    public function actionDelete($id=null)
    {
        if($id != null){
            $modelEmails = EmailPackets::find()->where(['id' => $id])->one();
            if($modelEmails != null){
                if($modelEmails->delete()){
                    \Yii::$app->session->setFlash('pricing_deleted');
                }else{
                    \Yii::$app->session->setFlash('pricing_not_deleted');
                }
                return $this->redirect('/administration/emails/index');
                
            }else{
                throw new \yii\web\NotFoundHttpException(); 
            }
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }
    }

    public function actionUpdate($id=null)
    {            
        if($id != null){
            $modelEmails = EmailPackets::find()->where(['id' => $id])->one();
            $langsContent = $this->rewriteArrModels($modelEmails->translation);
            if($modelEmails != null){

                if(Yii::$app->request->post()){
                    if($modelEmails->load(\Yii::$app->request->post())){
                        if($modelEmails->save()){
                            $this->writeLangModels($langsContent, $modelEmails->id);
                            \Yii::$app->session->setFlash('pricing_updated');
                        }else{
                            \Yii::$app->session->setFlash('pricing_not_updated');
                        }
                        return $this->redirect('/administration/emails/index');
                    }
                }
                $this->view->params['menu_item']['emails'] = 'active';
                return $this->render('update', [
                    'modelEmails' => $modelEmails,
                    'langsContent' => $langsContent,
                    'langs' => Lang::find()->all(),
                ]);
                
            }else{
                throw new \yii\web\NotFoundHttpException(); 
            }            
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }
    }

    public function rewriteArrModels($models)
    {
        $arrModels = [];
        $langs = Lang::find()->all();
        foreach ($langs as $key =>$lang) {
            $arrModels[$lang->url] = $models[$key];
        }
        return $arrModels;
    }


    public function createLangModels()
    {
        $arrModels = [];
        $langs = Lang::find()->all();
        foreach ($langs as $lang) {
            $arrModels[$lang->url] = new EmailPacketsContent();
        }
        return $arrModels;
    }

    public function writeLangModels($langsContent, $join_id)
    {
        $langs = Lang::find()->all();
        foreach ($langs as $lang) {
            $langsContent[$lang->url]->join_id = $join_id;
            $langsContent[$lang->url]->lang = $lang->url;
            foreach ($_POST[$lang->url] as $post_key => $post_value) {
                $langsContent[$lang->url]->$post_key = $post_value;
            }
            $langsContent[$lang->url]->save();
        }
    }
}