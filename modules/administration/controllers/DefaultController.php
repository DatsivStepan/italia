<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Pages;
use app\models\PagesContent;
use app\models\Setting;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;
use app\models\Lang;
use app\models\Currency;
use app\models\Partners;
use app\models\User;
/**
 * Default controller for the `administration` module
 */
class DefaultController extends Controller
{

    public function beforeAction($action) {
        $this->layout = 'adminLayout';
        if((!\Yii::$app->user->isGuest) && (Yii::$app->user->identity->type == 'admin')){
            return parent::beforeAction($action);            
        }else{
            if(\Yii::$app->user->isGuest){
                return parent::beforeAction($action);            
               //return $this->redirect('administration'); 
            }else{
                throw new \yii\web\NotFoundHttpException();                
            }
        }
    }
    
    public function actionIndex()
    {
//        echo 'dfdf';exit;
        if(\Yii::$app->user->isGuest){
            
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                if(Yii::$app->user->identity->type == 'admin'){
                    return $this->redirect('/administration/default/setting');
                }else{
                    return $this->redirect('/');                    
                }
            }
            $this->view->params['menu_item']['setting'] = 'active';
            return $this->render('index', [
                'model' => $model,
            ]);
            
        }else{
           return $this->redirect('administration/default/setting'); 
        }
        
    }
    
    public function actionSetting()
    {

        $modelNewSetting = new Setting();
        $modelNewSetting->scenario = 'add_setting';
        if($modelNewSetting->load(Yii::$app->request->post())){
            if($modelNewSetting->save()){
                Yii::$app->session->setFlash('add_setting');
                $this->refresh();
            }else{
                Yii::$app->session->setFlash('not_add_setting');
            }
        }
        $querySetting = Setting::find();
        $modelSetting = new ActiveDataProvider(['query' => $querySetting, 'pagination' => ['pageSize' => 30]]);

        $this->view->params['menu_item']['setting'] = 'active';
        return $this->render('setting',[
            'modelSetting' => $modelSetting,
            'modelNewSetting' => $modelNewSetting
        ]);
    }

    public function actionSettingdelete($id=null)
    {
        
        $modelSetting = Setting::find()->where(['id' => $id])->one();
        if($modelSetting->delete()){
            Yii::$app->session->setFlash('delete_setting');
            return $this->redirect(Url::home().'administration/default/setting');
        }else{
            Yii::$app->session->setFlash('not_delete_setting');
            return $this->redirect(Url::home().'administration/default/setting');
        }
    }
    
    public function actionSettingupdate($id=null)
    {
        $modelSetting = Setting::find()->where(['id' => $id])->one();
        $modelSetting->scenario = 'update_setting';
        if($modelSetting->load(Yii::$app->request->post())){
            if($modelSetting->save()){
                Yii::$app->session->setFlash('update_setting');
                return $this->redirect(Url::home().'administration/default/setting');
            }else{
                Yii::$app->session->setFlash('not_update_setting');
            }
        }
        $this->view->params['menu_item']['setting'] = 'active';
        return $this->render('settingupdate', [
            'modelSetting' => $modelSetting,
        ]);
    }
    
    //pages begin
    public function actionPages(){        
        $modelPages = Pages::find()->all();

        $this->view->params['menu_item']['page'] = 'active';
        return $this->render('pages', [
            'modelPages' => $modelPages,
        ]);
    }
    
    public function actionPage($id=null){
        
        if($id){
            $modelPage = Pages::find()->where(['id' => $id])->one();
            $langsContent = $this->rewriteArrModels($modelPage->translation);
        }else{
            $modelPage = new Pages();
            $langsContent = $this->createLangModels();
        }
        
        if($_POST){

            $modelPage->scenario = 'update';
            
            if($modelPage->save()){
                $this->writeLangModels($langsContent, $modelPage->id);
                Yii::$app->session->setFlash('update_page');
                return $this->redirect('/administration/default/pages');
            }else{
                Yii::$app->session->setFlash('not_update_page');
                return $this->redirect('/administration/default/pages');
            }
            
        }
        $this->view->params['menu_item']['page'] = 'active';
        return $this->render('page', [
            'modelPage' => $modelPage,
            'langs' => Lang::find()->all(),
            'langsContent' => $langsContent
        ]);
    }

    public function actionPartners(){        
        $partners = Partners::find()->all();
        $this->view->params['menu_item']['partners'] = 'active';
        return $this->render('partners', [
            'partners' => $partners,
        ]);
    }

    public function actionPartner($partner_id){
        if($partner_id){
            $modelPartner = Partners::find()->where(['id'=>$partner_id])->one();
        }
        else{
            $modelPartner = new Partners();
        }

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $modelPartner->load($post);
            $modelPartner->name = $post['Partners']['name'];
            $modelPartner->img = $post['image_prodile_src'];
            $modelPartner->save();
            return $this->redirect('/administration/default/partners'); 
        }
        
        $this->view->params['menu_item']['partners'] = 'active';
        return $this->render('partner', [
            'modelPartner' => $modelPartner
        ]);
    }

    public function actionPartnerdelete()
    {
        $modelSetting = Partners::find()->where(['id' => $_GET['id']])->one();
        $modelSetting->delete();
        return $this->redirect('/administration/default/partners');
    }
    
    public function actionCurrency()
    {
        $currency = Currency::find()->all();
        if(Yii::$app->request->post()) 
        {
            $post = Yii::$app->request->post();
            foreach ($currency as $key => $currenc) {
                $currenc->full_name = $post[$key]['full_name'];
                if(isset($post[$key]['value'])){
                    $currenc->value = $post[$key]['value'];
                }
                $currenc->save();
            }
            Yii::$app->session->setFlash('update');
        }       
        $this->view->params['menu_item']['currency'] = 'active';
        return $this->render('currency', [
            'currency' => $currency
        ]);
    }

    public function actionOwners()
    {
        if( isset($_GET['act']) && isset($_GET['id']) )
        {
            $owner = User::find()->where(['id'=>$_GET['id']])->one();
            if($_GET['act'] == 'activate'){    
                $owner->status = 10;
                $owner->sended_emails = -999999; 
            }elseif($_GET['act'] == 'block'){
                $owner->status = 0;
            }
            $owner->save();
            $this->redirect(Url::home().'administration/default/owners');
        }
        
        $modelOwners = User::find()->where(['type'=>'owner'])->all();
        $this->view->params['menu_item']['owners'] = 'active';
        return $this->render('owners', [
            'modelOwners' => $modelOwners,
        ]);
    }

    //pages end
    public function rewriteArrModels($models)
    {
        $arrModels = [];
        $langs = Lang::find()->all();
        foreach ($langs as $key =>$lang) {
            $arrModels[$lang->url] = $models[$key];
        }
        return $arrModels;
    }


    public function createLangModels()
    {
        $arrModels = [];
        $langs = Lang::find()->all();
        foreach ($langs as $lang) {
            $arrModels[$lang->url] = new PagesContent();
        }
        return $arrModels;
    }

    public function writeLangModels($langsContent, $join_id)
    {
        $langs = Lang::find()->all();
        foreach ($langs as $lang) {
            $langsContent[$lang->url]->join_id = $join_id;
            $langsContent[$lang->url]->lang = $lang->url;
            foreach ($_POST[$lang->url] as $post_key => $post_value) {
                $langsContent[$lang->url]->$post_key = $post_value;
            }
            $langsContent[$lang->url]->save();
        }
    }


    
    
}
