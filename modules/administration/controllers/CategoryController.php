<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Pages;
use app\models\Setting;
use app\models\Category;
use app\modules\administration\models\Pricing;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;
use app\models\CategoryContent;
use app\models\Lang;

class CategoryController extends Controller
{
    public function beforeAction($action) {
        $this->layout = 'adminLayout';
        if((!\Yii::$app->user->isGuest) && (Yii::$app->user->identity->type == 'admin')){
            return parent::beforeAction($action);            
        }else{
            if(\Yii::$app->user->isGuest){
                return parent::beforeAction($action);            
               //return $this->redirect('administration'); 
            }else{
                throw new \yii\web\NotFoundHttpException();                
            }
        }
    }
    
    public function actionIndex()
    {            
        $modelNewCategory = new Category;
        $langsContent = $this->createLangModels();
        $modelNewCategory->scenario = 'add';
                    
        if(Yii::$app->request->post()){
            if($modelNewCategory->load(\Yii::$app->request->post())){
                if($modelNewCategory->save()){
                    $this->writeLangModels($langsContent, $modelNewCategory->id);
                    $modelNewCategory = new Category;
                    $modelNewCategory->scenario = 'add';
                    \Yii::$app->session->setFlash('category_added');
                    $this->refresh();
                }else{
                    \Yii::$app->session->setFlash('category_not_added');                    
                }
            }
        }
        $arrayParentCategory = \yii\helpers\ArrayHelper::map(Category::find()->where(['parent_id' => 0])->orWhere(['parent_id' => null])->all(), 'id', 'name');
        
        $queryCategories = Category::find();
        $modelCategories = new ActiveDataProvider(['query' => $queryCategories, 'pagination' => ['pageSize' => 30]]);
        $this->view->params['menu_item']['category'] = 'active';
        return $this->render('index', [
            'arrayParentCategory' => $arrayParentCategory,
            'modelNewCategory' => $modelNewCategory,
            'modelCategories' => $modelCategories,
            'langsContent' => $langsContent,
            'langs' => Lang::find()->all(),
        ]);
    }
    
    public function actionDelete($id=null)
    {
        if($id != null){
            $modelCategory = Category::find()->where(['id' => $id])->one();
            if($modelCategory != null){
                if($modelCategory->delete()){
                    \Yii::$app->session->setFlash('category_deleted');
                }else{
                    \Yii::$app->session->setFlash('category_not_deleted');
                }
                return $this->redirect('/administration/category/index');
                
            }else{
                throw new \yii\web\NotFoundHttpException(); 
            }
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }
    }
    
    
    public function actionUpdate($id=null)
    {            
        if($id != null){
            $modelCategory = Category::find()->where(['id' => $id])->one();
            $langsContent = $this->rewriteArrModels($modelCategory->translation);
            if($modelCategory != null){
                $modelCategory->scenario = 'update';

                if(Yii::$app->request->post()){
                    if($modelCategory->load(\Yii::$app->request->post())){
                        if($modelCategory->save()){
                            $this->writeLangModels($langsContent, $modelCategory->id);
                            \Yii::$app->session->setFlash('category_updated');
                        }else{
                            \Yii::$app->session->setFlash('category_not_updated');
                        }
                        return $this->redirect('/administration/category/index');
                    }
                }
                
                $arrayParentCategory = \yii\helpers\ArrayHelper::map(Category::find()->where(['parent_id' => 0])->orWhere(['parent_id' => null])->all(), 'id', 'name');
                $this->view->params['menu_item']['category'] = 'active';
                return $this->render('update', [
                    'arrayParentCategory' => $arrayParentCategory,
                    'modelCategory' => $modelCategory,
                    'langsContent' => $langsContent,
                    'langs' => Lang::find()->all(),
                ]);
                
            }else{
                throw new \yii\web\NotFoundHttpException(); 
            }            
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }
    }

    public function rewriteArrModels($models)
    {
        $arrModels = [];
        $langs = Lang::find()->all();
        foreach ($langs as $key =>$lang) {
            $arrModels[$lang->url] = $models[$key];
        }
        return $arrModels;
    }


    public function createLangModels()
    {
        $arrModels = [];
        $langs = Lang::find()->all();
        foreach ($langs as $lang) {
            $arrModels[$lang->url] = new CategoryContent();
        }
        return $arrModels;
    }

    public function writeLangModels($langsContent, $join_id)
    {
        $langs = Lang::find()->all();
        foreach ($langs as $lang) {
            $langsContent[$lang->url]->join_id = $join_id;
            $langsContent[$lang->url]->lang = $lang->url;
            foreach ($_POST[$lang->url] as $post_key => $post_value) {
                $langsContent[$lang->url]->$post_key = $post_value;
            }
            $langsContent[$lang->url]->save();
        }
    }
    
}
