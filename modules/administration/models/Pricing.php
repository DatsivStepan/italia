<?php

namespace app\modules\administration\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Lang;
use app\modules\administration\models\PricingContent;

//status: 1 - продается, 2 - Продано, 3 - в архиве
class Pricing extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%pricing}}';
    }

    public function scenarios()
    {
        return [
            'add' => ['months_count', 'product_count', 'price', 'price_to_one', 'price_to_long', 'mouth_to_long', 'description', 'message'],
            'update' => ['months_count', 'product_count', 'price', 'price_to_one', 'price_to_long', 'mouth_to_long', 'description', 'message'],
        ];
    }
    
    public function rules()
    {
        return [
            [['months_count', 'product_count', 'price', 'price_to_one', 'price_to_long', 'mouth_to_long', 'message'], 'required']
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
        ];
    }

    public function getName()
    {
        $leng = Lang::getCurrent();
        $PricingContent = PricingContent::findOne([
            'join_id' => $this->id,
            'lang' => $leng->url,
        ]);
        return $PricingContent->name;
    }

    public function getDescriptionText()
    {
        $leng = Lang::getCurrent();
        $PricingContent = PricingContent::findOne([
            'join_id' => $this->id,
            'lang' => $leng->url,
        ]);
        return $PricingContent->description;
    }

    public function getTranslation()
    {
        return $this->hasMany(PricingContent::className(), ['join_id' => 'id']);
    }

}
