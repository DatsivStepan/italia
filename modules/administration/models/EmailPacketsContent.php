<?php
namespace app\modules\administration\models;

use yii\db\ActiveRecord;
use Yii;

class EmailPacketsContent extends \yii\db\ActiveRecord
{
	public static function tableName()
    {
        return 'email_packets_content';
    }
}