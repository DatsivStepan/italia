<?php

namespace app\modules\administration\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Lang;
use app\modules\administration\models\EmailPacketsContent;

class EmailPackets extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%email_packets}}';
    }
    
    public function rules()
    {
        return [
            [['emails_count', 'price', 'product'], 'required']
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function getName()
    {
        $leng = Lang::getCurrent();
        $EmailsContent = EmailPacketsContent::findOne([
            'join_id' => $this->id,
            'lang' => $leng->url,
        ]);
        return $EmailsContent->name;
    }

    public function getTranslation()
    {
        return $this->hasMany(EmailPacketsContent::className(), ['join_id' => 'id']);
    }

}
