<?php
namespace app\modules\administration\models;

use yii\db\ActiveRecord;
use Yii;

class PricingContent extends \yii\db\ActiveRecord
{
	public static function tableName()
    {
        return 'pricing_content';
    }
    public function attributeLabels()
    {
        return [
            'description' => Yii::t('app', 'Описание Пакета'),
        ];
    }
}