<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    use app\models\Lang;

    $langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/' . Lang::getCurrent()->url : '';   
    $this->title = Yii::t('main','Абонементы и Пакеты');
?>
<div class="container  style-padding-ul">
    <ul class="breadcrumb my-breadcrumb-style">
        <li>
            <a href="../" class="style-a-color"><?= Yii::t('main','home') ?></a>
        </li>
        <li>
            <a class="style-a-color active-color"><?= $this->title?></a>
        </li>
    </ul>
</div>
<div class="investicii">
    <div class="hidden-xs container">
        <div class="col-xs-12 padd-zero" style="padding-top: 10px !important;">

        </div>
    </div>
    <?= $this->render('/default/menu'); ?>
    <div class="katalog-content-style">
        <div class="container clas_cabin_no_conteiner22">
            <div class="di_mar_top dv_centr"><span class="sp_black">Абонементы</span></div>
            <div class="wh_block_C">
                <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12 p_pp_0 no-padding no_pad_0">
                    
                    <?php foreach($modelPricings as $key => $pricing){ ?>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mar_topp_33 no-padding p_pp_0 no_pad_0">
                            <div class="col-lg-1 col-md-2 col-sm-2  col-xs-1 no-padding no_pad_0">
                                <img src="/img/Ellipse1.png">
                                <span class="spa_pos_ab_l"><?= $key+1; ?></span>
                            </div>
                            <div class="col-lg-11 col-md-10 col-sm-10 col-xs-11 no-padding no_pad_0">
                                <div class="col-sm-12 paaa_lef p_pp_0 no_pad_0">
                                    <span class="cc_spa_paket">Пакет “<?= $pricing['name']; ?>”</span>
                                </div>
                                
                                <div class="col-sm-9 pp_mar_2d p_pp_0 no_pad_0 same-text-packet-black">
                                    <?=$pricing->descriptionText?>
                                </div>
                                <div class="col-sm-12 p_pp_0 no_pad_0">
                                    <span class="cc_spa_sr33_span">Цена: <?=$pricing['price_to_one']?> €</span>
                                </div>
                                <div class="col-sm-12 p_pp_0 no_pad_0 cont_for_two_price_but">
                                    <a href="<?=$langLink?>/pay/pay-first-steep?model=tarif&type=tarif_one&id=<?= $pricing['id']?>" class="a_oplatyty_a">Оплатить за мясяц </a>
                                </div>
                                <div class="col-sm-12 p_pp_0 no_pad_0">
                                    <span class="cc_spa_sr33_span">Цена: <?=$pricing['price_to_long']* $pricing['mouth_to_long']?> €</span>
                                </div>
                                <div class="col-sm-12 pp_mar_2d2e p_pp_0 no_pad_0 cont_for_two_price_but">
                                    <a href="<?=$langLink?>/pay/pay-first-steep?model=tarif&type=tarif_standart&id=<?= $pricing['id']?>" class="a_oplatyty_a">Оплатить за <?= $pricing['mouth_to_long']?> мясяцев</a>
                                </div>
                            </div>
                        </div>
                    <?php $tarifNumb = $key; } ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mar_topp_33 no-padding p_pp_0 no_pad_0">
                            <div class="col-lg-1 col-md-2 col-sm-2  col-xs-1 no-padding no_pad_0">
                                <img src="/img/Ellipse1.png">
                                <span class="spa_pos_ab_l"><?= $tarifNumb+2; ?></span>
                            </div>
                            <div class="col-lg-11 col-md-10 col-sm-10 col-xs-11 no-padding no_pad_0">
                                <div class="col-sm-12 paaa_lef p_pp_0 no_pad_0">
                                    <span class="cc_spa_paket">Пакет “<?= $premiumPacket->name ?>”</span>
                                </div>
                                <div class="col-sm-9 pp_mar_2d p_pp_0 no_pad_0 same-text-packet-black">
                                    <?=$premiumPacket->description?>
                                </div>
                                <div class="col-sm-12 p_pp_0 no_pad_0">
                                    <span class="cc_spa_sr33_span">Цена:  <?= $premiumPacket->price ?>€</span>
                                </div>
                                <div class="col-sm-12 p_pp_0 no_pad_0 cont_for_two_price_but">
                                    <a href="<?=$langLink?>/pay/pay-first-steep?model=tarif&type=premium" class="a_oplatyty_a">Оплатить за мясяц</a>
                                </div>
                            </div>
                        </div>
                    
                </div>
            </div>
        </div>
        <div class="container clas_cabin_no_conteiner22">
            <div class="di_mar_top dv_centr"><span class="sp_black">Дополнительные Пакеты</span></div>
            <div class="wh_block_C">
                <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12 p_pp_0 no-padding no_pad_0">
                    
                    <?php foreach($emailmodel as $key => $pricing){ ?>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mar_topp_33 no-padding p_pp_0 no_pad_0">
                            <div class="col-lg-1 col-md-2 col-sm-2  col-xs-1 no-padding no_pad_0">
                                <img src="/img/Ellipse1.png">
                                <span class="spa_pos_ab_l"><?= $key+1; ?></span>
                            </div>
                            <div class="col-lg-11 col-md-10 col-sm-10 col-xs-11 no-padding no_pad_0">
                                <div class="col-sm-12 paaa_lef p_pp_0 no_pad_0">
                                    <span class="cc_spa_paket">Пакет “<?= $pricing['name']; ?>”</span>
                                </div>
                                <div class="col-sm-12 pp_mar p_pp_0 no_pad_0">
                                    <p class="cc_spa_sroc">Возможности:</p>
                                    <?php if($pricing['emails_count']!==0): ?>
                                    <p class="cc_spa_sroc ">+<?= $pricing['emails_count']; ?> email с данными клиента.</p>
                                    <?php endif; ?> 
                                    <?php if ($pricing['product']!==0): ?>
                                    <p class="cc_spa_sroc ">+<?= $pricing['product']; ?> объявлений</p>
                                    <?php endif; ?>   
                                </div>
                                <div class="col-sm-12 pp_mar_2d p_pp_0 no_pad_0">
                                    <span class="cc_spa_sr33_span">Цена: <?= $pricing['price']; ?> €</span>
                                </div>
                                <div class="col-sm-12 pp_mar_2d2e p_pp_0 no_pad_0">
                                    <a href="<?=$langLink?>/pay/pay-first-steep?model=packet&id=<?= $pricing['id']?>" class="a_oplatyty_a">Оплатить</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    
                </div>
            </div>
        </div>
    </div>
</div>