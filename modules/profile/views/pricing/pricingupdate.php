<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    use app\models\Lang;

    $langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/' . Lang::getCurrent()->url : '';
    
    $this->title = Yii::t('main','Мои услуги');
?>
<div class="container  style-padding-ul">
    <ul class="breadcrumb my-breadcrumb-style">
        <li>
            <a href="../" class="style-a-color"><?= Yii::t('main','home') ?></a>
        </li>
        <li>
            <a class="style-a-color active-color"><?= $this->title?></a>
        </li>
    </ul>
</div>
<div class="investicii">
    <div class="hidden-xs container">
        <div class="col-xs-12 padd-zero" style="padding-top: 10px !important;">
        </div>
    </div>
    <?= $this->render('/default/menu'); ?>
    <div class="katalog-content-style">
        <div class="container clas_cabin_no_conteiner22">
                        <div class="di_mar_top dv_centr"><span class="sp_black">Мои услуги</span></div>
                        <div class="wh_block_C">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p_pp_0">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 p_pp_0">
                                    <div class="mar_tex_pod">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 p_pp_0">
                                            <span class="sp_tex_pod">Доступно объявлений:</span>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 div_bal_price_2 p_pp_0">
                                            <?php if(isset($activePaket) and $activePaket->type == 'premium'): ?>
                                            <span>(количество неограниченное)</span>
                                            <?php else: ?>
                                            <span><?= $activePaket ? $activePaket->countProdLimit - $productCount : 'нет активного пакета'?></span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 p_pp_0">
                                            <span class="sp_tex_pod">Доступно сообщений:</span>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 div_bal_price_2 p_pp_0">
                                            <?php if(isset($activePaket) and $activePaket->type == 'premium'): ?>
                                            <span>(количество неограниченное)</span>
                                            <?php else: ?>
                                            <span><?=$activePaket ? $activePaket->countMessage  : 'нет активного пакета'?></span>
                                            <?php endif; ?>
                                        </div>
                                        <?php if(isset($activePaket) and $activePaket->type == 'premium'): ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 p_pp_0">
                                            <span class="sp_tex_pod">Доступно Поднятий в ТОП:</span>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 div_bal_price_2 p_pp_0">
                                            <span><?=$activePaket->premiumVipCount?></span>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 p_pp_0 mar_booot_22 text-center-mob  no-padding">
                                    <div class="mar_tex_pod tarif_packet_table">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p_pp_0 table_title">
                                            <span class="sp_tex_pod">Абонементы:</span>
                                        </div>

                                        <?php if($enableTarif): ?>
                                        <div class="row table_headers">
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33">Название:</span>
                                            </div>
                                            <div class="col-lg-2 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33">Стоимость:</span>
                                            </div>
                                            <div class="col-lg-2 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33">Дата:</span>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33">Описание:</span>
                                            </div>                                            
                                        </div>
                                        <?php foreach ($enableTarif as $key => $paket): ?>
                                        <div class="row table-row">
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33"><?=$paket->name?> ( <?=$paket->mouths?> )</span>
                                            </div>
                                            <div class="col-lg-2 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33"><?=$paket->packetPrice?> €</span>
                                            </div>
                                            <div class="col-lg-2 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33"><?=$paket->date_create?></span>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33"><?=$paket->description?></span>
                                            </div>                                            
                                        </div>
                                        <?php endforeach; ?>
                                        <?php else: ?>
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6 col-sm-12">
                                                    <span class="sp_tex_comment_33">Нет активных тарифов</span>
                                                </div>                                          
                                            </div>
                                        <?php endif; ?>


                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p_pp_0 table_title">
                                            <span class="sp_tex_pod">Пакеты:</span>
                                        </div>

                                        <?php if($enablePaket): ?>
                                        <div class="row table_headers">
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33">Название:</span>
                                            </div>
                                            <div class="col-lg-2 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33">Стоимость:</span>
                                            </div>
                                            <div class="col-lg-2 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33">Дата:</span>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33">Описание:</span>
                                            </div>                                            
                                        </div>
                                        <?php foreach ($enablePaket as $key => $paket): ?>
                                        <div class="row table-row">
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33"><?=$paket->name?></span>
                                            </div>
                                            <div class="col-lg-2 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33"><?=$paket->paket->price?> €</span>
                                            </div>
                                            <div class="col-lg-2 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33"><?=$paket->date_create?></span>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <span class="sp_tex_comment_33">Пакет активен</span>
                                            </div>                                            
                                        </div>
                                        <?php endforeach; ?>
                                        <?php else: ?>
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6 col-sm-12">
                                                    <span class="sp_tex_comment_33">Нет активных пакетов</span>
                                                </div>                                          
                                            </div>
                                        <?php endif; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>
</div>