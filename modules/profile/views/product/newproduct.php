<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    use yii\helpers\ArrayHelper;
    
    $this->title = Yii::t('main','Add_new_ad');
?>

 <?php
    if(Yii::$app->session->hasFlash('product_not_added')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-error',
                ],
                'body' => 'Объявления не добавлено',
        ]);
    endif;
?>
<div class="container  style-padding-ul">
    <ul class="breadcrumb my-breadcrumb-style">
        <li>
            <a href="../" class="style-a-color"><?= Yii::t('main','home') ?></a>
        </li>
        <li>
            <a class="style-a-color active-color"><?= $this->title?></a>
        </li>
    </ul>
</div>
<div class="investicii">
    <div class="hidden-xs container">
        <div class="col-xs-12 padd-zero" style="padding-top: 10px !important;">
        </div>
    </div>
    <?= $this->render('/default/menu'); ?>
    
    <div class="container-fluid">
    <?php $form = ActiveForm::begin(['options' => ['target' => '_blank']]) ?>
        <div class="katalog-content-style">
            <div class="container clas_cabin_no_conteiner22">
            <div class="di_mar_top2"><span class="sp_black"><?= Yii::t('main','Add_new_ad') ?></span></div>
            <div class="col-sm-12 pad_lef_0">
                <ul class="nav nav-tabs">
                    <?php foreach ($langs as $key =>$lang): ?>
                    <li class="<?=$key==0 ? 'active' : '' ?>">
                        <a data-toggle="tab" href=".lang<?=$lang->id?>">
                        <img src="<?=Url::home().'flags/'.$lang->icon?>">
                        <span><?=$lang->name?></span>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>
                <div class="col-md-8 col-sm-12 no-padding no-padding444">
                    <div class="col-sm-12 otstup33 otstup4_text">

                        <div class="tab-content"> 
                        <?php foreach ($langs as $key =>$lang): ?>
                        <div class="lang<?=$lang->id?> tab-pane fade <?=$key==0 ? 'in active' : '' ?>">
                        <?= $form->field($langsContent[$lang->url], 'name')->textinput([
                                                'name' => $lang->url.'[name]',
                                                'required' => $key==0 ? true : false,
                                                'class' => 'form-style form_style_tup',
                                            ])->label(' '.Yii::t('main','name_of_the_property').'('.$lang->url.')'); ?>
                        </div>
                        <?php endforeach; ?>
                        </div>
                        
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 otstup22 otstup4_text">
                            <?= $form->field($modelNewProduct, 'product_type_id')->dropDownList($arrayProductType,
                                [
                                    'class' => 'form-style select-style form_style_tup',
                                    'prompt' => 'Select Type'
                                ]
                            ); ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 otstup33 otstup4_text">
                        <?= $form->field($modelNewProduct, 'category_id')->dropDownList(['Select Category'],
                            [
                                'class' => 'form-style select-style form_style_tup'
                            ]
                        ); ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 otstup33 otstup4_text">
                        <?= $form->field($modelNewProduct, 'object_id')->dropDownList([1=>'Коттедж', 2=>'Дом'],
                            [
                                'class' => 'form-style select-style form_style_tup',
                                'prompt' => 'Select Type Obj'
                            ]
                        ); ?>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 otstup44 otstup4_text">
                        <?= $form->field($modelNewProduct, 'region_id')->dropDownList(ArrayHelper::map($states, 'id', 'name') ,[
                            'class' => 'form-style select-style form_style_tup',
                            'prompt' => 'Select State',
                            
                            ]); ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 otstup44 otstup4_text">
                        <?= $form->field($modelNewProduct, 'city_id')->dropDownList(['Select City'],['class' => 'form-style select-style form_style_tup',
                            'disabled' => true
                        ]); ?>
                        
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 no-padding no-padding333 otstup4_text">
                    <div class="tab-content"> 
                    <?php foreach ($langs as $key =>$lang): ?>
                    <div class="lang<?=$lang->id?> tab-pane fade <?=$key==0 ? 'in active' : '' ?>">
                    <?= $form->field($langsContent[$lang->url], 'content')->textarea([
                                        'class' => 'form-style full-height-textarea', 
                                        'name' => $lang->url.'[content]',
                                        'required' => $key==0 ? true : false,
                                        'placeholder'=> 'Инструкция: Город – тип объекта (квартира, дом, вилла..) + колиечество комнат + главная характеристика объекта (с видом на море, в 100 м от пляжа, в центральной части города)
Пример: Лидо ди Езоло – вилла - 3 спальни – 5 минут до моря.',
                                    ])->label(' '.Yii::t('main','Description').' ('.$lang->url.')'); ?>
                    </div>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
            </div>
        
            <div class="bord_bot_22"></div>
            <input type="hidden" id="product-image_array" name="Product[image_array]" value="">

            <div class="container clas_cabin_no_conteiner22">
                <div class="col-xs-12 no-padding no_padddd">
                    <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding no_padddd">
                        <div class="mar_tex_pod"><span class="sp_tex_pod"><?= Yii::t('main','Upload_photo') ?>:</span></div>
                        <div style="clear: both"></div>
                        <div class="owl-carousel owl-theme owl-carousel-product">
                            
                            <div class="item_img33" style='height:auto;padding:5px;'>
                                <div>
                                    <?= Yii::t('main','Main_photo') ?>
                                </div>
                                <div style="position:relative;">
                                    <div class='NewProductImageBlock addPhoto' data-delete_status="no" style='background-image: url(/images/default_avatar.jpg);'>
                                    </div>
                                    <div class="deleteProductImage" data-name="" style='display:none;'> 
                                        <span>Удалить</span>
                                    </div>
                                </div>
                            </div>
                            <?php for ($i = 1; $i <= 9; $i++) { ?>
                                <div class="item_img33" style='height:auto;padding:5px;'>
                                    <div>
                                        <?= Yii::t('main','photo') ?> <?= $i+1; ?>:
                                    </div>
                                    <div style="position:relative;">
                                        <div class='NewProductImageBlock addPhoto' data-delete_status="no" style='background-image: url(/images/default_avatar.jpg);'>
                                        </div>
                                        <div class="deleteProductImage" data-name="" style='display:none;'>
                                            <span>Удалить</span>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="bord_bot_b_33"></div>
            <div class="container clas_cabin_no_conteiner22">
                <div class="col-sm-12 pad_lef_555">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 otstup22 prod-static-bl-height">
                                <?= $form->field($modelNewProduct, 'object_area')->textinput(
                                [
                                    'class' => 'form-style form_style_tup ',
                                    'type' => 'number',
                                    'min' => 0,
                                    ]); ?>
                            </div>
                            <div class="col-md-3 col-sm-3 otstup22 prod-static-bl-height">
                                <?= $form->field($attrModel, 'total_area')->textinput(
                                    [
                                        'class' => 'form-style form_style_tup', 
                                        'type' => 'number',
                                        'min' => 0,
                                    ])->label('Площадь земли'); ?>
                            </div>
                            <div class="col-md-3 col-sm-3 otstup22 prod-static-bl-height">
                                <?= $form->field($attrModel, 'area_yard')->textinput(
                                    [
                                        'class' => 'form-style form_style_tup', 
                                        'type' => 'number',
                                        'min' => 0,
                                    ])->label('Площадь двора'); ?>
                            </div>
                        </div>
                        <div class="row">                            
                            <div class="col-md-3 col-sm-3 col-xs-12 otstup33">
                                <?= $form->field($modelNewProduct, 'year_of_construction')->textinput(
                                [
                                    'class' => 'form-style form_style_tup form_style_tup222',
                                    'type' => 'number',
                                    'min' => 0,
                                ]); ?>
                            </div>
                            <div class="col-md-3 col-sm-3 otstup22 otstup33">
                                <?= $form->field($attrModel, 'year_restavration')->textinput(
                                    [
                                        'class' => 'form-style form_style_tup', 
                                        'type' => 'number',
                                        'min' => 0,
                                    ])->label('Год реставрации'); ?>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-sm-4 col-xs-12 otstup33 prod-static-bl-height">
                                <?=
                                    $form->field($modelNewProduct, 'address_status')
                                        ->radioList(
                                            [1 => Yii::t('main','yes'), 0 => Yii::t('main','no')],
                                            [
                                                'item' => function($index, $label, $name, $checked, $value) {
                                                    $checked_val = $index == 0 ? 'checked' : '';
                                                    $return = '<input type="radio" '.$checked_val.' name="' . $name . '" value="' . $value . '" tabindex="3">';
                                                    $return .= '<label>' . ucwords($label) . '</label>';
                                                    return $return;
                                                }
                                            ]
                                        );
                                ?>
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-12 otstup33">
                                <div id="adress_promt">
                                    <label class="control-label" for="product-address">Адрес объекта на карте</label>
                                    <span>(можно указывать приблизительный адрес объекта)</span>
                                </div>
                                <?= $form->field($modelNewProduct, 'address')->textinput(['class' => 'form-style form_style_tup'])->label(false); ?>
                            </div>
                        </div>
                    
                </div>
            </div>
            <div class="bord_bot_b_444"></div>            
            <div class="container clas_cabin_no_conteiner22">
                <div class="col-sm-12 pad_lef_555">
                    <div class="col-sm-9 no-padding">
                        <div class="col-md-2 col-sm-2 otstup22">
                            <?= $form->field($attrModel, 'room')->textinput(
                            [
                                'class' => 'form-style form_style_tup', 
                                'type' => 'number',
                                'value' => 2,
                                'min' => 1,
                                'max' => 50
                            ]); ?>

                        </div>
                        <div class="col-md-2 col-sm-2 otstup22">
                            <?= $form->field($attrModel, 'bathroom')->textinput(
                            [
                                'class' => 'form-style form_style_tup', 
                                'type' => 'number',
                                'value' => 1,
                                'min' => 1,
                                'max' => 30
                            ]); ?>

                        </div>
                        <div class="col-md-2 col-sm-2 otstup22">
                            <?= $form->field($attrModel, 'garage')->textinput(
                            [
                                'class' => 'form-style form_style_tup', 
                                'type' => 'number',
                                'value' => 1,
                                'min' => 0,
                                'max' => 20
                            ]); ?>

                        </div>
                        <div class="col-md-2 col-sm-2 otstup22">
                            <?= $form->field($attrModel, 'parking')->dropDownList([0 => 'нет', 1 => 'есть'],['class' => 'form-style select-style form_style_tup']); ?>                       
                        </div>
                        <div class="col-md-2 col-sm-2 otstup22">
                            <?= $form->field($attrModel, 'balcony')->dropDownList([0 => 'нет', 1 => 'есть'],['class' => 'form-style select-style form_style_tup']); ?>                       
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 pad_lef_555">
                    <div class="col-sm-9 no-padding">
                        <div class="col-md-2 col-sm-2 otstup22">
                            <?= $form->field($attrModel, 'yard')->dropDownList([0 => 'нет', 1 => 'есть'],['class' => 'form-style select-style form_style_tup']); ?>                       
                        </div>
                        <div class="col-md-2 col-sm-2 otstup22">
                            <?= $form->field($attrModel, 'pool')->dropDownList([0 => 'нет', 1 => 'есть'],['class' => 'form-style select-style form_style_tup']); ?>                       
                        </div>
                        <div class="col-md-2 col-sm-2 otstup22">
                            <?= $form->field($attrModel, 'furniture')->dropDownList([0 => 'нет', 1 => 'есть'],['class' => 'form-style select-style form_style_tup']); ?>                       
                        </div>
                        <div class="col-md-2 col-sm-2 otstup22">
                            <?= $form->field($attrModel, 'elevator')->dropDownList([0 => 'нет', 1 => 'есть'],['class' => 'form-style select-style form_style_tup']); ?>                       
                        </div>
                        <div class="col-md-2 col-sm-2 otstup22">
                            <?= $form->field($attrModel, 'energo_class')->dropDownList(
                                [0 => 'A++', 1 => 'A+', 2=>'A', 3=>'B', 4=>'C', 5=>'D', 6=>'E', 7=>'F', 8=>'G'],
                                [
                                    'class' => 'form-style select-style form_style_tup',
                                    'value' => 3.
                                ]); ?>                       
                        </div>
                    </div>
                </div>
            </div>
            <div class="bord_bot_b_444"></div>
            <div class="container clas_cabin_no_conteiner22">
                <div class="col-sm-12 pad_lef_555">
                    <div class="col-sm-9 no-padding">
                        <div class="col-md-3 col-sm-3 otstup22">
                            <?= $form->field($modelNewProduct, 'price')->textinput(
                            [
                                'class' => 'form-style form_style_tup', 
                                'type' => 'number',
                                'min' => 0,
                                'step'=>1000,
                                'value'=> 0
                            ]); ?>
                        </div>
                        <div class="col-md-3 col-sm-3 otstup22 center-currency-bl">
                            <p>€</p>
                        </div>
                        <div class="col-md-3 col-sm-3 otstup22 hidden">
                            <?= $form->field($modelNewProduct, 'currency')->dropDownList([0 => 'Euro'],['class' => 'form-style select-style form_style_tup']); ?>                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="bord_bot_b_444"></div>
            <div class="container clas_cabin_no_conteiner22">
                <div class="col-sm-12 pad_lef_555">
                    <div class="col-md-9 col-sm-11 col-xs-12 no-padding">
                        <div class="col-md-4 col-sm-4 col-xs-12 otstup22 ots_mar_45">
                            <?= Html::submitButton(Yii::t('main', 'Сохранить объявление'), 
                            [
                                'class' => 'btn bt_bot_bb_3',
                                'name' => 'save_butt',
                                'value' => 'save'
                            ]) ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 otstup22 ots_mar_45">
                            <?= Html::submitButton(Yii::t('main', 'Посмотреть объявление'), 
                            [
                                'class' => 'btn bt_bot_bb_3',
                                'name' => 'look_butt',
                                'value' => 'look'
                            ]) ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 otstup22 ots_mar_45">
                            <?= Html::submitButton(Yii::t('main', 'Publish_an_ad'), 
                            [
                                'class' => 'btn bt_bot_bb_3',
                                'name' => 'public_butt',
                                'value' => 'public'
                            ]) ?>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12 otstup22">
                            <span class="span_otvet_33"><?= Yii::t('main','The_answer_will_come_to_your_e_mail_thanks_for_contacting') ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    </div> 
</div>
                

<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>