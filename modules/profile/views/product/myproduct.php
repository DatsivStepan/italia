<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use app\models\Lang;

$langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/' . Lang::getCurrent()->url : '';
$this->title = Yii::t('main', 'my_announcements');
?>
<?php
if (Yii::$app->session->hasFlash('product_added')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Объявления добавлено',
    ]);
endif;

if (Yii::$app->session->hasFlash('product_deleted')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Объявления удалино',
    ]);
endif;

if (Yii::$app->session->hasFlash('product_not_deleted')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Объявления не удалино',
    ]);
endif;
if (Yii::$app->session->hasFlash('product_update')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Объявления Обновленно',
    ]);
endif;
?>
<div class="container  style-padding-ul">
    <ul class="breadcrumb my-breadcrumb-style">
        <li>
            <a href="../" class="style-a-color"><?= Yii::t('main','home') ?></a>
        </li>
        <li>
            <a class="style-a-color active-color"><?= $this->title?></a>
        </li>
    </ul>
</div>
<div class="investicii">
    <div class="hidden-xs container">
        <div class="col-xs-12 padd-zero" style="padding-top: 10px !important;">
        </div>
    </div>
    <?= $this->render('/default/menu'); ?>
    <div class="katalog-content-style">
        <div class="katalog-content-style">
            <div class="container clas_cabin_no_conteiner22">
                <div class="di_mar_top dv_centr"><span class="sp_black"><?= $this->title?></span>
                </div>
                <span class="sp_texxx2"><?= Yii::t('main', 'you') ?> <?= $stringMyProductCount; ?> <?= Yii::t('main', 'Ads') ?></span>
                <div class="col-xs-12 padd-zero">
                    <div class="tab-katalog" id="katalog-panel1">
                        <?php //var_dump($modelProduct); ?>
                        <div class="col-xs-12 padd-zero margin-katalog-panel">
                            <?php foreach ($modelProduct as $product) { ?>
                                <div class="content-class-style">
                                    <a href="<?= $langLink ?>/product/<?= $product['id']; ?>">
                                        <?php $img_src = ''; ?>
                                        <?php
                                        if (($product['img_src'] != '') && ($product['img_src'] != null)) {
                                            $img_src = '/images/product/' . $product['img_src'];
                                        }
                                        ?>
                                        <div class="col-xs-12 hover-div-slider padd-zero"
                                             style="background: url(<?= $img_src; ?>) no-repeat; background-size: cover;">
                                            <div class="change_hover_all">
                                                <div class="col-xs-12 padd-zero hov_img_none" style="opacity: 0">
                                                </div>
                                                <div class="change-hover">
                                                    <?php if($product->status < 10 ): ?>
                                                    <a class="top_button edit_button button_display col-xs-12" href="<?= $langLink ?>/profile/product/prodpubl?id=<?= $product['id']; ?>">Опубликовать</a>   
                                                    <?php elseif($product->vip == NULL): ?>
                                                    <a class="top_button edit_button button_display col-xs-12" href="<?= $langLink ?>/pay/pay-first-steep?model=top&id=<?= $product['id']; ?>">Поднять в ТОП</a>
                                                    <?php endif; ?>
                                                    <a class="edit_button button_display col-xs-12"
                                                       href="<?= $langLink ?>/profile/product/updateproduct?id=<?= $product['id']; ?>"><?= Yii::t('main', 'Edit') ?></a>
                                                    <a class="unistall_button button_display col-xs-12"
                                                       href="<?= $langLink ?>/profile/product/deleteproduct?id=<?= $product['id']; ?>"><?= Yii::t('main', 'Uninstall') ?></a>
                                                    <div class="col-xs-12 padd-zero style_border"
                                                         style="padding-bottom: 5px !important;">
                                                        <div class="col-xs-12">
                                                            <p class="title_room">
                                                                <?= isset($product['name']) ? $product['name'] : ''; ?>
                                                            </p>
                                                            <div class="width_style">
                                                                <span class="span-title"><?= Yii::t('main', 'Category') ?>
                                                                    :</span>
                                                                <span class="span-text">
                                                                <?= isset($product->category->name) ? $product->category->name : ''; ?>
                                                                </span>
                                                            </div>
                                                            <div class="width_style">
                                                                <span class="span-title"><?= Yii::t('main', 'Area') ?>
                                                                    :</span>
                                                                <span class="span-text">
                                                                <?= $product['object_area'] != 0 ? $product['object_area'] . 'м2' : ''; ?>
                                                                </span>
                                                            </div>
                                                            <div class="width_style">
                                                                <span class="span-title"><?= Yii::t('main', 'Region') ?>
                                                                    :</span>
                                                                <span class="span-text">
                                                                    <?= isset($product->region->name) ? $product->region->name : ''; ?>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 padd-zero"
                                                         style="padding-top: 5px !important;">
                                                        <div class="col-sm-7 col-xs-7">
                                                            <?php
                                                            $currency = '';
                                                            switch ($product['currency']) {
                                                                case 0:
                                                                    $currency = "€";
                                                                    break;
                                                                case 1:
                                                                    $currency = "$";
                                                                    break;
                                                                case 2:
                                                                    $currency = "руб";
                                                                    break;
                                                            }
                                                            ?>
                                                            <p class="money_style">
                                                                <?= $product['price'] != 0 ? $currency . $product['price'] : ''; ?>

                                                            </p>
                                                        </div>
                                                        <div class="col-sm-2 col-xs-2 padd-zero">
                                                            <img class="style-img shower_class"
                                                                 src="/img/shower_icon.png">
                                                            <span class="numb-style-text"><?=$product->prodAttr->bathroom?></span>
                                                        </div>
                                                        <div class="col-sm-3 col-xs-3 padd-zero ">
                                                            <img class="style-img bed_class" src="/img/bed_icon.png">
                                                            <span class="numb-style-text"><?=$product->prodAttr->room?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>