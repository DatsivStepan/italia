<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    
    $this->title = Yii::t('main','Editing_a_profile');
?>
<?php
    if(Yii::$app->session->hasFlash('data_saved')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-info',
                ],
                'body' => 'Data saved',
        ]);
    endif;
    if(Yii::$app->session->hasFlash('data_not_saved')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-error',
                ],
                'body' => 'Date not saved!',
        ]);
    endif; 
?>

<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>
<div class="container  style-padding-ul">
    <ul class="breadcrumb my-breadcrumb-style">
        <li>
            <a href="../" class="style-a-color"><?= Yii::t('main','home') ?></a>
        </li>
        <li>
            <a class="style-a-color active-color"><?= $this->title?></a>
        </li>
    </ul>
</div>
<div class="investicii">
    <div class="hidden-xs container">
        <div class="col-xs-12 padd-zero" style="padding-top: 10px !important;">
        </div>
    </div>
    <?= $this->render('menu'); ?>
    <div class="katalog-content-style">
        <div class="container clas_cabin_no_conteiner22">
        <div class="di_mar_top"><span class="sp_black"><?= Yii::t('main','Editing_a_profile') ?></span></div>
        <div class="wh_block_C">
            <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12 p_pp_0 no-padding no_pad_0 my-marg-upd-page">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mar_topp_33 no-padding p_pp_0 no_pad_0 no_pad_255">
                    <?php $avatar = '/images/default_avatar.jpg'; ?>
                    <?php if($modelUser->userinfo->avatar != ''){ ?>
                        <?php $avatar = '/'.$modelUser->userinfo->avatar; ?>
                    <?php } ?>
                    <img class="userProfileImage" src="<?= $avatar; ?>" >

                    <div class="blockSavePhotoButton" style="text-align: center;display:none;">
                        <input type="hidden" name="image_prodile_src" value="<?= $avatar; ?>">
                        <a class="saveProfilePhoto"><?= Yii::t('main','Save') ?></a>
                    </div>
                    <div class="change_photo" style="text-align:center;">
                        <a class="changeProfilePhoto sp_izm_logo"><?= Yii::t('main','Change_logo') ?></a>
                    </div>                        
                </div>
                
                <?php $form = ActiveForm::begin(); ?>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 mar_topp_33 no-padding p_pp_0 no_pad_0 pos_rel_right">
                        <div class="col-sm-10 otstup22">

                            <label><?= Yii::t('main','Login') ?></label>
                            <?= $form->field($modelUser, 'username')->textinput(['class' => 'form-style form_style_tup'])->label(false) ?>
                        </div>
                        <div class="col-sm-10 otstup22 ots_mar_top0">
                            <label><?= Yii::t('main','Password') ?></label>
                            <?= $form->field($modelUser, 'password')->passwordInput(['class' => 'form-style form_style_tup'])->label(false); ?>
                        </div>
                        <div class="col-sm-10 otstup22 ots_mar_top0 ma_mat_50">
                            <label><?= Yii::t('main','Password_Repeat') ?></label>
                            <?= $form->field($modelUser, 'password_repeat')->passwordInput(['class' => 'form-style form_style_tup'])->label(false); ?>
                        </div>
                        <div class="col-sm-12 padd-zero">
                            <?= Html::submitButton(Yii::t('main', 'Apply_Changes'), ['class' => 'btn btn-primary my-button-update-profile']) ?>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 mar_topp_33 no-padding p_pp_0 no_pad_0">
                        <div class="col-sm-10 otstup22">
                            <label><?= Yii::t('main','company_name') ?></label>
                            <?= $form->field($modelUser->userinfo, 'company_name')->textinput(['class' => 'form-style form_style_tup'])->label(false); ?>
                        </div>
                        <div class="col-sm-10 otstup22 ots_mar_top0">
                            <label><?= Yii::t('main','E_mail') ?></label>
                            <?= $form->field($modelUser, 'email')->textinput(['class' => 'form-style form_style_tup'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 mar_topp_33 no-padding p_pp_0 no_pad_0 pos_rel_left ma_mat_50">
                        <div class="col-lg-10 col-md-10 otstup22">
                            <label><?= Yii::t('main','phone_number') ?></label>
                            <?= $form->field($modelUser->userinfo, 'phone_number')->textinput(['class' => 'form-style form_style_tup'])->label(false); ?>
                        </div>
                        <div class="col-lg-10 col-md-10 otstup22">
                            <label><?= Yii::t('main','address') ?></label>
                            <?= $form->field($modelUser->userinfo, 'address')->textinput(['class' => 'form-style form_style_tup'])->label(false); ?>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>  
            </div>
        </div>
        </div>
    </div>
</div>

<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>