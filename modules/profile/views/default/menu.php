
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;
use app\models\Lang;
$langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
use app\assets\ProfileAsset;
ProfileAsset::register($this);

    $class = '';

    $class = ($this->context->getRoute() == 'profile/default/update')?'active':''; 
    $class1 = ($this->context->getRoute() == 'profile/default/index')?'active':''; 
    $class2 = ($this->context->getRoute() == 'profile/product/newproduct')?'active':''; 
    $class3 = ($this->context->getRoute() == 'profile/product/myproduct')?'active':''; 
    $class4 = ($this->context->getRoute() == 'profile/pricing/index')?'active':''; 
    $class5 = ($this->context->getRoute() == 'profile/pricing/services')?'active':''; 
?>
    <button type="button" class="navbar-toggle collapsed" style="background-color: #ffffff; border: 0px"
            data-toggle="collapse" data-target="#profile-menu" aria-expanded="false"
            aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <p>Profile menu</p>
        <span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </span>
    </button>

    <div id="profile-menu" class="menu_vup col-xs-12 no-padding navbar-collapse collapse">
        <ul id="myTab" class="nav nav-tabs container clas_cabin_no_conteiner">
            <li class='<?= $class1; ?>'>
                <?= HTML::a( Yii::t('main','Profile'),  $langLink.'/profile/default/index'); ?>
            </li>
            <li class='<?= $class2; ?>'>
                <?= HTML::a(Yii::t('main','Add_new_ad'), $langLink.'/profile/product/newproduct'); ?>
            </li>
            <li class='<?= $class3; ?>'>
                <?= HTML::a(Yii::t('main','my_announcements'), $langLink.'/profile/product/myproduct', ['class' => 'nav-link '.$class]); ?>
            </li>
            <?php if(Yii::$app->user->identity->type !== 'owner'): ?>
            <li class='<?= $class5; ?>'>
                <?= HTML::a('Мои услуги', $langLink.'/profile/pricing/services'); ?>
            </li>
            <li class='<?= $class4; ?>'>
                <?= HTML::a(Yii::t('main','Абонементы и пакеты'), $langLink.'/profile/pricing/index'); ?>
            </li>
            <?php endif; ?>
            <li class='<?= $class; ?>'>
                <?= HTML::a(Yii::t('main','Edit_profile'), $langLink.'/profile/default/update'); ?>
            </li>
        </ul>
        <div class="hidden-xs bord_bott2"></div>
    </div>
