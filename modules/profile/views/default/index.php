<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;

$this->title = Yii::t('main', 'Profile');

use yii\helpers\ArrayHelper;

$arraySetting = ArrayHelper::map(app\models\Setting::find()->all(), 'key', 'value');


if(Yii::$app->session->hasFlash('not_product_limit'))
{
    echo Alert::widget([
            'options' => [
                    'class' => 'alert-warning',
            ],
            'body' => 'Лимит объявлений исчерпан',
    ]);
}
elseif (Yii::$app->session->hasFlash('paket_sucses_active')) 
{
    echo Alert::widget([
            'options' => [
                    'class' => 'alert-success',
            ],
            'body' => 'Тарифный план успешно оплачен',
    ]);
}

?>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>

<div class="container  style-padding-ul">
    <ul class="breadcrumb my-breadcrumb-style">
        <li>
            <a href="../" class="style-a-color"><?= Yii::t('main','home') ?></a>
        </li>
        <li>
            <a class="style-a-color active-color"><?= $this->title?></a>
        </li>
    </ul>
</div>
<div class="hidden-xs container"></div>
<div class="col-xs-12 padd-zero" style="padding-top: 10px !important;">
</div>
<?= $this->render('menu'); ?>
<div class="katalog-content-style">
    <div class="container clas_cabin_no_conteiner22" style="margin-top: 55px">
        <div class="di_mar_top"><span class="sp_black"><?= Yii::t('main', 'Your_profile') ?></span></div>
        <div class="wh_block_C">
            <div class="div_prof_2">
                <?php $avatar = '/images/default_avatar.jpg'; ?>
                <?php if ($modelUser->userinfo and $modelUser->userinfo->avatar != '') { ?>
                    <?php $avatar = '/' . $modelUser->userinfo->avatar; ?>
                <?php } ?>
                <img class="userProfileImage" src="<?= $avatar; ?>">

                <div class="blockSavePhotoButton" style="text-align: center;display:none;">
                    <input type="hidden" name="image_prodile_src" value="<?= $avatar; ?>">
                    <a class="saveProfilePhoto"><?= Yii::t('main', 'Save') ?></a>
                </div>
                <div class="change_photo" style="text-align:center;">
                    <a class="changeProfilePhoto sp_izm_logo"><?= Yii::t('main', 'Change_logo') ?></a>
                </div>
            </div>
            <div class="div_contact_form_2">
                <p><?= Yii::t('main', 'Login') ?>:</p>
                <span><?= $modelUser->username; ?></span>
                <p><?= Yii::t('main', 'Company') ?>:</p>
                <span>
                        <?= $modelUser->userinfo->company_name; ?>
                    </span>
                <p>E-mail:</p>
                <span>
                        <?= $modelUser->email; ?>
                    </span>
                <p><?= Yii::t('main', 'Phone') ?>:</p>
                <span>
                        <?= $modelUser->userinfo->phone_number; ?>
                    </span>
            </div>
            <?php if(Yii::$app->user->identity->type !== 'owner'): ?>
            <div class="div_taruf_form_2">
                <p><?= Yii::t('main', 'Your_tariff') ?>:</p>
                <span>
                    <?=$enablePakets ? '“'.$enablePakets->name.'”' : 'нет активного пакета'?>
                </span>
            </div>
            <div class="div_ohol_form_2">
                <p><?= Yii::t('main', 'You_posted') ?>:</p>
                <?php if(isset($enablePakets) and $enablePakets->type == 'premium'): ?>
                <span><?=$productCount?> объявлений (количество неограниченное)</span>
                <?php elseif(isset($enablePakets)): ?>
                <span><?=$productCount?> <?= Yii::t('main', 'Ads_from') ?> <?=$enablePakets->countProdLimit?> <?= Yii::t('main', 'Possible') ?></span>
                <?php else: ?>
                <span>нет активного пакета</span>
                <?php endif; ?>
            </div>
            <div class="div_balans_form_2">
                <p>Доступно сообщений:</p>
                <?php if(isset($enablePakets) and $enablePakets->type == 'premium'): ?>
                <span>(количество неограниченное)</span>
                <?php else: ?>
                <span><?=$enablePakets ? $enablePakets->countMessage  : 'нет активного пакета'?></span>
                <?php endif; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="bord_bot_22"></div>
<div class="container clas_cabin_no_conteiner22">
    <div class="col-xs-12 no-padding no_padddd" style="margin-bottom: 177px">
        <div class=" col-lg-7 col-md-7 col-sm-7 col-xs-11 no-padding no_padddd">
            <div class="mar_tex_pod"><span class="sp_tex_pod"><?= Yii::t('main', 'Contact_technical_support') ?>:</span>
            </div>
            <div class="mar_tex_vop"><span class="sp_tex_vop"><?= Yii::t('main', 'Your_question') ?>:</span></div>
            <form id="texpod_form">
                <textarea name="text" class="tex_prum_mas clear_input"
                          placeholder="<?= Yii::t('main', 'Example_message_text') ?>"></textarea>
                <input type="hidden" name="email_user" value="<?= $arraySetting['email'] ?>">
                <a class="a_smena_taruf send_texpod"><?= Yii::t('main', 'Send') ?></a>
            </form>
            <span class="span_otvet22"><?= Yii::t('main', 'The_answer_will_come_to_your_e_mail_thanks_for_contacting') ?></span>
        </div>
    </div>
</div>
