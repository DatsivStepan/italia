<?php

namespace app\modules\profile\controllers;
use yii;
use yii\web\Controller;
use app\models\User;
use app\models\Product;
use app\models\Category;
use app\models\UserInfo;
use app\models\UserPakets;
use app\models\PremiumPacket;
use app\models\UserEmailPackets;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\administration\models\EmailPackets;

/**
 * Default controller for the `profile` module
 */
class PricingController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;

        if((!\Yii::$app->user->isGuest) && (Yii::$app->user->identity->status !== '0')){
            if(Yii::$app->user->identity->status !== '0'){
                return parent::beforeAction($action);
            }else{
                return $this->redirect('site/login');
            }         
        }else{
            if(\Yii::$app->user->isGuest){
                return parent::beforeAction($action);            
               //return $this->redirect('administration'); 
            }else{
                throw new \yii\web\NotFoundHttpException();                
            }
        }
    }
    
    public function actionIndex()
    {
        $modelPricings = \app\modules\administration\models\Pricing::find()->all();
        $emailmodel = EmailPackets::find()->all();
        $premiumPacket = PremiumPacket::find()->one();
        
        return $this->render('index',[
            'modelPricings' => $modelPricings,
            'emailmodel' => $emailmodel,
            'premiumPacket'=> $premiumPacket
        ]);
    }
    
    public function actionServices()
    {
        $enableTarif = UserPakets::find()
        ->where(['user_id' => \Yii::$app->user->id])
        ->andWhere(['>', 'status', 0])
        ->all();

        $enablePaket = UserEmailPackets::find()
        ->where(['user_id' => \Yii::$app->user->id])
        ->andWhere(['>', 'status', 0])
        ->all();
        //var_dump($enablePakets);exit;
        $activePaket = UserPakets::find()->where(['user_id' => \Yii::$app->user->id, 'status'=>'10'])->one();
        $productCount = Product::find()->andWhere(['user_id' => \Yii::$app->user->id])->count();

        return $this->render('pricingupdate',[
            'enableTarif' => $enableTarif,
            'enablePaket' => $enablePaket,
            'activePaket' => $activePaket,
            'productCount' => $productCount
        ]);
    }        
    
}
