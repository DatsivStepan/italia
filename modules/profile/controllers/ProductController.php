<?php

namespace app\modules\profile\controllers;
use yii;
use yii\web\Controller;
use app\models\User;
use app\models\Product;
use app\models\ProductImage;
use app\models\ProductAttr;
use app\models\Category;
use app\models\UserInfo;
use app\models\UserPakets;
use app\models\Countries;
use app\models\States;
use app\models\Cities;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use app\models\Lang;
use app\models\ProductContent;
use yii\imagine\Image;

/**
 * Default controller for the `profile` module
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;

        if((!\Yii::$app->user->isGuest) && (Yii::$app->user->identity->status !== '0')){
            if(Yii::$app->user->identity->status !== '0'){
                return parent::beforeAction($action);
            }else{
                return $this->redirect('site/login');
            }        
        }else{
            if(\Yii::$app->user->isGuest){
                return parent::beforeAction($action);            
               //return $this->redirect('administration'); 
            }else{
                throw new \yii\web\NotFoundHttpException();                
            }
        }
    }
    
    public function actionIndex()
    {
        $modelUser = User::find()->with('userinfo')->where(['id' => \Yii::$app->user->id])->one();
        
        return $this->render('index',[
            'modelUser' => $modelUser,
        ]);
    }
    
    public function actionNewproduct()
    {
        $productCount = Product::find()->andWhere(['user_id' => \Yii::$app->user->id])->count();
        $enablePakets = UserPakets::find()->where(['user_id' => \Yii::$app->user->id, 'status'=>'10'])->one();

        if(Yii::$app->user->identity->type == 'owner' && (int)$productCount > 0)
        {
            \Yii::$app->session->setFlash('not_product_limit');
            return $this->redirect('/profile');
            
        }
        if(Yii::$app->user->identity->type !== 'owner' )
        {
            if( !$enablePakets  or (int)$productCount >= (int)$enablePakets->countProdLimit )
            {
                \Yii::$app->session->setFlash('not_product_limit');
                return $this->redirect('/profile');
            }
        }
        


        $modelNewProduct = new Product();
        $langsContent = $this->createLangModels();
        $attrModel = $this->createAttrModel();
        $modelNewProduct->scenario = 'new_product';
        $langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
        
        if(Yii::$app->request->post()){
            //var_dump($_POST);exit;
            if($modelNewProduct->load(\Yii::$app->request->post())){
                $modelNewProduct->user_id = \Yii::$app->user->id;
                if($modelNewProduct->save()){
                    $this->writeLangModels($langsContent, $modelNewProduct->id);
                    $this->writeAttrModel($attrModel, $_POST['ProductAttr'] ,$modelNewProduct->id);
                    
                    $arrayProductImage = json_decode($_POST['Product']['image_array']);                        
                    if($arrayProductImage){
                        foreach($arrayProductImage as $key => $stringImageSrc){
                            if($key == 0){
                                $modelNewProduct->scenario = 'update_image';
                                $modelNewProduct->img_src = $stringImageSrc;
                                $modelNewProduct->save();
                            }else{
                                $modelNewProductImage = new ProductImage();
                                $modelNewProductImage->scenario = 'new_product_image';
                                $modelNewProductImage->img_src = $stringImageSrc;
                                $modelNewProductImage->product_id = $modelNewProduct->id;
                                $modelNewProductImage->save();
                            }
                        }
                    }
                    \Yii::$app->session->setFlash('product_added');

                    if(isset($_POST['public_butt']))
                    {
                        unset($_POST['public_butt']);
                        $modelNewProduct->status = 10;
                        $modelNewProduct->save();
                    }elseif(isset($_POST['save_butt']))
                    {
                        unset($_POST['save_butt']);
                        $modelNewProduct->status = 5;
                        $modelNewProduct->save();
                    }elseif(isset($_POST['look_butt'])) 
                    {
                        unset($_POST['look_butt']);
                        return $this->redirect('/product/'.$modelNewProduct->id);
                    }
                    $_POST = array();

                    return $this->redirect($langLink.'/profile/product/myproduct');
                }else{
                    \Yii::$app->session->setFlash('product_not_added');                    
                }
            }
        }
        $arrayProductType = ArrayHelper::map(Category::find()->where(['parent_id' => '0'])->orWhere(['parent_id' => null])->all(), 'id', 'name');
        $states = States::find()->where(['country_id' => 107])->all();
        return $this->render('newproduct',[
            'modelNewProduct' => $modelNewProduct,
            'arrayProductType' => $arrayProductType,
            'states' => $states,
            'langsContent' => $langsContent,
            'langs' => Lang::find()->all(),
            'attrModel' => $attrModel,
        ]);
    }
    
    public function actionMyproduct(){
        if(Yii::$app->user->identity->type=='admin'){
            $queryProduct = Product::find();
        }else{
            $queryProduct = Product::find()->andWhere(['user_id' => \Yii::$app->user->id]);
        }
        $stringMyProductCount = $queryProduct->count();
        $modelProduct = new ActiveDataProvider(['query' => $queryProduct, 'pagination' => ['pageSize' => 12]]);
        
        return $this->render('myproduct',[
//            'modelUser' => $modelUser,
            'stringMyProductCount' => $stringMyProductCount,
            'modelProduct' => $modelProduct->getModels(),
            'pagination' => $modelProduct->pagination,
        ]);
    }
    
    public function actionSaveproductimg(){
        $ds          = DIRECTORY_SEPARATOR;
        $storeFolder = \Yii::getAlias('@webroot').'/images/product/';

        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
                mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }

            $check = getimagesize($_FILES['file']['tmp_name']);
            $img_wight = $check[0];
            $img_height = $check[1];

            if($img_wight > 1000)
            {
                $wight = 1000;
                $height =  $img_height / ($img_wight/1000);
            }
            elseif ($img_wight < 1000 && $img_height > 600) 
            {
                $height = 600;
                $wight =  $img_wight / ($img_height/600);
            }
            else
            {
                $height = $img_height;
                $wight =  $img_wight;
            }

            $tempFile = $_FILES['file']['tmp_name'];
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.jpg';

            Image::thumbnail($tempFile, $wight, $height)
            ->save($targetFile, ['quality' => 80]);
            return \Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.jpg';
        }
    }
    
    
    public function actionDeleteproduct($id=null)
    {
        $langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
        if($id != null){
            $modelProduct = Product::find()->andWhere(['id' => $id])->one();
            if(($modelProduct) && ($modelProduct->user_id == \Yii::$app->user->id || Yii::$app->user->identity->type=='admin')){ 
                ProductImage::deleteAll(['product_id' => $modelProduct->id]);
                if($modelProduct->delete()){
                    \Yii::$app->session->setFlash('product_deleted');
                    return $this->redirect($langLink.'/profile/product/myproduct');
                }else{
                    \Yii::$app->session->setFlash('product_not_deleted');
                    return $this->redirect($langLink.'/profile/product/myproduct');                    
                }
            }else{
                throw new \yii\web\NotFoundHttpException();                 
            }
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }          
    }            
    public function actionUpdateproduct($id=null)
    {
        $langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
        if($id != null){            
            $modelProduct = Product::find()->andWhere(['id' => $id])->one();

            $attrModel = ProductAttr::find()->where(['join_id' => $id])->one();
            
            if(!$attrModel){
                $attrModel = $this->createAttrModel();
            }

            $langsContent = $this->rewriteArrModels($modelProduct->translation);
            if(($modelProduct) && ($modelProduct->user_id == \Yii::$app->user->id || Yii::$app->user->identity->type=='admin'))
            {            
                    $modelProduct->scenario = 'new_product';

                    if(Yii::$app->request->post()){
                        if($modelProduct->load(\Yii::$app->request->post())){
                            $modelProduct->user_id = \Yii::$app->user->id;
                            if($modelProduct->save()){

                                if(isset($_POST['public_butt']))
                                {
                                    $modelProduct->status = 10;
                                    $modelProduct->save();
                                }
                                
                                $this->writeLangModels($langsContent, $modelProduct->id);
                                $this->writeAttrModel($attrModel, $_POST['ProductAttr'] ,$modelProduct->id);
                                ProductImage::deleteAll(['product_id' => $modelProduct->id]);
                                $arrayProductImage = json_decode($_POST['Product']['image_array']);                        
                                if($arrayProductImage){
                                    foreach($arrayProductImage as $key => $stringImageSrc){
                                        if($key == 0){
                                            $modelProduct->scenario = 'update_image';
                                            $modelProduct->img_src = $stringImageSrc;
                                            $modelProduct->save();
                                        }else{
                                            $modelNewProductImage = new ProductImage();
                                            $modelNewProductImage->scenario = 'new_product_image';
                                            $modelNewProductImage->img_src = $stringImageSrc;
                                            $modelNewProductImage->product_id = $modelProduct->id;
                                            $modelNewProductImage->save();
                                        }
                                    }
                                }
                                \Yii::$app->session->setFlash('product_update');
                                return $this->redirect($langLink.'/profile/product/myproduct');
                            }
                        }
                    }
                    
                    $arrayProductType = ArrayHelper::map(Category::find()->where(['parent_id' => '0'])->orWhere(['parent_id' => null])->all(), 'id', 'name');
                    $arrayProductImage = [];                    
                    if(($modelProduct->img_src != '') && ($modelProduct->img_src != null)){
                        $arrayProductImage[] = $modelProduct->img_src;
                    }
                    foreach($modelProduct->images as $productImage){
                        $arrayProductImage[] = $productImage['img_src'];                        
                    }
                    $jsonProductImage = json_encode($arrayProductImage);

                    
                    $states = States::find()->where(['country_id' => 107])->all();
                    $regions = States::find()->where(['country_id' => $modelProduct->country_id])->all();
                    $cities = Cities::find()->where(['state_id' => $modelProduct->region_id])->all();
                    $subCategory = Category::find()->where(['parent_id' => $modelProduct->product_type_id])->all();
                    $ArraySubCategory = ArrayHelper::map($subCategory, 'id', 'name');

                    return $this->render('updateproduct',[
                        'modelProduct' => $modelProduct,
                        'arrayProductType' => $arrayProductType,
                        'jsonProductImage' => $jsonProductImage,
                        'states' => $states,
                        'regions' => $regions,
                        'cities' => $cities,
                        'subCategory'=>$subCategory,
                        'langsContent' => $langsContent,
                        'langs' => Lang::find()->all(),
                        'ArraySubCategory'=> $ArraySubCategory,
                        'attrModel'=> $attrModel,

                    ]);
            }else{
                throw new \yii\web\NotFoundHttpException();                 
            }
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }
    }
    
    
    public function actionSaveprofilephoto()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/users_images/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            
            $check = getimagesize($_FILES['file']['tmp_name']);
            $img_wight = $check[0];
            $img_height = $check[1];

            if($img_wight > 1000)
            {
                $wight = 1000;
                $height =  $img_height / ($img_wight/1000);
            }
            elseif ($img_wight < 1000 && $img_height > 600) 
            {
                $height = 600;
                $wight =  $img_wight / ($img_height/600);
            }
            else
            {
                $height = $img_height;
                $wight =  $img_wight;
            }
            
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.png';  //5

            Image::thumbnail($tempFile, $wight, $height)
            ->save($targetFile, ['quality' => 80]);
            return 'images/users_images/'.\Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.png'; //5
        }
    }

    public function actionGetRegions()
    {
        if (Yii::$app->request->isAjax) 
        {
            $data = Yii::$app->request->post();
            $country_id = $data['country_id'];
            $arrStates = States::find()->where(['country_id' => $country_id])->all();
            $jsonData = JSON::encode($arrStates);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return [
                'search' => $jsonData,
            ];
        }
    }
    public function actionGetTowns()
    {
        if (Yii::$app->request->isAjax) 
        {
            $data = Yii::$app->request->post();
            $region_id = $data['region_id'];
            $arrTowns = Cities::find()->where(['state_id' => $region_id])->all();
            $jsonData = JSON::encode($arrTowns);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return [
                'search' => $jsonData,
            ];
        }
    }

    public function actionGetSubCategoty()
    {
        if (Yii::$app->request->isAjax) 
        {
            $data = Yii::$app->request->post();
            $parent_id = $data['product_type_id'];
            $arrSubCat = Category::find()->where(['parent_id' => $parent_id])->all();
            $langs = Lang::find()->all();
            $subCats = [];
            foreach ($arrSubCat as $key_cat=>$subCat) {
                foreach ($langs as $key => $lang) {
                    if($lang->local==$data['lang']){
                        $subCats[$key_cat]['name'] = $subCat->translation[$key]->name;
                        $subCats[$key_cat]['id'] = $subCat->id;
                    }
                }
                
            }

            $jsonData = JSON::encode($subCats);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return [
                'search' => $jsonData,
            ];
        }
    }

    public function actionProdpubl()
    {
        $langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
        $modelProduct = Product::find()->andWhere(['id' => $_GET['id']])->one();
        $modelProduct->scenario = 'new_product';
        $modelProduct->status = 10;
        $modelProduct->save();
        \Yii::$app->session->setFlash('product_update');
        return $this->redirect($langLink.'/profile/product/myproduct');
    }

    public function rewriteArrModels($models)
    {
        $arrModels = [];
        $langs = Lang::find()->all();
        foreach ($langs as $key =>$lang) {
            $arrModels[$lang->url] = $models[$key];
        }
        return $arrModels;
    }

    public function createAttrModel()
    {
        return new ProductAttr();
    }

    public function writeAttrModel($attrModel, $attrData, $join_id)
    {
        $attrModel->join_id = $join_id;
        foreach ($attrData as $attrName => $attrVal) {
            $attrModel->$attrName = $attrVal;
        }
        $attrModel->save();
    }

    public function createLangModels()
    {
        $arrModels = [];
        $langs = Lang::find()->all();
        foreach ($langs as $lang) {
            $arrModels[$lang->url] = new ProductContent();
        }
        return $arrModels;
    }

    public function writeLangModels($langsContent, $join_id)
    {
        $langs = Lang::find()->all();
        foreach ($langs as $lang) {
            $langsContent[$lang->url]->join_id = $join_id;
            $langsContent[$lang->url]->lang = $lang->url;
            foreach ($_POST[$lang->url] as $post_key => $post_value) {
                $langsContent[$lang->url]->$post_key = $post_value;
            }
            $langsContent[$lang->url]->save();
        }
    }

}
