<?php

namespace app\modules\profile\controllers;
use yii;
use yii\web\Controller;
use app\models\User;
use app\models\UserInfo;
use app\models\UserPakets;
use app\models\Product;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
/**
 * Default controller for the `profile` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;

        if(!\Yii::$app->user->isGuest){
            if(Yii::$app->user->identity->status !== '0'){
                return parent::beforeAction($action);
            }else{
                return $this->redirect('site/login');
            }         
        }else{
            if(\Yii::$app->user->isGuest){
                return parent::beforeAction($action);            
               //return $this->redirect('administration'); 
            }else{
                throw new \yii\web\NotFoundHttpException();                
            }
        }
    }
    
    
    public function actionIndex()
    {
        $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
        $enablePakets = UserPakets::find()->where(['user_id' => \Yii::$app->user->id, 'status'=>'10'])->one();
        $productCount = Product::find()->andWhere(['user_id' => \Yii::$app->user->id])->count();

        //var_dump($productCount);exit;
        return $this->render('index',[
            'modelUser' => $modelUser,
            'enablePakets'=> $enablePakets,
            'productCount'=> $productCount,
        ]);
    }
    
    public function actionUpdate()
    {
        $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
        $modelUser->scenario = 'update';
        $modelUserInfo = UserInfo::find()->where(['user_id' => $modelUser->id])->one();
        if($modelUserInfo == null){
           $modelUserInfo = new UserInfo(); 
            $modelUserInfo->user_id = \Yii::$app->user->id;
        }
        $modelUserInfo->scenario = 'add';
        
        if(\Yii::$app->request->post()){
            if($modelUser->load(\Yii::$app->request->post())){
                if($modelUser->save()){
                    if($modelUserInfo->load(\Yii::$app->request->post())){
                        if($modelUserInfo->save()){
                            \Yii::$app->session->setFlash('data_saved');
                        }else{
                            \Yii::$app->session->setFlash('data_not_saved');                            
                        }
                    }
                    $modelUser = User::find()->with('userinfo')->where(['id' => \Yii::$app->user->id])->one();
                }else{
                    \Yii::$app->session->setFlash('data_not_saved');
                }
            }
        }
        
        $modelUserInfo = UserInfo::find()->where(['user_id' => $modelUser->id])->one();        
        return $this->render('update',[
            'modelUser' => $modelUser,
        ]);
        
    }
    
    public function actionSaveprofilephoto()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/users_images/';   //2
        
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }

            $check = getimagesize($_FILES['file']['tmp_name']);
            $img_wight = $check[0];
            $img_height = $check[1];

            if($img_wight > 1000)
            {
                $wight = 1000;
                $height =  $img_height / ($img_wight/1000);
            }
            elseif ($img_wight < 1000 && $img_height > 600) 
            {
                $height = 600;
                $wight =  $img_wight / ($img_height/600);
            }
            else
            {
                $height = $img_height;
                $wight =  $img_wight;
            }
            
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.png';  //5

            Image::thumbnail($tempFile, $wight, $height)
            ->save($targetFile, ['quality' => 80]);
            return 'images/users_images/'.\Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.png'; //5
        }
    }
    
    public function actionUpdateprofilephoto()
    {
        $result = [];
        $modelUserInfo = UserInfo::find()->where(['user_id' => \Yii::$app->user->id])->one();
        $modelUserInfo->scenario = 'avatar';
        $modelUserInfo->avatar = $_POST['image_src'];
        if($modelUserInfo->save()){
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
    
}
