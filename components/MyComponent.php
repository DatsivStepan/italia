<?php
namespace app\components; 
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
 
class MyComponent extends Component
{
	public function ifSet($var)
	{
		if(isset($var)){
			return $var;
		}else{
			return '';
		}
	} 
	public static function echoYesNo($val){
		if($val){
			return 'есть';
		}else{
			return 'нет';
		}
	}
	public static function echoEnergoClass($val){
		$arrEnergo = [0 => 'A++', 1 => 'A+', 2=>'A', 3=>'B', 4=>'C', 5=>'D', 6=>'E', 7=>'F', 8=>'G'];
		foreach ($arrEnergo as $key => $value) {
			if($key == $val){
				return $value;
				break;
			}
		}
	}
}