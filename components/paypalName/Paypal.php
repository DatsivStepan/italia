<?php
/**
 * File Paypal.php.
 *
 * @author Marcio Camello <marciocamello@outlook.com>
 * @see https://github.com/paypal/rest-api-sdk-php/blob/master/sample/
 * @see https://developer.paypal.com/webapps/developer/applications/accounts
 */

namespace app\components\paypalName;

define('PP_CONFIG_PATH', __DIR__);

use Yii;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\base\Component;
use PayPal\Api\Address;
use PayPal\Api\CreditCard;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Api\FundingInstrument;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Rest\ApiContext;
use PayPal\Exception\PaypalConnectionException;

class Paypal extends Component
{
    //region Mode (production/development)
    const MODE_SANDBOX = 'sandbox';
    const MODE_LIVE    = 'live';
    //endregion

    //region Log levels
    /*
     * Logging level can be one of FINE, INFO, WARN or ERROR.
     * Logging is most verbose in the 'FINE' level and decreases as you proceed towards ERROR.
     */
    const LOG_LEVEL_FINE  = 'FINE';
    const LOG_LEVEL_INFO  = 'INFO';
    const LOG_LEVEL_WARN  = 'WARN';
    const LOG_LEVEL_ERROR = 'ERROR';
    //endregion

    //region API settings
    public $clientId;
    public $clientSecret;
    public $isProduction = false;
    public $currency = 'USD';
    public $config = [];

    /** @var ApiContext */
    private $_apiContext = null;

    /**
     * @setConfig 
     * _apiContext in init() method
     */
    public function init()
    {
        $this->setConfig();
    }

    /**
     * @inheritdoc
     */
    private function setConfig()
    {
        // ### Api context
        // Use an ApiContext object to authenticate
        // API calls. The clientId and clientSecret for the
        // OAuthTokenCredential class can be retrieved from
        // developer.paypal.com

        $this->_apiContext = new ApiContext(
            new OAuthTokenCredential(
                $this->clientId,
                $this->clientSecret
            )
        );

        // #### SDK configuration

        // Comment this line out and uncomment the PP_CONFIG_PATH
        // 'define' block if you want to use static file
        // based configuration
        $this->_apiContext->setConfig(ArrayHelper::merge(
            [
                'mode'                      => self::MODE_SANDBOX, // development (sandbox) or production (live) mode
                'http.ConnectionTimeOut'    => 30,
                'http.Retry'                => 1,
                'log.LogEnabled'            => YII_DEBUG ? 1 : 0,
                'log.FileName'              => Yii::getAlias('@runtime/logs/paypal.log'),
                'log.LogLevel'              => self::LOG_LEVEL_FINE,
                'validation.level'          => 'log',
                'cache.enabled'             => 'true'
            ],$this->config)
        );

        // Set file name of the log if present
        if (isset($this->config['log.FileName'])
            && isset($this->config['log.LogEnabled'])
            && ((bool)$this->config['log.LogEnabled'] == true)
        ) {
            $logFileName = \Yii::getAlias($this->config['log.FileName']);

            if ($logFileName) {
                if (!file_exists($logFileName)) {
                    if (!touch($logFileName)) {
                        throw new ErrorException('Can\'t create paypal.log file at: ' . $logFileName);
                    }
                }
            }

            $this->config['log.FileName'] = $logFileName;
        }

        return $this->_apiContext;
    }
    public function payAccept( $paymentId, $PayerID ){
        
        $payment = new Payment();
        $payment->setId($paymentId);
        $execution = new PaymentExecution();
        $execution->setPayerId($PayerID);

        return $payment->execute($execution, $this->setConfig());
    }

    public function payServ($sPrice, $sName, $payer_id, $sCurrency='EUR' )
    {
        $servicePrice = $sPrice;  
        $formattedServicePrice = number_format($servicePrice,2);

        $serviceName = $sName;

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");
        $payer->setPayerInfo([
          "payer_id" => $payer_id
        ]);
        /*$payer->setPayerInfo([
          "email" => "bbuyer@example.com",
          "first_name" => "Betsy",
          "last_name"=> "Buyer",
          "billing_address"=>[
            "line1"=>"52 N Main St",
            "city"=>"Johnstown",
            "country_code"=>"US",
            "postal_code"=>"43210",
            "state"=>"OH",
            "phone"=>"408-334-8890"
          ]
        ]);*/        
        

        $item = new Item();
        $item->setName($serviceName)
            ->setCurrency($sCurrency)
            ->setQuantity(1)
            ->setPrice($formattedServicePrice);

        $itemList = new ItemList();
        $itemList->setItems(array($item));

        $amount = new Amount();
        $amount->setCurrency($sCurrency);
        $amount->setTotal(number_format(($formattedServicePrice),2));

        $transaction = new Transaction();
        $transaction->setDescription($serviceName);
        $transaction->setItemList($itemList);
        $transaction->setAmount($amount);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl("http://optima-domus.roketdev.pro/pay/success");
        $redirectUrls->setCancelUrl("http://optima-domus.roketdev.pro");

        $payment = new Payment();
        $payment->setIntent("sale");
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));

        try {
            $thisPay = $payment->create($this->_apiContext);
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            var_dump($ex->getCode());
            var_dump($ex->getData());
            exit;
        } catch (Exception $ex) {
            var_dump($ex);exit;
        }
        return $thisPay;
    }

    public function payEmail($sPrice, $sName, $payer_id, $sCurrency='EUR' )
    {
        $servicePrice = $sPrice;  
        $formattedServicePrice = number_format($servicePrice,2);

        $serviceName = $sName;




        $payer = new Payer();
        $payer->setPaymentMethod("paypal");
        $payer->setPayerInfo([
          "payer_id" => $payer_id
        ]);
        /*$payer->setPayerInfo([
          "email" => "bbuyer@example.com",
          "first_name" => "Betsy",
          "last_name"=> "Buyer",
          "billing_address"=>[
            "line1"=>"52 N Main St",
            "city"=>"Johnstown",
            "country_code"=>"US",
            "postal_code"=>"43210",
            "state"=>"OH",
            "phone"=>"408-334-8890"
          ]
        ]);*/        
        

        $item = new Item();
        $item->setName($serviceName)
            ->setCurrency($sCurrency)
            ->setQuantity(1)
            ->setPrice($formattedServicePrice);

        $itemList = new ItemList();
        $itemList->setItems(array($item));

        $amount = new Amount();
        $amount->setCurrency($sCurrency);
        $amount->setTotal(number_format(($formattedServicePrice),2));

        $transaction = new Transaction();
        $transaction->setDescription($serviceName);
        $transaction->setItemList($itemList);
        $transaction->setAmount($amount);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl("http://optima-domus.roketdev.pro/pay/success-email");
        $redirectUrls->setCancelUrl("http://optima-domus.roketdev.pro");

        $payment = new Payment();
        $payment->setIntent("sale");
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));

        try {
            $thisPay = $payment->create($this->_apiContext);
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            var_dump($ex->getCode());
            var_dump($ex->getData());
            exit;
        } catch (Exception $ex) {
            var_dump($ex);exit;
        }
        return $thisPay;
    }
}
