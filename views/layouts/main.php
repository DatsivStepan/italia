<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\widgets\LangWidget;
use app\models\Lang;
use app\models\PremiumPacket;
use app\assets\AppAsset;
use app\modules\administration\models\Pricing;
use app\models\Category;
AppAsset::register($this);

$langC = Lang::find()->where(['local' => Yii::$app->language])->asArray()->one();
$thisLangth = $langC['url'];

$langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';

$arraySetting = ArrayHelper::map(app\models\Setting::find()->all(),'key','value');
$pricing = Pricing::find()->all();

$categoriesOrenda = Category::find()->where( ['parent_id'=>1] )->all();
$categoriesProdacha = Category::find()->where( ['parent_id'=>2] )->all();
$premiumPacket = PremiumPacket::find()->one();
$session = Yii::$app->session;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=500, initial-scale=0.6, maximum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div style='display:none;'>
    <?php 
    NavBar::begin([
    ]); 
    NavBar::end([
    ]); ?>
</div>

 
<div class="registration-panel">
    <div class="container">
        <div class="col-xs-12 padd-div-registration">
            <div class="col-xs-12 close-mob-registr text-center">
                <img class='close-registration' src="/img/close_img.png" />
            </div>
            <div class="col-md-11 col-sm-10 col-xs-12 col-padding-registr-text text-center">
                <p class="title-panel-click marg-titl-registr">
                    <?= Yii::t('main','registartion_title') ?>
                </p>
                <p class="send-email-log">
                    <a href="<?=$langLink?>/site/signup?owner" class="pay-button">Регистрация для владельцов</a>
                </p>
            </div>
            <div class="col-md-1 col-sm-2 close-registr">
                <img class='close-registration' src="/img/close_img.png" />
            </div>
        </div>
        <div class="col-xs-12 padding-paket-div">
            <?php $tarifNumb= 1; foreach ($pricing as $key => $price): ?>
            <div class="col-md-6 col-sm-6 col-xs-12 padd-same-packet">
                <div class="col-xs-12 text-vertical-center media-padd-zero">
                    <div class="col-lg-1 col-md-2 col-sm-2  col-xs-1 no-padding no_pad_0">
                        <img src="/img/Ellipse1.png">
                        <span class="spa_pos_ab_l"><?= $tarifNumb; ?></span>
                    </div>
                    <p class="paket_name">Пакет “<?=Html::encode($price->name) ?>”</p>
                </div>
                <div class="col-xs-12 otstup-opus-pacet">
                    
                    <div class="otstup-div-style same-text-packet">
                        <?=$price->descriptionText?>
                    </div>
                    <div class="div-otstup-price">
                        <span class="price-text-style"><?= Yii::t('main','Price') ?>:</span>
                        <span class="price-text-style"><?=Html::encode($price->price* $price->months_count) ?>€</span>
                    </div>
                    <a href="<?=$langLink?>/site/signup?paket-id=<?=Html::encode($price->id) ?>" class="pay-button"><?= Yii::t('main','Pay_now') ?></a>
                </div>
            </div>
            <?php $tarifNumb++; endforeach; ?>
            <div class="col-md-6 col-sm-6 col-xs-12 padd-same-packet">
                <div class="col-xs-12 text-vertical-center media-padd-zero">
                    <div class="col-lg-1 col-md-2 col-sm-2  col-xs-1 no-padding no_pad_0">
                        <img src="/img/Ellipse1.png">
                        <span class="spa_pos_ab_l"><?= $tarifNumb; ?></span>
                    </div>
                    <p class="paket_name">Пакет “<?=Html::encode($premiumPacket->name) ?>”</p>
                </div>
                <div class="col-xs-12 otstup-opus-pacet">
                    <div class="otstup-div-style same-text-packet">
                        <?=$premiumPacket->description?>
                    </div>
                    <div class="div-otstup-price">
                        <span class="price-text-style"><?= Yii::t('main','Price') ?>:</span>
                        <span class="price-text-style"><?=Html::encode($premiumPacket->price) ?>€</span>
                    </div>
                    <a href="<?=$langLink?>/site/signup?paket-id=0" class="pay-button"><?= Yii::t('main','Pay_now') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class='call-back-panel'>
    <div class='container'>
        <div class='col-xs-12 padd-call-back-panel'>
            <div class="col-xs-12 close-mob-registr text-center">
                <img class='close-call-back' src="/img/close_img.png" />
            </div>
            <div class="col-md-11 col-sm-10 col-xs-12 col-padding-call-back-text text-center">
                <p class="title-panel-click marg-titl-registr">
                    Благодарим за Ваш запрос!
                </p>
                <p class="send-email-log">
                    Мы свяжемся с Вами с ближайшее время.
                </p>
            </div>
            <div class="col-md-1 col-sm-2 close-registr">
                <img class='close-call-back' src="/img/close_img.png" />
            </div>
        </div>
        <form class="col-xs-12 center-block-my text-center" id="call_to_email">
            <div class="block-style-input">
                <input type="hidden" name="email_user" value="<?=$arraySetting['email']?>" >
                <input type="text" name="name" placeholder="<?= Yii::t('main','Your_name') ?>:" class="form-call-back-panel clear_input"/>
            </div>
            <div class="block-style-input">
                <input type="text" name="number" placeholder="<?= Yii::t('main','Your_phone_number') ?>:" class="form-call-back-panel clear_input"/>
            </div>
            <a class="peredzvo-button send_call_to_email"><?= Yii::t('main', 'Call_back')?></a>
        </form>
    </div>
</div>

<div class='call-back-popup' <?= !$session->has('popup') ? '' : 'nopopup="yes"'?>>
    <div class='container'>
        <div class='col-xs-12 padd-call-back-panel'>
            <div class="col-xs-12 close-mob-registr text-center">
                <img class='close-call-popup' src="/img/close_img.png" />
            </div>
            <div class="col-md-11 col-sm-10 col-xs-12 col-padding-call-back-text text-center">
                <p class="title-panel-click marg-titl-registr">
                    Есть вопросы?
                </p>
                <p class="send-email-log">
                    Наш менеджер свяжется с Вами в ближайшее время
                </p>
            </div>
            <div class="col-md-1 col-sm-2 close-registr">
                <img class='close-call-popup' src="/img/close_img.png" />
            </div>
        </div>
        <form class="col-xs-12 center-block-my text-center" id="call_to_popup">
            <div class="block-style-input">
                <input type="hidden" name="email_user" value="<?=$arraySetting['email']?>" >
                <input type="text" name="name" placeholder="<?= Yii::t('main','Your_name') ?>:" class="form-call-back-panel clear_input"/>
            </div>
            <div class="block-style-input">
                <input type="text" name="number" placeholder="<?= Yii::t('main','Your_phone_number') ?>:" class="form-call-back-panel clear_input"/>
            </div>
            <div class="block-style-input">
                <input type="text" name="email" placeholder="Ваш e-mail:" class="form-call-back-panel clear_input"/>
            </div>
            <a class="peredzvo-button send_call_to_popup">Отправить</a>
        </form>
    </div>
</div>

<div class="header">
    <div class="container">
        <div class="col-xs-12 text-center-mob no-padding">
            <div class="col-xs-12 style-padd-top padd-left-none">
                <nav class="navbar navbar-default display-none style-navbar">
                    <div class="container-fluid">
                        <div class="navbar-header ">
                            <button type="button" class="navbar-toggle collapsed" style="background-color: #ffffff; border: 0px"
                                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                                    aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                        </div>
                        <div id="navbar" class="navbar-collapse collapse menue_golovne" aria-expanded="false"
                             style="height: 1px;">
                            <ul class="nav navbar-nav menue_golovne">
                                <li>
                                    <a  class="text 
                                    <?=Html::encode(isset($this->params['menu_item']['about']) ? 
                                    $this->params['menu_item']['about'] : '')?>
                                    " href="<?=$langLink?>/site/about"><?= Yii::t('main','ABOUT_THE_PROJECT') ?></a>
                                </li>
                                <li>
                                    <a class="text 
                                    <?=Html::encode(isset($this->params['menu_item']['lease']) ? 
                                    $this->params['menu_item']['lease'] : '')?>
                                    " href="<?=$langLink?>/category/lease"><?= Yii::t('main','RENTALS') ?></a>
                                </li>
                                <li>
                                    <a class="text 
                                    <?=Html::encode(isset($this->params['menu_item']['selling']) ? 
                                    $this->params['menu_item']['selling'] : '')?>
                                    " href="<?=$langLink?>/category/selling"><?= Yii::t('main','SALE') ?></a>
                                </li>
                                <li>
                                    <a class="text 
                                    <?=Html::encode(isset($this->params['menu_item']['uslugi']) ? 
                                    $this->params['menu_item']['uslugi'] : '')?>
                                    " href="<?=$langLink?>/site/uslugi"><?= Yii::t('main','SERVICES') ?></a>
                                </li>
                                <li>
                                    <a class="text 
                                    <?=Html::encode(isset($this->params['menu_item']['investicii']) ? 
                                    $this->params['menu_item']['investicii'] : '')?>
                                    " href="<?=$langLink?>/site/investicii"><?= Yii::t('main','INVESTMENTS_IN_REAL_ESTATE') ?></a>
                                </li>
                                <li>
                                    <a class="text 
                                    <?=Html::encode(isset($this->params['menu_item']['owners']) ? 
                                    $this->params['menu_item']['owners'] : '')?>
                                    " href="<?=$langLink?>/site/for-owners"><?= Yii::t('main','FOR_OWNERS') ?></a>
                                </li>
                                <li>
                                    <a class="text 
                                    <?=Html::encode(isset($this->params['menu_item']['contact']) ? 
                                    $this->params['menu_item']['contact'] : '')?>
                                    " href="<?=$langLink?>/site/contact"><?= Yii::t('main','CONTACTS') ?></a>
                                </li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div><!--/.container-fluid -->
                </nav>
                <div class="col-lg-2 col-sm-12 text-center logo padd-left-none">
                    <a href="<?=$langLink?>/"><img src="/img/logo.png"/></a>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-12 no-padding bottom-marg">
                    <div class="text-right-style">
                        <img src="/img/navigation.png"/>
                        <span class="text-header-size color-loc-first"><?=$arraySetting['adress']?></span>
                    </div>
                    <div class="text-right-style marg-top">
                        <img src="/img/email.png"/>
                        <span class="text-header-size color-loc-first"><?=$arraySetting['email']?></span>
                    </div>
                </div>
                <div class="col-lg-7 col-md-8 col-xs-12 padd-style">
                    <div class="no-margin display-style text-vertical-center">
                        <div class="display-style call-back-div">
                            <img src="/img/call_back.png" />
                            <span class="text-size-call color-call-back click-call-back"><?= Yii::t('main','Back_call') ?></span>
                        </div>
                        <?php if(\Yii::$app->user->isGuest): ?>
                        <div class="display-style registaration-block">
                            <a class="myButton-style text-header-size click-registration"><?= Yii::t('main','CHECK_IN') ?></a>
                            <a href="<?=$langLink?>/site/login" class="myButton-style text-header-size">Вход</a>
                        </div>
                        <?php else: ?>
                        <div class="display-style registaration-block">
                            <a href="<?=$langLink?>/profile" class="myButton-style text-header-size">Профиль</a>
                            <a href="<?=$langLink?>/site/logout" class="myButton-style text-header-size">Выход</a>
                        </div>
                        <?php endif; ?>
                        <div class="display-style">
                            <div>
                                <img src="/img/telephone.png"/>
                                <span class="text-size-call text-number-style"><?=$arraySetting['phone']?></span>
                            </div>
                            <div>
                                <img src="/img/telephone.png"/>
                                <span class="text-size-call text-number-style"><?=$arraySetting['phone2']?></span>
                            </div>
                        </div>
                        <div class="display-style text-size-flag">
                            
                            <?= LangWidget::widget() ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 no-padding marg-top-menu">
                <div class="col-md-11 col-sm-10 ul-text-style">
                    <ul class="menu-ul">
                        <li>
                            <a  class="
                            <?=Html::encode(isset($this->params['menu_item']['about']) ? 
                            $this->params['menu_item']['about'] : '')?>
                            " href="<?=$langLink?>/site/about"><?= Yii::t('main','ABOUT_THE_PROJECT') ?></a>
                        </li>
                        <li>
                            <a class="
                            <?=Html::encode(isset($this->params['menu_item']['lease']) ? 
                            $this->params['menu_item']['lease'] : '')?>
                            " href="<?=$langLink?>/category/lease"><?= Yii::t('main','RENTALS') ?></a>
                        </li>
                        <li>
                            <a class="
                            <?=Html::encode(isset($this->params['menu_item']['selling']) ? 
                            $this->params['menu_item']['selling'] : '')?>
                            " href="<?=$langLink?>/category/selling"><?= Yii::t('main','SALE') ?></a>
                        </li>
                        <li>
                            <a class="
                            <?=Html::encode(isset($this->params['menu_item']['uslugi']) ? 
                            $this->params['menu_item']['uslugi'] : '')?>
                            " href="<?=$langLink?>/site/uslugi"><?= Yii::t('main','SERVICES') ?></a>
                        </li>
                        <li>
                            <a class="
                            <?=Html::encode(isset($this->params['menu_item']['investicii']) ? 
                            $this->params['menu_item']['investicii'] : '')?>
                            " href="<?=$langLink?>/site/investicii"><?= Yii::t('main','INVESTMENTS_IN_REAL_ESTATE') ?></a>
                        </li>
                        <li>
                            <a class="
                            <?=Html::encode(isset($this->params['menu_item']['owners']) ? 
                            $this->params['menu_item']['owners'] : '')?>
                            " href="<?=$langLink?>/site/for-owners"><?= Yii::t('main','FOR_OWNERS') ?></a>
                        </li>
                        <li>
                            <a class="
                            <?=Html::encode(isset($this->params['menu_item']['contact']) ? 
                            $this->params['menu_item']['contact'] : '')?>
                            " href="<?=$langLink?>/site/contact"><?= Yii::t('main','CONTACTS') ?></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-1 col-sm-2 no-padding">
                    <div class="image-style">
                        <a href="<?=$arraySetting['fb_link']?>"><img src="/img/facebook.png"/></a>
                    </div>
                    <div class="image-style">
                        <a href="<?=$arraySetting['inst_link']?>"><img src="/img/instagram.png"/></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content">
        <?= Breadcrumbs::widget([
            'homeLink' => ['label' => Yii::t('main','home'), 'url' => $langLink.'/'],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
</div>

<div class="footer">
    <div class="container text-center-mob">
        <div class="col-xs-12" style="padding-top: 28px; padding-bottom: 6px; border-bottom: 1px solid #585859">
            <form class="col-lg-3 col-md-3 col-sm-6 no-padding" id="form_subscribe">
                <span class="text-span-footer-style"><?= Yii::t('main', 'SUBSCRIBE_TO') ?> :</span>
                <input type="hidden" name="email_user" value="<?=$arraySetting['email']?>" >
                <input type="email" name="email" class="input-style-footer" placeholder="<?= Yii::t('main', 'Your_Email')?>">
                <a class="btn Button-submit-style subscribe "><?= Yii::t('main','SUBSCRIBE') ?></a>
            </form>
            <div class="col-sm-6 infomation-none">
                <span class="text-span-footer-style"><?= Yii::t('main','CONTACTS')?> </span>
                <div class="all-information">
                    <img src="/img/telephone_footer.png"/>
                    <span class="text-number"><?=$arraySetting['phone']?></span>
                </div>
                <div class="all-information image-padding-footer">
                    <img src="/img/navigation_footer.png" style="float: left; padding-top: 5px;"/>
                    <div class="inline">
                        <span class="footer-coordinate"><?=$arraySetting['adress']?></span>
                    </div>
                </div>
                <div class="all-information image-padding-footer">
                    <img src="/img/email_footer.png"/>
                    <span class="footer-coordinate"><?=$arraySetting['email']?></span>
                </div>
                <div class="all-information click-registration-bl">
                    <a class="registration-button click-registration"><?= Yii::t('main','CHECK_IN')?></a>
                </div>
                <div class="all-information image-padding-footer call-back-div">
                    <img src="/img/call_back_footer.png"/>
                    <span class="call-back-footer click-call-back">Обратный звонок</span>
                </div>
                <div class="all-information marg-top-style foot-social-butt">
                    <img src="/img/facebook_footer.png"/>
                    <img src="/img/instagram_footer.png"/>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 no-padding media-padd-top">
                <div class="col-sm-4 correct-padd-media">
                    <span class="text-span-footer-style block"><?= Yii::t('main','THE_MAIN_THING') ?></span>
                    <span class="text-span-footer-style click " data-togg="1"><?= Yii::t('main','THE_MAIN_THING') ?></span>
                    <ul class="all-ul" id="show-1">
                        <li><a href="<?=$langLink?>/site/about"><?= Yii::t('main','about_the_project') ?></a><a href=""></li>
                        <li><a href="<?=$langLink?>/category/lease"><?= Yii::t('main','rentals') ?></a></li>
                        <li><a href="<?=$langLink?>/category/selling"><?= Yii::t('main','sale') ?></a></li>
                        <li><a href="<?=$langLink?>/site/uslugi"><?= Yii::t('main','services') ?></a></li>
                        <li><a href="<?=$langLink?>/site/investicii"><?= Yii::t('main','investments_in_real_estate') ?></a></li>
                        <li><a href="<?=$langLink?>/site/partners"><?= Yii::t('main','partners') ?></a></li>
                        <li><a href="<?=$langLink?>/site/reviews"><?= Yii::t('main','reviews') ?></a></li>
                        <li><a href="<?=$langLink?>/site/contact"><?= Yii::t('main','contacts') ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-5 correct-padd-media no-padding">
                    <span class="text-span-footer-style block"><?= Yii::t('main','sale') ?></span>
                    <span class="text-span-footer-style click" data-togg="2"><?= Yii::t('main','sale') ?></span>
                    <ul class="all-ul" id="show-2">
                        <?php foreach ($categoriesOrenda as $category): ?>
                        <li>
                            <a href="<?=$langLink?>/category/<?= 'lease/'.$category->id ?>">
                                <?= Yii::$app->mycomponent->ifSet($category->name) ?>
                            </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-sm-3 correct-padd-media padd-zero">
                    <span class="text-span-footer-style block"><?= Yii::t('main','rentals') ?></span>
                    <span class="text-span-footer-style click" data-togg="3"><?= Yii::t('main','rentals') ?></span>
                    <ul class="all-ul" id="show-3">
                        <?php foreach ($categoriesProdacha as $category): ?>
                        <li>
                            <a href="<?=$langLink?>/category/<?= 'selling/'.$category->id ?>">
                                <?= Yii::$app->mycomponent->ifSet($category->name) ?>
                            </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 information-block">
                <span class="text-span-footer-style"><?= Yii::t('main','CONTACTS') ?></span>
                <div class="all-information">
                    <img src="/img/telephone_footer.png"/>
                    <span class="text-number"><?=$arraySetting['phone']?></span>
                </div>
                <div class="all-information image-padding-footer for-image-corr">
                    <img src="/img/navigation_footer.png"/>
                    <div class="inline">
                        <span class="footer-coordinate"><?=$arraySetting['adress']?></span>
                    </div>
                </div>
                <div class="all-information image-padding-footer">
                    <img src="/img/email_footer.png"/>
                    <span class="footer-coordinate"><?=$arraySetting['email']?></span>
                </div>
                <div class="all-information">
                    <a class="registration-button click-registration"><?= Yii::t('main','CHECK_IN') ?></a>
                </div>
                <div class="all-information image-padding-footer call-back-div">
                    <img src="/img/call_back_footer.png"/>
                    <span class="call-back-footer click-call-back"><?= Yii::t('main','Back_call') ?></span>
                </div>
                <div class="all-information marg-top-style foot-social-butt">
                    <a href="<?=$arraySetting['fb_link']?>">
                        <img src="/img/facebook_footer.png"/>
                    </a>                    
                    <a href="<?=$arraySetting['inst_link']?>">
                        <img src="/img/instagram_footer.png"/>
                    </a>                    
                </div>
            </div>
        </div>
        <div class="col-xs-12 text-vertical-center padd-top">
            <div class="col-xs-12 col-sm-3 col-md-2 footer-logo">
                <img src="/img/logo_footer.png">
            </div>
            <div class="col-xs-12 col-sm-9 col-md-10 infa-span-domus">
                <span>© 2017. Domus Optima. <?= Yii::t('main','All_rights_reserved') ?></span>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
