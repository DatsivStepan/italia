<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use app\components\MyComponent;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    use app\models\Lang;
    $langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
    $this->title = 'Мои объявления';
?>
<div class='call-back-panel call-to-sell'>
    <div class='container'>
        <div class='col-xs-12 padd-call-back-panel'>
            <div class="col-xs-12 close-mob-registr text-center">
                <img class='close-call-back' src="/img/close_img.png" />
            </div>
            <div class="col-md-11 col-sm-10 col-xs-12 col-padding-call-back-text text-center">
                <p class="title-panel-click marg-titl-registr">
                    Связаться с нами
                </p>
                <p class="send-email-log">
                    
                </p>
            </div>
            <div class="col-md-1 col-sm-2 close-registr">
                <img class='close-call-back' src="/img/close_img.png" />
            </div>
        </div>
        <form class="col-xs-12 center-block-my text-center" id="call_to_selling" action="/">
            <div class="block-style-input">
                <input type="hidden" name="email_user" value="<?=$modelUserAutor->email?>" >
                <input type="hidden" name="prod_name" value="<?=$modelProduct->name?>" >
                <input type="text" name="name" placeholder="<?= Yii::t('main','Your_name') ?>:" class="form-call-back-panel clear_input" required="true"/>
            </div>
            <div class="block-style-input">
                <input type="text" name="number" placeholder="<?= Yii::t('main','Your_phone_number') ?>:" class="form-call-back-panel clear_input" required="true"/>
            </div>
            <div class="block-style-input">
                <input type="email" name="email" placeholder="Ваш E-Mail:" class="form-call-back-panel clear_input" required="true" />
            </div>
            <div class="block-style-input">
                <textarea name="content" class="form-call-back-panel clear_input" required="true" placeholder="Ваш вопрос..." rows="5" style="height: auto;"></textarea>
            </div>
            <button type="submit" class="peredzvo-button send_call_to_selling">fgfhg</button>

        </form>
    </div>
</div>
<div class="content">
    <div class="kartochka">
        <div class="container">
            <div class="col-xs-12 padd-zero style-padding-ul">
                <ul class="breadcrumb my-breadcrumb-style">
                    <li>
                        <a href="../" class="style-a-color"><?= Yii::t('main','home') ?></a>
                    </li>
                    <li>
                        <a href="<?=$langLink?>/category/<?=$modelProduct->product_type_id == 1 ? 'lease' : 'selling'?>" class="style-a-color"><?= $modelProduct->category->name; ?></a>
                    </li>
                    <li>
                        <a class="style-a-color active-color"><?= $modelProduct->name; ?></a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 padd-zero title-border">
                <div class="col-md-6 padd-zero kartochka-title-name">
                    <p class="text-title-same"><?= $modelProduct->name; ?></p>
                </div>
                <div class="col-md-6 padd-zero kartochka-title-price">
                    <span class="name-price"><?= Yii::t('main','Price') ?>:</span>
                    <?php 
                        $currency = '';
                        switch ($modelProduct->currency) {
                            case 0:
                                $currency =  "€";
                                break;
                            case 1:
                                $currency =  "$";
                                break;
                            case 2:
                                $currency =  "руб";
                                break;
                        }
                    ?>
                    <span class="number-price"><?= $currency; ?> <?= $modelProduct->price; ?></span>
                </div>
            </div>
            <div class="col-xs-12 my-padding-kartochka">
                <div class="col-md-6 col-xs-12 padd-left-none">
                   <div class="col-xs-12 padd-zero">
                       <div id="slider" class="flexslider">
                           <ul class="slides">
                               <?php if(($modelProduct->img_src != '') || ($modelProduct->img_src != null)){ ?>
                                   <li>
                                       <a href="<?= Url::home().'images/product/'.$modelProduct->img_src; ?>" class="fresco" data-fresco-group="example">
                                           <div  style="background: url('<?= Url::home().'images/product/'.$modelProduct->img_src; ?>')" class="item-popup-link" data-src="<?= Url::home().'$images/product/'.$modelProduct->img_src; ?>"></div>
                                       </a>
                                   </li>
                               <?php }else{ ?>
                                   <li>
                                       <a href="<?= Url::home(); ?>img/DefaultFotoProduct.png" class="fresco" data-fresco-group="example">
                                           <div style="background: url('<?= Url::home(); ?>img/DefaultFotoProduct.png')" class="item-popup-link" data-src="<?= Url::home(); ?>img/DefaultFotoProduct.png"></div>
                                       </a>
                                   </li>
                               <?php } ?>
                               <?php $other_images = $modelProduct->images?>
                               <?php if($other_images){ ?>
                               <?php foreach ($other_images as $images){ ?>
                                   <li>
                                       <a href="<?= Url::home().'images/product/'.$images->img_src; ?>" class="fresco" data-fresco-group="example">
                                           <div  style="background: url('<?= Url::home().'images/product/'.$images->img_src; ?>')" class="item-popup-link" data-src="<?= Url::home().'$images/product/'.$images->img_src; ?>"></div>
                                       </a>
                                   </li>
                               <?php }?>
                               <?php } ?>
                           </ul>
                       </div>
                       <div id="carousel" class="flexslider">
                           <ul class="slides" >
                               <?php if(($modelProduct->img_src != '') || ($modelProduct->img_src != null)){ ?>
                               <li class="style-img-select-my">
                                   <a href="<?= Url::home().'images/product/'.$modelProduct->img_src; ?>"  data-fresco-group="example">
                                       <div  style="background: url('<?= Url::home().'images/product/'.$modelProduct->img_src; ?>')" class="c_item img-class-watch" data-src="<?= Url::home().'$images/product/'.$modelProduct->img_src; ?>"></div>
                                           <div class="watch-icon">
                                               <img src="/img/watch-icon.png" />
                                           </div>
                                   </a>
                               </li>
                               <?php }else{ ?>
                               <li>
                                   <a href="<?= Url::home(); ?>img/DefaultFotoProduct.png"  data-fresco-group="example">
                                       <div style="background: url('<?= Url::home(); ?>img/DefaultFotoProduct.png')" class="c_item" data-src="<?= Url::home(); ?>img/DefaultFotoProduct.png"></div>
                                   </a>
                               </li>
                               <?php } ?>
                               <?php if($other_images){ ?>
                               <?php foreach ($other_images as $images){ ?>
                                   <li class="style-img-select-my">
                                       <a href="<?= Url::home().'images/product/'.$images->img_src; ?>"  data-fresco-group="example">
                                           <div  style="background: url('<?= Url::home().'images/product/'.$images->img_src; ?>')" class="c_item img-class-watch" data-src="<?= Url::home().'$images/product/'.$images->img_src; ?>"></div>
                                           <div class="watch-icon">
                                               <img src="/img/watch-icon.png" />
                                           </div>
                                       </a>
                                   </li>
                               <?php }?>
                               <?php }?>
                           </ul>
                       </div>
                  </div>
                    <div class='col-xs-12 my-padding-kartochka'>
                        <div class="col-xs-12 padd-zero same-border-kartochka">
                            <p class='text-title-same'><?= Yii::t('main','Description_of_the_object') ?>:</p>
                        </div>
                        <div class='col-xs-12 padd-object-inform same-span-inform'>
                            <span class='inform-content-object'>
                                <?= $modelProduct->about; ?>
                            </span>
                        </div>
                    </div>
                    <div class="col-xs-12 my-padding-kartochka">
                        <div class='col-xs-12 padd-zero'>
                            <p class='text-title-same'><?= Yii::t('main','Location_of_the_object_on_the_map') ?></p>
                            <div class="col-xs-12 padd-zero img-map-senter">
                                <!--<img src='/img/map-icon.png' />-->
                                <div id="map_container">
                                    <div id="map" style="height:300px;"></div>
                                    <input type="hidden" value="<?= $modelProduct['address']?>" required="required" name="search_address" class="form-control kart_city" id="searchTextField">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12 inform-pos padd-min-size">
                    <div class='col-xs-12 inform-pos inform-center-text'>
                        <p class='information-title'><?= Yii::t('main','The_basic_info') ?>:</p>
                        <a class='call-seller click-call-back-2'>Связаться с нами</a>
                    </div>
                    <div class="col-xs-12 inform-pos my-padding-kartochka">
                        <div class='col-xs-1 padd-zero'>
                            <img src="/img/company-inf-icon.png"/>
                        </div>
                        <div class='col-xs-11 padd-zero'>
                            <div class="col-xs-12 same-border-kartochka padd-zero">
                                <p class='find-pos'>Сведения:</p>
                            </div>
                            <div class='col-xs-12 information-otstup-style'>
                                <div class="col-xs-6">
                                    <p class='find-pos'><?= Yii::t('main','Company') ?>:</p>
                                    <span class='inf-pos-sapan'><?= $modelUserAutor->userinfo->company_name; ?></span>
                                </div>
                                <div class="col-xs-6">
                                    <?php $avatar = '/images/default_avatar.jpg'; ?>
                                    <?php if($modelUserAutor->userinfo->avatar != ''){ ?>
                                        <?php $avatar = '/'.$modelUserAutor->userinfo->avatar; ?>
                                    <?php } ?>
                                    <a href="<?=$langLink.'/agency/'.$modelUserAutor->id?>">
                                      <img src='<?= $avatar; ?>' style="width:100%" />
                                    </a>
                                </div>
                            </div>
                            <div class='col-xs-12 information-otstup-style'>
                                <div class="col-xs-6">
                                    <p class='find-pos'>E-Mail:</p>
                                    <span class='inf-pos-sapan'><?= $modelUserAutor->email; ?></span>
                                </div>
                                <div class="col-xs-6">
                                    <p class='find-pos'>Телефон:</p>
                                    <span class='inf-pos-sapan'><?= $modelUserAutor->userinfo->phone_number; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 inform-pos my-padding-kartochka">
                        <div class='col-xs-1 padd-zero'>
                            <img src="/img/light-icon.png"/>
                        </div>
                        <div class='col-xs-11  padd-zero'>
                            <div class="col-xs-12 same-border-kartochka padd-zero">
                                <p class='find-pos'><?= Yii::t('main','Features_of_the_object') ?>:</p>
                            </div>
                            <div class='col-xs-12 inform-ul-same'>
                                <div class='col-xs-6'>
                                    <ul class="left-object-ul">
                                        <li>
                                            <a>Жилая площадь</a>
                                        </li>
                                        <li>
                                            <a>Площадь земли</a>
                                        </li>
                                        <li>
                                            <a>Площадь двора</a>
                                        </li>
                                        <li>
                                            <a><?= Yii::t('main','Year_of_construction') ?></a>
                                        </li>
                                        <li>
                                            <a>Год реставрации</a>
                                        </li>
                                        <li>
                                            <a><?= Yii::t('main','Number_of_rooms') ?></a>
                                        </li>
                                        <li>
                                            <a><?= Yii::t('main','Number_of_bathrooms') ?></a>
                                        </li>
                                        <li>
                                            <a><?= Yii::t('main','garage') ?></a>
                                        </li>
                                        <li>
                                            <a><?= Yii::t('main','Parking_place') ?></a>
                                        </li>
                                        <li>
                                            <a><?= Yii::t('main','Terrace_balcony') ?></a>
                                        </li>
                                        <li>
                                            <a><?= Yii::t('main','Private_courtyard') ?></a>
                                        </li>
                                        <li>
                                            <a><?= Yii::t('main','pool') ?></a>
                                        </li>
                                        <li>
                                            <a><?= Yii::t('main','furniture') ?></a>
                                        </li>
                                        <li>
                                            <a><?= Yii::t('main','elevator') ?></a>
                                        </li>
                                        <li>
                                            <a><?= Yii::t('main','Energy_class_of_the_object') ?></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-6">
                                    <ul class="right-object-ul">
                                        <li>
                                            <a><?=$modelProduct->object_area ? $modelProduct->object_area : '-'?></a>
                                        </li>
                                        <li>
                                            <a><?=$attrModel->total_area ? $attrModel->total_area : '-'?></a>
                                        </li>
                                        <li>
                                            <a><?=$attrModel->area_yard ? $attrModel->area_yard : '-'?></a>
                                        </li>
                                        <li>
                                            <a><?=$modelProduct->year_of_construction ? $modelProduct->year_of_construction : '-'?></a>
                                        </li>
                                        <li>
                                            <a><?=$attrModel->year_restavration ? $attrModel->year_restavration : '-'?></a>
                                        </li>
                                        <li>
                                            <a><?=$attrModel->room?></a>
                                        </li>
                                        <li>
                                            <a><?=$attrModel->bathroom?></a>
                                        </li>
                                        <li>
                                            <a><?=$attrModel->garage?></a>
                                        </li>
                                        <li>
                                            <a><?=MyComponent::echoYesNo($attrModel->parking);?></a>
                                        </li>
                                        <li>
                                            <a><?=MyComponent::echoYesNo($attrModel->balcony);?></a>
                                        </li>
                                        <li>
                                            <a><?=MyComponent::echoYesNo($attrModel->yard);?></a>
                                        </li>
                                        <li>
                                            <a><?=MyComponent::echoYesNo($attrModel->pool);?></a>
                                        </li>
                                        <li>
                                            <a><?=MyComponent::echoYesNo($attrModel->furniture);?></a>
                                        </li>
                                        <li>
                                            <a><?=MyComponent::echoYesNo($attrModel->elevator);?></a>
                                        </li>
                                        <li>
                                            <a><?=MyComponent::echoEnergoClass($attrModel->energo_class);?></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-xs-12 question-pos marg-pos">
                        <div class="col-xs-12 question-pos">
                            <p class='text-title-same'>Задать вопрос нам:</p>
                        </div>
                        <div class="col-xs-12 question-pos">
                            <form class="col-xs-12 style-question-padd question-seller" id="ask_guestion"  >
                                <input type="hidden" name="email_user" value="<?=$arraySetting['email'] ?>" >
                                <input type="text" name="name" placeholder="<?= Yii::t('main','Your_name') ?>:" class="form-style-kartochka"/>
                                <input type="text" name="email" placeholder="<?= Yii::t('main','Your_email_mail') ?>:" class="form-style-kartochka"/>
                                <textarea name="text"  placeholder="<?= Yii::t('main','Your_message') ?>:" class="form-style-kartochka write-text-form"></textarea>
                                <a class="send-question"><?= Yii::t('main','Send') ?></a>
                            </form>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <?= app\widgets\InterestedProductWidget::widget(['id' => $modelProduct->id ]); ?>
</div>