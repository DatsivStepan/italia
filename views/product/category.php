<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use app\models\Lang;
use app\widgets\SingleProduct;
$langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
$this->title = $title;
?>
<div class="content">
    <div class="arenda-head">
        <div class="container">
            <div class="col-xs-12 padd-zero style-padding-ul">
                <ul class="breadcrumb my-breadcrumb-style">
                    <li>
                        <a class="style-a-color" href="../"><?= Yii::t('main','home') ?></a>
                    </li>
                    <li>
                        <a class="style-a-color active-color"><?=Html::encode($title)?></a>
                    </li>
                </ul>
                <p class="text-title-same"><?=Html::encode($title)?> недвижимости</p>
            </div>
        </div>
    </div>
    <div class="arenda-content">
        <div class="container">
            <div class="col-xs-12 padd-zero" style="padding-bottom: 10px !important;">

                <span id="category_data_cont" 
                typeid="<?=$parent_id?>" 
                catalogcategory="<?=$active_category=='all' ? 0 : $active_category?>"></span>

                <p class="hidden" data-category="<?= Yii::t('main','Category') ?>" data-area="<?= Yii::t('main','Area') ?>" data-region="<?= Yii::t('main','Regione') ?>" id="product_transl"></p>

                <div class="row" id="filters-category-section">
                    <div class="col-md-3 col-sm-6 style-katalog-padding">
                        <select class="form-style select-style form_style_tup" name="order-type">
                            <option value="SORT_DESC">по убыванию</option>
                            <option value="SORT_ASC">по возрастанию</option>
                        </select>                            
                    </div>
                    <div class="col-md-3 col-sm-6 style-katalog-padding">
                        <select class="form-style select-style form_style_tup" name="order-name">
                            <option value="date_create">по дате публикации</option>
                            <option value="price_usd">по цене</option>
                        </select>                            
                    </div>
                </div>

                <ul class="arenda-ta-style">
                    <li>
                        <a href="<?=$langLink?>/category/<?= $link?>" class="<?='all'==$active_category ? 'tab-active-arend' : '' ?>">
                            Все
                        </a>
                    </li>
                    <?php foreach ($categories as $category): ?>
                        <li>
                            <a href="<?=$langLink?>/category/<?= $link.'/'.$category->id ?>" class="<?=$category->id==$active_category ? 'tab-active-arend' : '' ?>">
                                <?= Yii::$app->mycomponent->ifSet($category->name) ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                    <span id="dunamic_container">
                    <?php $k=0; foreach ($categories as $category): ?>                
                    <?php if($category->id==$active_category || $active_category == 'all') : ?>

                        <?php if($active_category == 'all' && $k==0) : ?>  
                        <p class="arenda-cont-title">
                            Результат поиска <?= $count_prod ?>
                            <?php $k++; ?>
                        </p>
                        <?php elseif($active_category !== 'all'): ?>
                        <p class="arenda-cont-title">
                            Результат поиска <?= count($category->product) ?>
                        </p>
                        <?php endif; ?>
                        
                        <?php $i=0; foreach ($category->product as $product): ?>
                            <?= SingleProduct::widget([ 'product' => $product ]) ?>
                        <?php endforeach;?>
                        
                        
                    <?php endif; ?>
                    <?php endforeach; ?>
                    </span><!-- end dunamic container -->


                    <?= LinkPager::widget(['pagination'=>$pagination]);  ?>
                </div>
            </div>
        </div>
    </div>
</div>