<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use app\models\Lang;
use app\widgets\SingleProduct;
$langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
$this->title = $title;
?>
<div class="content">
    <div class="arenda-head">
        <div class="container">
            <div class="col-xs-12 padd-zero style-padding-ul">
                <ul class="breadcrumb my-breadcrumb-style">
                    <li>
                        <a class="style-a-color" href="../"><?= Yii::t('main','home') ?></a>
                    </li>
                    <li>
                        <a class="style-a-color active-color"><?=Html::encode($title)?></a>
                    </li>
                </ul>
                <p class="text-title-same"><?=Html::encode($title)?></p>
            </div>
        </div>
    </div>
    <div class="arenda-content">
        <div class="container">
            <div class="col-xs-12 padd-zero" style="padding-bottom: 10px !important;">

                    <span id="dunamic_container">

                        <p class="arenda-cont-title">
                            Результат поиска <?= $count_prod ?>
                        </p>

                        <?php $i=0; foreach ($products as $product): ?>
                            <?= SingleProduct::widget([ 'product' => $product ]) ?>
                        <?php endforeach;?>

                    </span><!-- end dunamic container -->


                    <?= LinkPager::widget(['pagination'=>$pagination]);  ?>
                </div>
            </div>
        </div>
    </div>
</div>