<?php
use yii\helpers\Html;
use app\models\Lang;
use yii\helpers\ArrayHelper;
use app\widgets\SingleProduct;
$langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
$this->title = $title;
?>

<div class="content">
    <div class="katalog-style">
        <div class="container">
            <div class="col-xs-12 padd-zero style-padding-ul">
                <ul class="breadcrumb my-breadcrumb-style">
                    <li>
                        <a class="style-a-color"><?= Yii::t('main','home') ?></a>
                    </li>
                    <li>
                        <a class="style-a-color active-color"><?= Yii::t('main','Catalog') ?></a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 padd-zero katalog-style-border text-center-mob">
                <span class="text-title-same"><?= Yii::t('main','Real_Estate_Directory') ?></span>
                <ul class="tabs-style-katalog" id="tabs-catalog">
                    <li>
                        <a data-category="1" class="katalog-active"><?= Yii::t('main','RENTALS') ?></a>
                    </li>
                    <li>
                        <a data-category="2" class=""><?= Yii::t('main','SALES') ?></a>
                    </li> 
                </ul>
            </div>
        </div>
    </div>
    <div class="katalog-content-style">
        <div class="container">
            <div class="col-xs-12 padd-zero">
                <div class="tab-katalog">
                    <div class="col-xs-12 padd-zero" style="margin-top: 20px;">
                        <p style="font: 15px roman; color: #555555;"><?= Yii::t('main','Search_Options') ?>:</p>
                    </div>
                    
                    <?= Html::beginForm(['catalog/search'], 'post', ['class' => 'form-inline']); ?>
                    <div class="col-xs-12 padd-zero" id="filters-section" style="margin-top: 20px;">
                        <div class="row" style="padding: 10px 15px;">
                            <div class="col-md-3 col-sm-6 style-katalog-padding" >
                                <?= Html::dropDownList('order-type', null ,
                                    [
                                        'SORT_DESC' => 'по убыванию',
                                        'SORT_ASC' => 'по возрастанию'                                        
                                    ],
                                    [
                                        'class' => 'form-style select-style form_style_tup',
                                    ]) ?>
                            </div>
                            <div class="col-md-3 col-sm-6 style-katalog-padding" >
                                <?= Html::dropDownList('order-name', null ,
                                    [
                                        'date_create' => 'по дате публикации',
                                        'price_usd' => 'по цене'                                        
                                    ],
                                    [
                                        'class' => 'form-style select-style form_style_tup',
                                    ]) ?>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 style-katalog-padding" >
                            <p class="label-text-style"><?= Yii::t('main','Choose_region') ?>:</p>
                            <?= Html::dropDownList('catalog-region', null ,
                                ArrayHelper::map($arrStates, 'id', 'name'),
                                [
                                    'class' => 'form-style select-style form_style_tup',
                                    'prompt' => 'Select Region',
                                ]) ?>
                        </div>
                        <div class="col-md-3 col-sm-6 style-katalog-padding">
                            <p class="label-text-style"><?= Yii::t('main','Choose_location') ?>:</p>
                            <?= Html::dropDownList('catalog-category', null ,
                                ArrayHelper::map($categories, 'id', 'name'), 
                                [
                                    'class' => 'form-style select-style form_style_tup',
                                    'prompt' => 'Select Category',
                                ]) ?>
                        </div>
                        <div class="col-md-3 col-sm-6 style-katalog-padding">
                            <p class="label-text-style"><?= Yii::t('main','Price_list') ?>:</p>
                            <?= Html::dropDownList('catalog-price', null ,
                                [
                                    '0-100000'=>'Низкие цены', 
                                    '100000-350000'=>'Средные цены',
                                    '350000-10000000'=>'Высокие цены',
                                ], 
                                [
                                    'class' => 'form-style select-style form_style_tup',
                                    'prompt' => 'Select Price',
                                ]) ?>                   
                        </div>
                        <div class="col-md-2 col-sm-3 style-katalog-padding same-marg-button catalog-reset-container">
                            <a class="sbros-button" id="catalog-reset" data-type="1"><?= Yii::t('main','Reset') ?></a>
                        </div>
                    </div>
                    <?= Html::submitButton('', ['class' => 'hidden', 'id' => 'ajax-search']) ?>
                    <?= Html::endForm() ?>
                    <div class="col-xs-12 padd-zero margin-katalog-panel">
                        <p class="hidden" data-category="<?= Yii::t('main','Category') ?>" data-area="<?= Yii::t('main','Area') ?>" data-region="<?= Yii::t('main','Regione') ?>" id="product_transl"></p>
                        <p class="arenda-cont-title" id="search_count"><?= Yii::t('main','Search_results') ?> <span><?=Html::encode($count)?></span></p>


                        <span id="dunamic_container">
                        <?php foreach ($products as $product): ?>
                            <?= SingleProduct::widget([ 'product' => $product ]) ?>
                        <?php endforeach;?>
                        </span><!-- end dunamic container -->

                        
                    </div>
                    <button class="btn btn-info <?=$count>count($products)? '': 'hidden'?>" id="load_more" 
                    data-offset="<?=count($products)?>">
                        Загрузить еще
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>