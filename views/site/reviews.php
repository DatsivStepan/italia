<?php
use yii\helpers\Html;
use yii\bootstrap\Alert;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
if(Yii::$app->session->hasFlash('confirm')):
    echo Alert::widget([
            'options' => [
                    'class' => 'alert-info',
            ],
            'body' => Yii::t('main','Feedback_Confirmed') ,
    ]);
endif;
if(Yii::$app->session->hasFlash('deleted')):
    echo Alert::widget([
            'options' => [
                    'class' => 'alert-danger',
            ],
            'body' => Yii::t('main','Review_Removed'),
    ]);
endif;
if(Yii::$app->session->hasFlash('added')):
    echo Alert::widget([
            'options' => [
                    'class' => 'alert-success',
            ],
            'body' => Yii::t('main','Review_Posted_on_Moderation'),
    ]);
endif;
$this->title = $title;
?>
<div class="content">
    <div class="investicii">
        <div class="container">
            <div class="col-xs-12 padd-zero style-padding-ul">
                <ul class="breadcrumb my-breadcrumb-style">
                    <li>
                        <a class="style-a-color"><?= Yii::t('main','home') ?></a>
                    </li>
                    <li>
                        <a class="style-a-color active-color"><?= Yii::t('main','Reviews') ?></a>
                    </li>
                </ul>
            </div>
            <?php if($isLogined==true) : ?>
            <div class="row text-center">
                <?php $form = ActiveForm::begin(); ?>
                <div class="col-sm-6 col-sm-push-3">
                    <label class="review_label"><?= Yii::t('main','Leave_your_feedback') ?></label>
                    <?= $form->field($newReview, 'content')->textarea(['class' => 'form-style form_style_textarea', 'value'=>''])->label(false) ?>
                    <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn review_add']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <?php endif; ?>
            <div class="col-xs-12 otzivi-padd-style">
                <p class="text-title-same"><?= Yii::t('main','Reviews') ?></p>
                <div class="text-center-mob" id="masonry_hybrid_demo1"> 
                    <div class="grid-sizer"></div> 
                    <div class="gutter-sizer"></div>
                    <?php foreach ($reviews as $review): ?>
                    <div class="grid-item">
                        <p style="font:20px bold; color: black;"><?= Html::encode($review->name) ?></p>
                        <?php if($isAdmin==true): ?>
                        <div class="row">
                            <?php if(!$review->status): ?>
                            <div class="col-xs-6"><a href="?confirm=<?=$review->id?>" class="btn btn-success"><?= Yii::t('main','Confirm') ?></a></div>
                            <?php endif; ?>
                            <div class="col-xs-6"><a href="?delete=<?=$review->id?>" class="btn btn-danger"><?= Yii::t('main','Uninstall') ?></a></div>
                        </div>
                        <?php endif; ?>
                        <span style="font:13px roman; color: #6c6c6c; line-height: 25px;">
                            <?= Html::encode($review->content) ?>
                        </span>
                        <p style="font:13px roman; color: #4b3a39;">
                            <?= Yii::$app->formatter->asDate($review->date_create, 'php:d-m-Y') ?>
                        </p>
                    </div>
                    <?php endforeach; ?>
                    <?= LinkPager::widget(['pagination'=>$pagination]);  ?>    
                </div>
            </div>
        </div>
    </div>
</div>