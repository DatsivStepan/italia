<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1>Signup</h1>
    
    <?php $form = ActiveForm::begin(['id' => 'form-signup',
        'options' => ['class' => 'form-horizontal loginAndSingupForms'],
            ]); ?>
        <?= $form->field($newModel, 'username')->textInput(
            ['autofocus' => true, 'placeholder'=>'Введите логин'])
            ->label(false) ?>
        <?= $form->field($newModel, 'email')->textInput(
            ['placeholder'=>'Введите email'])
            ->label(false) ?>
        <?= $form->field($newModel, 'password')->passwordInput(['placeholder'=>'Введите пароль'])->label(false) ?>
        <?= $form->field($newModel, 'password_repeat')->passwordInput(['placeholder'=>'Повторите пароль'])->label(false) ?>
        
        <div class="form-group">
                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

    
</div>
