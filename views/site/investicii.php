<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$arraySetting = ArrayHelper::map(app\models\Setting::find()->all(),'key','value');
$this->title = $title;
?>

<div class="content">
    <div class="investicii">
        <div class="container">
            <div class="col-xs-12 padd-zero style-padding-ul">
                <ul class="breadcrumb my-breadcrumb-style">
                    <li>
                        <a href="../" class="style-a-color"><?= Yii::t('main','home') ?></a>
                    </li>
                    <li>
                        <a class="style-a-color active-color"><?=Html::encode($page->curentLangPages[0]->name)?></a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 " style="padding-bottom: 40px;">
                <p style="font: 25px bold; color: black;"><?=Html::encode($page->curentLangPages[0]->name)?></p>
                <div class="col-md-9 col-sm-8 class-for-ul">
                    <?=Html::decode($page->curentLangPages[0]->content)?>
                </div>
                <div class="col-md-3 col-sm-4 right-panel">
                    <p style="font: 20px bold; color: black;">
                        <?= Yii::t('main','You_can_always_learn_more_from_our_managers') ?>
                    </p>
                    <div class="col-xs-12 padd-zero">
                        <img src="/img/telephone_footer.png">
                        <span class="right-panel-number"><?=$arraySetting['phone']?></span>
                    </div>
                    <div class="all-information for-image-corr">
                        <img src="/img/navigation_footer.png"/>
                        <div class="inline">
                            <span class="right-panel-text-style"><?=$arraySetting['adress']?></span>
                        </div>
                    </div>
                    <div class="all-information">
                        <img src="/img/email_footer.png"/>
                        <span class="right-panel-text-style"><?=$arraySetting['email']?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>