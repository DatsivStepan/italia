<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\Lang;
$arraySetting = ArrayHelper::map(app\models\Setting::find()->all(),'key','value');
$langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
$this->title = $title;
?>

<div class="content">
    <div class="kontakti-head">
        <div class="container">
            <div class="col-xs-12 padd-zero style-padding-ul">
                <ul class="breadcrumb my-breadcrumb-style">
                    <li>
                        <a href="../" class="style-a-color"><?= Yii::t('main','home') ?></a>
                    </li>
                    <li>
                        <a class="style-a-color active-color"><?= Yii::t('main','contacts') ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="kontaki-content">
        <div class="container">
            <div class="col-xs-12">
                <p class="text-title-same"><?= Yii::t('main','contacts') ?></p>
            </div>
            <div class="col-xs-12 kontakt-location">
                <div class="col-xs-12 map-kontakt">
                   <!-- <img class='map-loc-kontakt' src='/img/map-kontakti.png'/>-->
                    <div id="map_container">
                        <div id="map" style="height:551px; width:100%;"></div>
                        <input type="hidden" value="<?=$arraySetting['adress']?>" required="required" name="search_address" class="form-control kart_city" id="searchTextField">
                    </div>
                    <div class="col-md-4 col-sm-5 block-inform-kontakt padd-zero">
                        <div class="col-xs-12 ">
                            <div class="col-xs-12 padd-numb-kontakti">
                                <p class="number-kontakt-block"><?=$arraySetting['phone']?></p>
                            </div>
                            <div class='col-xs-12 same-display otstup-left'>
                                <img style='float: left;' src="/img/navig-kontakti.png">
                                <div class="text-block-loc">
                                    <span class='loc-kontakt-style'><?=$arraySetting['adress']?></span>
                                </div>
                            </div>
                            <div class='col-xs-12 same-display'>
                                <img style='float: left;' src='/img/email-kontakti.png'/>
                                <div class="text-block-loc">
                                    <span class='loc-kontakt-style'><?=$arraySetting['email']?></span>
                                </div>
                            </div>
                            <div class='col-xs-12'>
                                <div class="col-xs-12 worck-graphik">
                                    <span class='worck-text'>
                                       <?= Yii::t('main','schedule_of_work') ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="div-border"></div>
                        <div class="col-xs-12 text-vertical-center online-padd">
                            <div class="col-xs-12 padd-zero">
                                <div class="col-lg-4 col-md-5">
                                    <span class="online-text"><?= Yii::t('main','We_are_online') ?>:</span>
                                </div>
                                <div class="col-md-6 online-img-style">
                                    <a href="<?=$arraySetting['fb_link']?>"><img src="/img/facebook_footer.png"></a>
                                    <a href="<?=$arraySetting['inst_link']?>"><img src="/img/instagram_footer.png"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 block-iform-mob padd-zero">
                    <div class="col-xs-12 padd-numb-kontakti">
                        <p class="number-kontakt-block"><?=$arraySetting['phone']?></p>
                    </div>
                    <div class='col-xs-12 same-display otstup-left'>
                        <img style='float: left;' src="/img/navig-kontakti.png">
                        <div class="text-block-loc">
                            <span class='loc-kontakt-style'><?=$arraySetting['adress']?></span>
                        </div>
                    </div>
                    <div class='col-xs-12 same-display'>
                        <img style='float: left;' src='/img/email-kontakti.png'/>
                        <div class="text-block-loc">
                            <span class='loc-kontakt-style'><?=$arraySetting['email']?></span>
                        </div>
                    </div>
                    <div class='col-xs-12'>
                        <div class="col-xs-12 worck-graphik">
                            <span class='worck-text'>
                                <?= Yii::t('main','schedule_of_work') ?>
                            </span>
                        </div>
                    </div>
                    <div class="div-border">

                    </div>
                    <div class="col-xs-12 text-vertical-center online-padd">
                        <div class="col-lg-4 col-md-5 col-sm-2 col-xs-4">
                            <span class="online-text"><?= Yii::t('main','We_are_online') ?>:</span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 online-img-style">
                            <a href="<?=$arraySetting['fb_link']?>"><img src="/img/facebook_footer.png"></a>
                            <a href="<?=$arraySetting['inst_link']?>"><img src="/img/instagram_footer.png"></a>
                        </div>
                    </div>
                </div>
                <form class="col-xs-12 question-contakt" id="ask_guestion_site">
                    <p class="question-title"><?= Yii::t('main','Have_questions') ?>?</p>
                    <div class="col-xs-12 padd-zero">
                        <div class="col-md-4 col-xs-12 padd-zero">
                            <input type="hidden" name="email_user" value="<?=$arraySetting['email']?>" >
                            <input type="text" name="name" placeholder="<?= Yii::t('main','Your_name') ?>:" class="form-style-kartochka"/>
                            <input type="text" name="email" placeholder="<?= Yii::t('main','Your_email_mail') ?>:" class="form-style-kartochka"/>
                        </div>
                        <div class="col-md-8 col-xs-12 otstup-message">
                            <textarea name="text" placeholder="<?= Yii::t('main','Your_message') ?>:" class="form-style-kartochka write-form-kontakti"></textarea>
                        </div>
                    </div>
                    <div class='col-xs-12 after-send-padd text-vertical-center text-center-mob'>
                        <div class="col-lg-2 col-sm-3">
                            <a class='send-button-contact'><?= Yii::t('main','Pay_now') ?></a>
                        </div>
                        <div class="col-lg-10 col-sm-9 mob-padd">
                            <span class='fancks-after-send'><?= Yii::t('main','Thank_you_for_contacting') ?></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xs-12 buy-house-padd">
                <p class='text-title-same'><?= Html::encode($howBuy->curentLangPages[0]->name) ?></p>
                <span class='how-to-buy'>
                    <?= Html::decode($howBuyContent) ?>
                </span>
                <p class='read-next-press'><a href="<?=$langLink?>/site/how-buy">Читать дальше</a></p>
            </div>
        </div>
    </div>
</div>