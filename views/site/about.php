<?php
use yii\helpers\Html;
use app\models\Lang;
$langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
$this->title = $title;
?>

<div class="content">
    <div class="OurProject">
        <div class="container">
            <div class="col-xs-12 padd-zero style-padding-ul">
                <ul class="breadcrumb my-breadcrumb-style">
                    <li>
                        <a class="style-a-color"><?= Yii::t('main','home') ?></a>
                    </li>
                    <li>
                        <a class="style-a-color active-color"><?= Html::encode($aboutProg->curentLangPages[0]->name) ?></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 style-katalog-padding-left">
                <div class="col-xs-12 padd-zero">
                    <p class="text-title-same"><?= Html::encode($aboutProg->curentLangPages[0]->name) ?></p>
                    <span class='how-to-buy'>
                        <?= Html::decode($aboutProg->curentLangPages[0]->content) ?>
                    </span>
                </div>
                <div class="col-xs-12 O_project_padd">
                    <img class='img_o_proekte' src='/img/img_proect1.png' />
                </div>
            </div>
            <div class="col-md-6 col-xs-12 style-katalog-padding-right">
                <div class="col-xs-12 padd-zero">
                    <img class='img_o_proekte' src='/img/img_proect2.png' />
                </div>
                <div class="col-xs-12 O_project_padd">
                    <p class="text-title-same"><?= Html::encode($ourPhilosof->curentLangPages[0]->name) ?></p>
                    <span class='how-to-buy'>
                        <?= Html::decode($ourPhilosof->curentLangPages[0]->content) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class='preymychestva'>
        <div class='container'>
            <div class='col-xs-12 preymychestva-padd-col'>
                <p class='text-title-same'>Преимущества</p>
                <div class="col-xs-12 padding-text-preymych">
                    <div class='col-md-4 col-xs-12'>
                        <p class='preymych-title'>10</p>
                        <span class='preymych-text'>
                            10 лет на рынке недвижимости в Италии
                        </span>
                    </div>
                    <div class='col-md-4 col-xs-12'>
                        <p class='preymych-title'>100</p>
                        <span class='preymych-text'>
                            Более 100 крупных сделок
                            по продаже и аренде недвижимости
                            в Италии
                        </span>
                    </div>
                    <div class='col-md-4 col-xs-12'>
                        <p class='preymych-title'>15</p>
                        <span class='preymych-text'>
                            15 профессионалов, которые помогут
                            вам и ответят на все вопросы
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="partneri_porect">
        <div class="container">
            <div class="col-xs-12 partner-proect-padding">
                <div class="col-xs-12 text-center padd-titl-proekte">
                    <p class="text-title-same">Наши партнеры</p>
                </div>
                <div class="col-xs-12">
                    <div class="text-center-mob" id="masonry_hybrid_demo3"> 
                        <div class="grid-sizer"></div> 
                        <div class="gutter-sizer"></div> 
                        <?php foreach ($partners as $partner): ?>
                        <div class="grid-item text-center partn-same-padd">
                            <img src="/<?= Html::encode($partner->img) ?>" />
                            <p class="partner-text-style"><?= Html::encode($partner->name) ?></p>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="col-xs-12 text-center">
                        <a href="<?=$langLink?>/site/partners" class="see-all-button">Смотреть всех </a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 padding-buy-house-proekte">
                <p class='text-title-same'><?= Html::encode($howBuy->curentLangPages[0]->name) ?></p>
                <span class='how-to-buy'>
                    <?= Html::decode($howBuyContent) ?>
                </span>
                <p class='read-next-press'><a href="<?=$langLink?>/site/how-buy">Читать дальше</a></p>
            </div>
        </div>
    </div>
</div>
