<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use app\models\Lang;
$langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
?>
<div class="content">
    <div class="arenda-head">
        <div class="container">
            <div class="col-xs-12 padd-zero style-padding-ul">
                <ul class="breadcrumb my-breadcrumb-style">
                    <li>
                        <a class="style-a-color" href="../"><?= Yii::t('main','home') ?></a>
                    </li>
                    <li>
                        <a class="style-a-color active-color">Поиск</a>
                    </li>
                </ul>
                <p class="text-title-same">Результаты Поиска</p>
            </div>
        </div>
    </div>
    <div class="arenda-content search-result-content">
        <div class="container">
            <div class="col-xs-12 padd-zero" style="padding-bottom: 10px !important;">

                    <?php if(count($modelProducts)) : ?>
                        <?php foreach ($modelProducts as $product): ?>
                        <a href="<?=$langLink?>/product/<?= Html::encode($product->id) ?>">
                        <div class="content-class-style"><!-- start product container -->
                            <div class="col-xs-12 hover-div-slider padd-zero"
                            style="background-image: url(/images/product/<?= Html::encode($product->img_src) ?>);">
                                <div class="col-xs-12 padd-zero hov_img_none">
                                </div>
                                <div class="change-hover">
                                    <div class="col-xs-12 padd-zero style_border">
                                        <div class="col-xs-12">
                                            <p class="title_room">
                                                <?= Html::encode($product->name) ?>
                                            </p>
                                            <div class="width_style">
                                                <span class="span-title">Категория:</span> 
                                                <span class="span-text">
                                                    <?= Html::encode($product->category->name) ?>
                                                </span>
                                            </div>
                                            <div class="width_style">
                                                <span class="span-title">Площадь:</span> 
                                                <span class="span-text">
                                                    <?= Html::encode($product->object_area) ?>
                                                </span>
                                            </div>
                                            <div class="width_style">
                                                <span class="span-title">Регион:</span> 
                                                <span class="span-text">
                                                    <?= Html::encode($product->region->name) ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 padd-zero style-padd-div-same">
                                        <div class="col-sm-7 col-xs-7">
                                            <p class="money_style">
                                            <?= Html::encode($product->currenc->name.' '.$product->price) ?>
                                            </p>
                                        </div>
                                        <div class="col-sm-2 col-xs-2 padd-zero">
                                            <img class="style-img shower_class" src="/img/shower_icon.png">
                                            <span class="numb-style-text">2</span>
                                        </div>
                                        <div class="col-sm-3 col-xs-3 padd-zero ">
                                            <img class="style-img bed_class" src="/img/bed_icon.png">
                                            <span class="numb-style-text">3</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end product container -->
                        </a>
                        <?php endforeach; ?>
                        <?= LinkPager::widget(['pagination'=>$pagination]);  ?>
                    <?php else: ?>
                    <h2 id="notResults">Ничего не найдено</h2>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>