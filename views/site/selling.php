<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<div class="content">
    <div class="arenda-head">
        <div class="container">
            <div class="col-xs-12 padd-zero style-padding-ul">
                <ul class="breadcrumb my-breadcrumb-style">
                    <li>
                        <a class="style-a-color" href="../">Главная</a>
                    </li>
                    <li>
                        <a class="style-a-color active-color">Продажа</a>
                    </li>
                </ul>
                <p class="text-title-same">Продажа недвижимости</p>
            </div>
        </div>
    </div>
    <div class="arenda-content">
        <div class="container">
            <div class="col-xs-12 padd-zero" style="padding-bottom: 10px !important;">
                <ul class="nav nav-tabs">
                    <?php $k=0; foreach ($categories as $category): ?>
                        <li class="<?=$k==0 ? 'active' : '' ?>">
                            <a href="#tab<?= Html::encode($category->id) ?>">
                                <?= Html::encode($category->name) ?>
                            </a>
                        </li>
                    <?php $k++; endforeach; $k=0;?>
                </ul>
                
                <div class="tab-content">
                    <?php $k=0; foreach ($categories as $category): ?>                
                    <div id="tab<?= Html::encode($category->id) ?>" class="tab-pane fade <?=$k==0 ? 'in active' : '' ?>"><!-- start tab container -->
                        <p class="arenda-cont-title">
                            Результат поиска <?= count($category->product) ?>                                
                        </p>

                        <?php $i=0; foreach ($category->product as $product): ?>
                        <?php if($product->category_id==$category->id): ?>
                        <a href="/product/<?= Html::encode($product->id) ?>">
                        <div class="content-class-style"><!-- start product container -->
                            <div class="col-xs-12 hover-div-slider padd-zero"
                            style="background-image: url(../images/product/<?= Html::encode($product->img_src) ?>);">
                                <div class="col-xs-12 padd-zero hov_img_none">
                                </div>
                                <div class="change-hover">
                                    <div class="col-xs-12 padd-zero style_border">
                                        <div class="col-xs-12">
                                            <p class="title_room">
                                                <?= Html::encode($product->name) ?>
                                            </p>
                                            <div class="width_style">
                                                <span class="span-title">Категория:</span> 
                                                <span class="span-text">
                                                    <?= Html::encode($product->category->name) ?>
                                                </span>
                                            </div>
                                            <div class="width_style">
                                                <span class="span-title">Площадь:</span> 
                                                <span class="span-text">
                                                    <?= Html::encode($product->object_area) ?>
                                                </span>
                                            </div>
                                            <div class="width_style">
                                                <span class="span-title">Регион:</span> 
                                                <span class="span-text">
                                                    <?= Html::encode($product->region->name) ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 padd-zero style-padd-div-same">
                                        <div class="col-sm-7 col-xs-7">
                                            <p class="money_style">
                                            € <?= Html::encode($product->price) ?>
                                            </p>
                                        </div>
                                        <div class="col-sm-2 col-xs-2 padd-zero">
                                            <img class="style-img shower_class" src="/img/shower_icon.png">
                                            <span class="numb-style-text">2</span>
                                        </div>
                                        <div class="col-sm-3 col-xs-3 padd-zero ">
                                            <img class="style-img bed_class" src="/img/bed_icon.png">
                                            <span class="numb-style-text">3</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end product container -->
                        </a>
                        <?php $i++; endif; ?>
                        <?php endforeach; $k=$i=0;?>
                        
                    </div><!-- end tab -->
                    <?php $k++; ?>
                    <?php endforeach; $k=0;?>
                </div><!-- end tab-content -->

                </div>
            </div>
        </div>
    </div>
</div>