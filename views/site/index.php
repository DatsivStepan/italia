<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\widgets\OutpupProduct;
use app\widgets\DoubleTabsWidget;
use app\widgets\SingleProduct;
use app\models\Lang;
/* @var $this yii\web\View */
$langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
$this->title = $title;
$parent_cats_arr = [ 2, 1 ];
?>
    <div class="search">
    <?php $form = ActiveForm::begin(['action' =>['site/search']]); ?>
        <div class="container">
            <div class="col-xs-12 padd-search-div text-center">
                <p class="title-white-color"><?= Yii::t('main','PROPERTY_PROPERTY_IN_ITALY_IS_AT_US') ?></p>
            </div>
            <div class="col-xs-12 style-padding-select-catalog">
                <div class="col-md-3 col-sm-4 otstup">
                    <p class='style-p-text'><?= Yii::t('main','SEARCH_OF_THE_REAL_ESTATE') ?>:</p>
                    <input type="text" name="name" placeholder="<?= Yii::t('main','For_example_Rent_an_apartment') ?>" class="form-style"/>
                </div>
                <div class="col-md-2 col-sm-4 otstup">
                    <p class='style-p-text'><?= Yii::t('main','PRICE') ?>:</p>
                    <select class="form-style select-style" name="price">
                        <option value="">Все</option>
                        <option value="0-100000">< 100 000 €</option>
                        <option value="100000-200000">100 000 - 200 000 €</option>
                        <option value="200000-350000">200 000 - 350 000 €</option>
                        <option value="350000-450000">350 000 - 450 000 €</option>
                        <option value="450000-100000000">> 450 000 € </option>
                    </select>
                </div>
                <div class="col-md-2 col-sm-4 otstup">
                    <p class='style-p-text'><?= Yii::t('main','AREA') ?>:</p>
                    <select class="form-style select-style" name="area">
                        <option value="">Все</option>
                        <option value="0-60">< 60 <?= Yii::t('main','M2') ?></option>
                        <option value="60-90">60-90 <?= Yii::t('main','M2') ?></option>
                        <option value="90-120">90-120 <?= Yii::t('main','M2') ?></option>
                        <option value="120-150">120-150 <?= Yii::t('main','M2') ?></option>
                        <option value="150-10000">> 150 <?= Yii::t('main','M2') ?></option>
                    </select>
                </div>
                <div class="col-md-2 col-sm-4 otstup">
                    <p class='style-p-text'><?= Yii::t('main','CATEGORY') ?>:</p>
                    <select class="form-style select-style" name="category">
                        <option value="">Все</option>
                        <?php foreach ($categories as $category): ?>
                            <?php if($category['parent_id'] == 1): ?>
                                <option value="<?=$category['name']?>"><?=$category['name']?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-2 col-sm-4 otstup">
                    <p class='style-p-text'><?= Yii::t('main','REGION') ?>:</p>
                    <select class="form-style select-style" name="region">
                        <option value="">Все</option> 
                        <?php foreach ($arrStates as $state): ?>
                        <option value="<?=$state['id']?>"><?=$state['name']?></option>
                        <?php endforeach; ?>             
                    </select>
                </div>
                <div class="col-md-1 col-sm-4 otstup no-padding">
                    <?= Html::submitButton('', ['class' => 'btn btn-default search-style-Button']) ?>
                </div>
            </div>
            <div class="col-xs-12" style="
                text-align: center;
                padding-top: 72px;
                padding-bottom: 43px;
                visibility: hidden;">
                <img src="/img/setting_icon.png"/>
                <select class="ultra_search">
                    <option><?= Yii::t('main','Advanced_Search') ?></option>
                </select>
            </div>
        </div>
    <?php ActiveForm::end(); ?>  
    </div>


    <div class="proect">
        <div class="container">
            <div class="col-xs-12 padding-div-style">
                <p class="titl-all"><?= Yii::t('main','BRIEFLY_ABOUT_OUR_PROJECT') ?></p>
            </div>
            <div class="col-xs-12 padd-zero" style="text-align: center;">
                <p style="margin: 0; font-family:roman; color: #353535;">Fly & Travel Club - <?= Yii::t('main', 'Fly_Travel_Club') ?> </p>
            </div>
            <div class="col-xs-12" style="text-align: center;padding-top: 30px; padding-bottom: 48px;">
                <a href="<?=$langLink?>/site/about" class="read_reatly"><?= Yii::t('main','READ_MORE_READ_MORE') ?></a>
            </div>
        </div>
    </div>
    <div class="proposition">
        <div class="container">
            <div class="col-xs-12 padding-div-style">
                    <p class=" titl-all title_slider"><?= Yii::t('main' , 'OFFERS_PREMIUM') ?></p>
                    <a href="<?=$langLink?>/catalog" class="katalog_button"><?= Yii::t('main', 'TO_CATALOG') ?></a>
            </div>
            <div class="col-xs-12 padd-zero">
                <?= OutpupProduct::widget(['products'=>$product_vip]) ?>
            </div>
        </div>
    </div>
    <div class="newStend">
        <div class="container">
            <div class="col-xs-12 padding-div-style">
                <p class="titl-all title-new-add"><?= Yii::t('main','NEW_APPS') ?></p>
            </div>
            <div class="col-xs-12 padd-zero" style="margin-bottom: 30px;">

            <?php foreach ($newProducts as $newProduct) : ?>
                <div class="content-class-style-bl">
                    <?= SingleProduct::widget([ 'product' => $newProduct ]) ?>
                </div>
            <?php endforeach; ?>
                
            </div>
        </div>
    </div>
    <div class="whyUs">
        <div class="container-fluid">
            <div class="col-xs-12 padding-div-style">
                <p class="title-white-color"><?= Yii::t('main','WHY_ARE_LOOKING_AND_BUYING_ACCOMMODATION_THROUGH_OPTIMA_DOMUS') ?></p>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-3 text-center">
                        <div class="marging-style-us">
                            <p class="head-title"><?= Yii::t('main','WHY_WE') ?></p>
                            <p class="content-title">
                                <?= Yii::t('main','TEXT1') ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="marging-style-us">
                            <p class="head-title"><?= Yii::t('main','CUSTOMER_KNOWLEDGE') ?></p>
                            <p class="content-title">
                                <?= Yii::t('main','TEXT1') ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="marging-style-us">
                            <p class="head-title"><?=   Yii::t('main','ATTENTION_TO_DETAIL')?></p>
                            <p class="content-title">
                                <?= Yii::t('main','TEXT2') ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="marging-style-us">
                            <p class="head-title"><?=   Yii::t('main','SECURITY')?></p>
                            <p class="content-title">
                                <?= Yii::t('main','TEXT2') ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 text-center">
                        <div class="marging-style-us">
                            <p class="head-title"><?= Yii::t('main','ABOUT_LEASE')?></p>
                            <p class="content-title">
                                <?= Yii::t('main','TEXT3') ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="marging-style-us">
                            <p class="head-title"><?= Yii::t('main','COMBINATION_OF_PRICE_AND_QUALITY')?></p>
                            <p class="content-title">
                                <?= Yii::t('main','TEXT3') ?>
     
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="marging-style-us">
                            <p class="head-title"><?= Yii::t('main','BUY_WITH_US')?></p>
                            <p class="content-title">
                                <?= Yii::t('main','TEXT3') ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="marging-style-us">
                            <p class="head-title"><?= Yii::t('main','RELIABLE_PARTNERS')?></p>
                            <p class="content-title">
                                <?= Yii::t('main','TEXT3') ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tabsButton">

        <?= DoubleTabsWidget::widget(['categories' => $categories ]) ?>

    </div>
    

    <div class="buyAndrenting">
        <div class="container">
            <div class="col-xs-12 padding-div-style">
                <p class="titl-all"><?= Yii::t('main','HOW_TO_BUY_OR_LEASE_HOUSING_THROUGH') ?> OPTIMA DOMUS</p>
            </div>
            <div class="col-xs-12 padd-zero bottom-style">
                <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12 padd-zero display-base style-same-marg buy-andrenting-col-1">
                    <div class="col-xs-3 padd-zero">
                        <img class="home-img-media" src="/img/home-icon.png"/>
                    </div>
                    <div class="col-lg-7 col-md-6 col-sm-7 col-xs-6 padd-zero">
                        <p class="choose_catalog"><?= Yii::t('main','SELECT_OBJECT_FROM_CATALOG') ?></p>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-2 col-xs-3 padd-zero">
                        <img class="open-img-media" src="/img/open-icon.png"/>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5 col-sm-7 col-xs-12 padd-zero style-same-marg buy-andrenting-col-2">
                    <div class="col-xs-12 padd-zero display-base buy-andrenting-col-2-row-1">
                        <div class="col-sm-2 col-xs-3 padd-zero">
                            <img class="media-width-img" src="/img/doc-icon.png" />
                        </div>
                        <div class="col-sm-10 col-xs-9 padd-zero ">
                            <span class="catalog-span-text">
                                Если вы хотите общаться с итальянским агентством 
                                при нашей поддержке и участии, на русском 
                                языке, необходимо:
                            </span>
                            <p class="choose_catalog open-call-back-popup">
                                ЗАПОЛНИТЬ И ОТПРАВИТЬ ФОРМУ ЗАПРОСА 
                                «СВЯЗАТЬСЯ С НАМИ».
                            </p>  
                            <p class="style-litle-p"><?= Yii::t('main','OR') ?></p>
                        </div>
                    </div>
                    <div class="col-xs-12 padd-zero display-base">
                        <div class="col-sm-2 col-xs-3 padd-zero">
                            <img class="media-width-img" src="/img/mail-icon.png" />
                        </div>
                        <div class="col-sm-10 col-xs-9 padd-zero">
                            <p class="choose_catalog"><?= Yii::t('main','WRITE_US_ON') ?> E-MAIL</p>
                            <span class="catalog-span-email">
                                INFOS@OPTIMA-DOMUS.COM
                            </span>
                            <p class="style-litle-p opacity-zero"><?= Yii::t('main','OR') ?></p>
                        </div>
                    </div>
                    <div class="col-xs-12 padd-zero display-base buy-andrenting-col-2-row-3">
                        <div class="col-sm-2 col-xs-3 padd-zero">
                            <img class="media-width-img" src="/img/tel-icon.png" />
                        </div>
                        <div class="col-sm-10 col-xs-9 padd-zero">
                            <span class="catalog-span-text">
                                Если вы хотите общаться напрямую с итальянским 
                        агентством, без нашего участия, на итальянском 
                        или английском языке, необходимо:
                            </span>
                            <p class="choose_catalog"><?= Yii::t('main','CONTACT_DIRECTLY_WITH_THE_ITALIAN_AGENCY') ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12 media-padd-top style-same-marg padd-zero buy-andrenting-col-3">
                    <div class="col-xs-12 col-sm-5 col-md-12 same-style">
                        <p>АРЕНДА</p>
                        <span>
                            Запрос на аренду объекта
                            отправляется только по форме
                            обратной связи с нами или на
                            электронную почту:
                            info@optima-domus.com.
                        </span>
                        <span>
                            При отправке запроса аренды,
                            просим уточнять:
                            полное Имя и Фамилию,
                            точные даты, количество
                            человек и спальных комнат,
                            недельный бюджет
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-5 col-lg-push-3 col-md-push-4 buy-andrenting-warring col-xs-12">
                <p>ВАЖНО:</p>
                <span>
                    СТОИМОСТЬ ОБЪЕКТА И АГЕНТСКИХ КОМИССИЙ ПРИ 
                    НАШЕМ УЧАСТИИ НЕ МЕНЯЕТСЯ.
                </span>
                <span>
                    ВЫ ПЛАТИТЕ ТОЛЬКО МЕСТНОМУ ИТАЛЬЯНСКОМУ 
                    АГЕНТСТВУ И ПОЛУЧАЕТЕ ДОПОЛНИТЕЛЬНУЮ ПОДДЕРЖКУ 
                    С НАШЕЙ СТОРОНЫ –
                </span>
                <a href="site/about">читать подробнее</a>
            </div>
        </div>
    </div>
    <div class="lowPrice">
        <div class="container">
            <div class="col-xs-12 padding-div-style">
                <p class="titl-all"><?= Yii::t('main','LOW_PRICES') ?></p>
            </div>
            <div class="col-xs-12 padd-zero">
                <?= OutpupProduct::widget(['products'=>$product_low]) ?>
            </div>
        </div>
    </div>
