<?php
use yii\helpers\Html;
$this->title = $title;
?>

<div class="content">
    <div class="investicii">
        <div class="container">
            <div class="col-xs-12 padd-zero style-padding-ul">
                <ul class="breadcrumb my-breadcrumb-style">
                    <li>
                        <a href="../" class="style-a-color"><?= Yii::t('main','home') ?></a>
                    </li>
                    <li>
                        <a class="style-a-color active-color"><?= Yii::t('main','Our_partners') ?></a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <p class="row text-title-same"><?= Yii::t('main','Our_partners') ?></p>

                <div class="col-xs-12 partn-marg-style">
                    <?php foreach ($partners as $partner): ?>
                        <div class="my-class-partners text-center partn-same-padd vertical-center">
                            <img src="/<?= Html::encode($partner->img) ?>" />
                            <p class="partner-text-style"><?= Html::encode($partner->name) ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="col-xs-12 col-sm-9 padd-zero">
                    <ul class="padd-ul-style">
                        <li>
                            <p><?= Html::encode($aboutPartner->curentLangPages[0]->name) ?></p>
                            <span class="my-class-span-des">
                                <?= Html::decode($aboutPartner->curentLangPages[0]->content) ?>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>