<?php
    use yii\helpers\Html;
    use app\widgets\ProductLimit;
    $parent_cats_arr = [ 1, 2 ];
?>
<ul class="nav nav-tabs">
    <li class="active">        
        <a data-toggle="tab" href="#parent-tab1"><?= Yii::t('main','RENTALS') ?></a>
    </li>
    <li>
        <a data-toggle="tab" href="#parent-tab2"><?= Yii::t('main','SALES')?></a>
    </li>
</ul>

<div class="tab-content container">
<?php foreach ($parent_cats_arr as $cat_key => $parent_cat): ?>

    <div id="parent-tab<?=$parent_cat?>" class="tab-pane fade <?=$cat_key == 0 ? 'in active' : ''?>">
        
        <ul class="nav nav-tabs">
            <?php $k=0; foreach ($categories as $category): ?>
                <?php if($category->parent_id == $parent_cat): ?>

                <li class="<?=$k == 0 ? 'active' : ''?>">
                    <a data-toggle="tab" href="#tab-vidg<?=$parent_cat?>-<?= Html::encode($category->id) ?>">
                        <?= Yii::$app->mycomponent->ifSet($category->name) ?>    
                    </a>
                </li>

                <?php $k++; endif; ?>
            <?php endforeach; $k=0;?>                            
        </ul>

        <div class="tab-content">
        <?php $k=0; foreach ($categories as $category): ?>
            <?php if($category->parent_id == $parent_cat): ?>
            <div class="tab-pane fade <?=$k == 0 ? 'in active' : ''?>" id="tab-vidg<?=$parent_cat?>-<?= Html::encode($category->id) ?>">
                <?= ProductLimit::widget(['limit' => 8, 'categoryId'=>$category->id, ]) ?>
            </div><!-- end pr block -->
        <?php $k++; endif; ?>
        <?php endforeach; $k=0;?>  
        </div><!-- end sub tab -->

    </div>
<?php endforeach; ?> <!--end parent cats loop -->
</div><!--end parent tabs content -->