<?php
    use yii\helpers\Html;
    use app\models\Lang;
    $langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';    
?>
<a href="<?=$langLink?>/product/<?= Html::encode($product->id) ?>" class="content-class-style">
    <div class="col-xs-12 hover-div-slider padd-zero"
    >
        <div class="col-xs-12 padd-zero hov_img_none" style="background-image: url(/images/product/<?= Html::encode($product->img_src) ?>);">
        </div>
        <div class="change-hover">
            <div class="col-xs-12 padd-zero">
                <div class="col-xs-12 product-atribute-bl">
                    <p class="title_room"><?= Html::encode($product->name) ?></p>
                    <div class="width_style">
                        <span class="span-title"><?= Yii::t('main','Category') ?>:</span>
                        <span class="span-text title_category">
                            <?= isset($product->category->name) ? $product->category->name : '&#8212;'  ?>  
                        </span>
                    </div>
                    <div class="width_style">
                        <span class="span-title"><?= Yii::t('main','Area') ?>:</span>
                        <span class="span-text title_object_area">
                            <?= isset($product->object_area) ? $product->object_area : '&#8212;'  ?>
                        </span>
                    </div>
                    <div class="width_style">
                        <span class="span-title"><?= Yii::t('main','Regione') ?>:</span>
                        <span class="span-text title_region">
                            <?= isset($product->region->name) ? $product->region->name : '&#8212;'  ?>
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 product-hr"></div>    

            <div class="col-xs-12 padd-zero style-padd-div-same">
                <div class="col-sm-7 col-xs-7 no-padding">
                    <p class="money_style"><?= Html::encode($product->currenc->name.' '.$product->price) ?></p>
                </div>
                <div class="col-sm-2 col-xs-2 padd-zero shower_bl">
                    <img class="style-img shower_class" src="/img/shower_icon.png">
                    <span class="numb-style-text"><?=$product->prodAttr->bathroom?></span>
                </div>
                <div class="col-sm-3 col-xs-3 padd-zero bed_bl">
                    <img class="style-img bed_class" src="/img/bed_icon.png">
                    <span class="numb-style-text"><?=$product->prodAttr->room?></span>
                </div>
            </div>
        </div>
    </div>
</a>