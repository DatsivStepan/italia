<?php
    use yii\helpers\Html;
    use app\models\Lang;
    use app\widgets\SingleProduct;
    $langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';  
?>

<div class="owl-carousel owl-theme my-owl-item">
    <?php foreach ($products as $product): ?>
        <?= SingleProduct::widget([ 'product' => $product ]) ?>
    <?php endforeach; ?>           
</div>