<?php
use yii\helpers\Html;
use app\models\Lang;
$langLink = Lang::getCurrent()->url != Lang::getDefaultLang()->url ? '/'.Lang::getCurrent()->url : '';
?>
<div class="interested">
    <div class="container">
        <div class="col-xs-12 padding-div-style">
            <p class="text-title-same interested-title-style"><?= Yii::t('main','Maybe_you_are_interested_in') ?></p>
            <a class="katalog_button" href="<?=$langLink?>/catalog"><?= Yii::t('main','To_the_catalog') ?></a>
        </div>
        <div class="col-xs-12 padd-zero">
            <?php foreach ($modelProduct as $value_modelProduct){ ?>
            <a href="<?=$langLink?>/product/<?= Html::encode($value_modelProduct->id) ?>">
                <div class="content-class-style">
                    <div class="col-xs-12 hover-div-slider padd-zero">
                        <div class="col-xs-12 padd-zero hov_img_none">
                        </div>
                        <div class="change-hover">
                            <div class="col-xs-12 padd-zero style_border">
                                <div class="col-xs-12">
                                    <p class="title_room"><?= $value_modelProduct->name ?></p>
                                    <div class="width_style">
                                        <span class="span-title">Категория:</span>
                                        <span class="span-text"><?= $value_modelProduct->category->name ?></span>
                                    </div>
                                    <div class="width_style">
                                        <span class="span-title">Площадь:</span>
                                        <span class="span-text"><?= $value_modelProduct->object_area ?> м2</span>
                                    </div>
                                    <div class="width_style">
                                        <span class="span-title">Регион:</span>
                                        <span class="span-text"><?= $value_modelProduct->region->name ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 padd-zero style-padd-div-same">
                                <div class="col-sm-7 col-xs-7">
                                    <p class="money_style"><?= $value_modelProduct->currenc->name.' '.$value_modelProduct->price ?></p>
                                </div>
                                <div class="col-sm-2 col-xs-2 padd-zero">
                                    <img class="style-img shower_class" src="/img/shower_icon.png">
                                    <span class="numb-style-text">2</span>
                                </div>
                                <div class="col-sm-3 col-xs-3 padd-zero ">
                                    <img class="style-img bed_class" src="/img/bed_icon.png">
                                    <span class="numb-style-text">3</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            <?php }?>
        </div>
    </div>
</div>