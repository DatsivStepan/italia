<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<div class="flag-position">
    <a href="#"><img src="<?= Url::home().'flags/'.$current->icon;?>"/></a>
    <span><?= $current->url;?></span>
</div>
<?php foreach ($langs as $lang):?>
    <div class="flag-position">
        <?= Html::a('<img src="'.Url::home().'flags/'.$lang->icon.'">', Url::home().$lang->url.Yii::$app->getRequest()->getLangUrl()) ?>
        <span><?= $lang->url ?></span>
    </div>
<?php endforeach;?>