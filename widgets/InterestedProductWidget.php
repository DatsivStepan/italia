<?php
namespace app\widgets;
use app\models\Product;

class InterestedProductWidget extends \yii\bootstrap\Widget
{
    public $id;
    public function init(){}

    public function run() {
        $interested_product = Product::find()->andWhere(['id' => $this->id])->one();
        $product_category = $interested_product->category_id;
        $product_type = $interested_product->product_type_id;
        $modelProduct = Product::find()->andWhere(['category_id' => $product_category,'product_type_id'=> $product_type])->all();
        return $this->render('InterestedProductWidget/interested_product',[
            'modelProduct' => $modelProduct,
            ]);

    }
}
