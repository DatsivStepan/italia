<?php
namespace app\widgets;

class SingleProduct extends \yii\bootstrap\Widget
{
	public $product;

    public function init(){}

    public function run() {
        return $this->render('SingleProduct/index', [
            'product' => $this->product,
        ]);
    }
}