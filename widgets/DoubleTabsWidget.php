<?php
namespace app\widgets;

class DoubleTabsWidget extends \yii\bootstrap\Widget
{
	public $categories;

    public function init(){}

    public function run() {
        return $this->render('tabs/view', [
            'categories' => $this->categories,
        ]);
    }
}