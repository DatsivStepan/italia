<?php
namespace app\widgets;
use app\models\Product;

class ProductLimit extends \yii\bootstrap\Widget
{
	public $limit;
	public $categoryId;

    public function init(){}

    public function run() {
        return $this->render('prod/view', [
            'products' => Product::find()->limit($this->limit)->andWhere( ['category_id'=>$this->categoryId] )->all(),
        ]);
    }
}