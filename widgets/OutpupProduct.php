<?php
namespace app\widgets;

class OutpupProduct extends \yii\bootstrap\Widget
{
	public $products;

    public function init(){}

    public function run() {
        return $this->render('prod/view', [
            'products' => $this->products,
        ]);
    }
}